//sequencing.js
const mongoose = require("mongoose");

const EmailCounterSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true
    },
    seq: {
        type: Number,
        required: true
    }
});

const Counter = mongoose.model('EmailCounter', EmailCounterSchema);
const getSequenceNextValueForEmail = async (seqName) => {
    await Counter.find({
        "_id": seqName
    }).then(async (res) => {
        if (res.length === 0) {
            const counter = new Counter({
                _id: 'email_id',
                seq: 0
            })
            await counter.save().then(() => {
                console.log("save successfull!!");
            }).catch((err) => {
                console.log("during save: ", err);
            })
        }
    }).catch((err) => {
        console.log("Something went wrong: ", err);
    })
    return new Promise((resolve, reject) => {
        Counter.findByIdAndUpdate(
            { "_id": seqName },
            { "$inc": { "seq": 1 } }).then((counter) => {
                resolve(counter.seq + 1);

            }).catch((error) => {
                console.log("In seq: ", error);
            })
    });
};

const insertEmailCounter = (seqName) => {
    const newCounter = new Counter({ _id: seqName, seq: 1 });
    return new Promise((resolve, reject) => {
        newCounter.save()
            .then(data => {
                resolve(data.seq);
            })
            .catch(err => reject(error));
    });
}
module.exports = {
    EmailCounterSchema,
    getSequenceNextValueForEmail,
    insertEmailCounter
}