const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const otpGenerator = require('otp-generator');
const bcrypt = require('bcrypt');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const PDFDocument = require('pdfkit');
const memberSchema = require("./models/member")
const moment = require('moment');
const historySchema = require("./models/history")

// Use cors middleware

const Owner = require('./models/owner');
const OtpCode = require('./models/otp');
const TemporaryOwner = require('./models/temporaryOwner'); 
const TempForgot = require('./models/TempForgot'); 
const sequencing = require("./sequencing.js");
const emailSequencing = require("./email_sequencing.js")
const History = require('./models/history');
const Member = require('./models/member')
const Event = require('./models/event')
const eventSchema = require("./models/event")
const expenditureSchema = require("./models/expenditure")
const deactivatedSchema = require("./models/deactivated")
const app = express();
const port = 5000;

app.use(bodyParser.json()); // Parse JSON requests
app.use(cookieParser()); // Parse cookies
app.use(cors({
  credentials: true,
  origin: 'https://my-project-rust-nu.vercel.app',
}));
// MongoDB connection
mongoose.connect('mongodb+srv://tid_iomp:tid_iomp@cluster0.rcrczdw.mongodb.net/fithub_temp', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
}).then(() => {
  console.log('Connected to MongoDB');
}).catch((error) => {
  console.error('Failed to connect to MongoDB:', error);
  process.exit(1);
});

// Configure nodemailer
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'fithub72@gmail.com',
    pass: 'ltni dsgw qeuf pfjw',
  },
});


// Owner Registration route (Tested)
/*app.post('/owners/register', async (req, res) => {
  try {
    const { name, gymName, email, number, aadhar, password, city, street, pincode, insta} = req.body;
    const existingUser = await Owner.findOne({ $or: [{ aadhar }, { number }, { email }] });
    if (existingUser) {
      
      return res.status(400).json({ error: 'You are already registered. Please login.' });
    }
    function generateNumericOTP(length) {
      const digits = '123456789';
      let otp = '';
      for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * digits.length);
        otp += digits[randomIndex];
      }
      return otp;
    }
    const otp = generateNumericOTP(6);
    const hashedPassword = await bcrypt.hash(password, 10);
    const otpTimestamp = Date.now();
    async function generateCharCode() {
      const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      let charCode;
      async function findUniqueCode() {
        charCode = '';
        for (let i = 0; i < 4; i++) {
          charCode += characters.charAt(Math.floor(Math.random() * characters.length));
        }
        const existingOwner = await TemporaryOwner.findOne({ charCode });
        if (existingOwner) {
          return findUniqueCode();
        }
        return charCode;
      }
      return await findUniqueCode();
    }
    const charCode = await generateCharCode();
    await TemporaryOwner.create({
      name,
      gymName,
      email,
      number,
      aadhar,
      password: hashedPassword,
      charCode,
      city,
      street,
      pincode,
      otp,
      otpTimestamp,
      insta
    });
    let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");

    const mailOptions = {
      from: 'your-email@gmail.com',
      to: email,
      subject: 'Verify Your Email with FitHub',
      html: 
      ``
      
  };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error('Error sending email:', error);
        return res.status(500).json({ error: 'An error occurred while sending OTP' });
      } else {
        console.log('Email sent: ' + info.response);
        res.json({ message: 'OTP sent to your email. Please verify.' });
      }
    });
  } catch (error) {
    console.error('Error while registering:', error);
    res.status(500).json({ error: 'An error occurred while registering' });
  }
});*/


const { promisify } = require('util');

const sendMailAsync = (mailOptions) => {
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        reject(error);
      } else {
        resolve(info);
      }
    });
  });
};

app.post('/owners/register', async (req, res) => {
  try {
    const { name, gymName, email, number, aadhar, password, city, street, pincode, insta } = req.body;
    const existingUser = await Owner.findOne({ $or: [{ aadhar }, { number }, { email }] });

    if (existingUser) {
      return res.status(400).json({ error: 'You are already registered. Please login.' });
    }

    const otp = generateNumericOTP(6);
    const hashedPassword = await bcrypt.hash(password, 10);
    const otpTimestamp = Date.now();
    const charCode = await generateCharCode();

    await TemporaryOwner.create({
      name,
      gymName,
      email,
      number,
      aadhar,
      password: hashedPassword,
      charCode,
      city,
      street,
      pincode,
      otp,
      otpTimestamp,
      insta
    });

    let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");

    const mailOptions = {
      from: 'your-email@gmail.com',
      to: email,
      subject: 'Verify Your Email with FitHub',
      html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>Email Verification</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
      <![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
       h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
       .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
       .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
       .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
       .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
       </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
      <td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><a target="_blank" href="https://viewstripo.email" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#EDEDED;font-size:14px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></a> </td></tr></table></td></tr></table></td></tr></table></td></tr></table>
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#fcfdfe" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#fcfdfe;width:600px" role="none"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Dear ${name},</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p>
      <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Thank you for registering with FitHub! To complete your gym registration, please use the following OTP (One Time Password):</p></td></tr> <tr><td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="" style="height:41px; v-text-anchor:middle; width:141px" arcsize="50%" stroke="f" fillcolor="#4550be"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>458745</center> </v:roundrect></a>
      <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#4550be;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#4550be;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #4550be">${otp}</a></span> <!--<![endif]--></td></tr> <tr>
      <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Please note that this OTP is valid for 2 minutes.</p></td></tr></table></td></tr></table></td></tr></table></td></tr></table> <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr>
      <td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://viewstripo.email" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr><td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr>
      <td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">If you have any questions or need assistance with this process, our friendly support team is here to help.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr><td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr>
      <td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:8121050345" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:8121050345" style="height:47px; v-text-anchor:middle; width:214px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:17px; font-weight:400; line-height:17px; mso-text-raise:1px'>+91 8121050345</center> </v:roundrect></a>
      <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:8121050345" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 8121050345</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
      <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:fithub@gmail.com" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:fithub@gmail.com" style="height:41px; v-text-anchor:middle; width:238px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>fithub@gmail.com</center> </v:roundrect></a>
      <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:fithub@gmail.com" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">fithub@gmail.com</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
       <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
      <td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
      <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">fithub@gmail.com</p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+(</a>91) 8121050345</p></td></tr> <tr>
      <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Hyderabad</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td><td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/fithub_72" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`
    };

    res.json({ message: 'OTP sent to your email. Please verify.' });

    await sendMailAsync(mailOptions);

  } catch (error) {
    console.error('Error while registering:', error);
    res.status(500).json({ error: 'An error occurred while registering' });
  }
});

function generateNumericOTP(length) {
  const digits = '123456789';
  let otp = '';
  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * digits.length);
    otp += digits[randomIndex];
  }
  return otp;
}

async function generateCharCode() {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let charCode;

  async function findUniqueCode() {
    charCode = '';
    for (let i = 0; i < 4; i++) {
      charCode += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    const existingOwner = await TemporaryOwner.findOne({ charCode });
    
    if (existingOwner) {
      return findUniqueCode();
    }

    return charCode;
  }

  return await findUniqueCode();
}

// Owner Verification route (Tested)
/*app.post('/owners/verify', async (req, res) => {
  try {
    const { email, otp } = req.body;

    const temporaryOwner = await TemporaryOwner.findOne({ email });

    if (!temporaryOwner || temporaryOwner.otp !== otp) {
      return res.status(400).json({ error: 'Invalid OTP' });
    }

    const currentTimestamp = Date.now();
    const otpTimestamp = temporaryOwner.otpTimestamp;

    if (currentTimestamp - otpTimestamp > 2 * 60 * 1000) {
      await TemporaryOwner.findOneAndDelete({ email });
      return res.status(400).json({ error: 'OTP has expired, Please register once again!' });
    }

    const { name, gymName, number, aadhar, password, charCode, city, street, pincode, insta } = temporaryOwner;
    const generatedId = `FHGYM_${await sequencing.getSequenceNextValue("user_id")}`;
    const owner = new Owner({
      ownerId: generatedId,
      name,
      gymName,
      email,
      number,
      aadhar,
      password,
      charCode,
      city,
      street,
      pincode,
      insta
    });

    await owner.save();

    await TemporaryOwner.findOneAndDelete({ email });

    // Send owner details to admin for verification
    const currentDate = new Date();
    const formattedDate = `${currentDate.getDate()}/${currentDate.getMonth() + 1}/${currentDate.getFullYear()}`;
    const formattedTime = currentDate.toLocaleTimeString('en-US', { hour: 'numeric', minute: '2-digit', hour12: true });

    const mailOptions = {
      from: 'fithub72@gmail.com',
      to: 'shaiksohailhu7n@gmail.com', // Replace with actual admin email
      subject: 'Owner Details for Verification',
      html: `
        <!DOCTYPE html>
        <html lang="en">
    
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Owner Details for Verification</title>
           
        </head>
    
        <body>
    
            <div class="container">
    
                <div class="header">
                    <h1>Owner Details for Verification</h1>
                </div>
    
                <div class="details">
                    <strong>Owner Id:</strong> ${generatedId}<br>
                    <strong>Name:</strong> ${name}<br>
                    <strong>Gym Name:</strong> ${gymName}<br>
                    <strong>Email:</strong> ${email}<br>
                    <strong>Number:</strong> ${number}<br>
                    <strong>Aadhar:</strong> ${aadhar}<br>
                    <strong>CharCode:</strong> ${charCode}<br>
                    <strong>City:</strong> ${city}<br>
                    <strong>Street:</strong> ${street}<br>
                    <strong>Pincode:</strong> ${pincode}<br>
                    <strong>TimeStamp:</strong> ${formattedDate} - ${formattedTime}<br>
                    <string>insta: </string> ${insta}<br>
                </div>
    
                <div class="footer">
                    <p>This email was sent by FitHub.</p>
                </div>
    
            </div>
    
        </body>
    
        </html>
      `
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error('Error sending email to admin:', error);
        // You may choose to handle the error here
      } else {
        console.log('Email sent to admin: ' + info.response);
      }
    });

    res.json({ message: 'Verification Successfull. Please Login', ownerId: owner.ownerId });
  } catch (error) {
    console.error('Error while verifying OTP:', error);
    res.status(500).json({ error: 'An error occurred while verifying OTP' });
  }
});*/

app.post('/owners/verify', async (req, res) => {
  try {
    const { email, otp } = req.body;

    const temporaryOwner = await TemporaryOwner.findOne({ email });

    if (!temporaryOwner || temporaryOwner.otp !== otp) {
      return res.status(400).json({ error: 'Invalid OTP' });
    }

    const currentTimestamp = Date.now();
    const otpTimestamp = temporaryOwner.otpTimestamp;

    if (currentTimestamp - otpTimestamp > 2 * 60 * 1000) {
      await TemporaryOwner.findOneAndDelete({ email });
      return res.status(400).json({ error: 'OTP has expired, Please register once again!' });
    }

    const { name, gymName, number, aadhar, password, charCode, city, street, pincode, insta } = temporaryOwner;
    const generatedId = `FHGYM_${await sequencing.getSequenceNextValue("user_id")}`;
    const owner = new Owner({
      ownerId: generatedId,
      name,
      gymName,
      email,
      number,
      aadhar,
      password,
      charCode,
      city,
      street,
      pincode,
      insta
    });

    await owner.save();

    await TemporaryOwner.findOneAndDelete({ email });

    // Send owner details to admin for verification
    const currentDate = new Date();
    const formattedDate = `${currentDate.getDate()}/${currentDate.getMonth() + 1}/${currentDate.getFullYear()}`;
    const formattedTime = currentDate.toLocaleTimeString('en-US', { hour: 'numeric', minute: '2-digit', hour12: true });
    let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");
    const mailOptions = {
      from: 'fithub72@gmail.com',
      to: 'shaiksohailhu7n@gmail.com', // Replace with actual admin email
      subject: 'Owner Details for Verification',
      html: `
        <!DOCTYPE html>
        <html lang="en">
    
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Owner Details for Verification</title>
           
        </head>
    
        <body>
    
            <div class="container">
    
                <div class="header">
                    <h1>Owner Details for Verification</h1>
                </div>
    
                <div class="details">
                    <strong>Owner Id:</strong> ${generatedId}<br>
                    <strong>Name:</strong> ${name}<br>
                    <strong>Gym Name:</strong> ${gymName}<br>
                    <strong>Email:</strong> ${email}<br>
                    <strong>Number:</strong> ${number}<br>
                    <strong>Aadhar:</strong> ${aadhar}<br>
                    <strong>CharCode:</strong> ${charCode}<br>
                    <strong>City:</strong> ${city}<br>
                    <strong>Street:</strong> ${street}<br>
                    <strong>Pincode:</strong> ${pincode}<br>
                    <strong>TimeStamp:</strong> ${formattedDate} - ${formattedTime}<br>
                    <strong>insta: </strong> ${insta}<br>
                    <strong>eid :  ${eid}<br>
                </div>
    
                <div class="footer">
                    <p>This email was sent by FitHub.</p>
                </div>
    
            </div>
    
        </body>
    
        </html>
      `
    };

    // Use sendMailAsync function
    res.json({ message: 'Verification Successful. Please Login', ownerId: owner.ownerId });

    await sendMailAsync(mailOptions);


  } catch (error) {
    console.error('Error while verifying OTP:', error);
    res.status(500).json({ error: 'An error occurred while verifying OTP' });
  }
});

// Login (Tested)
app.post('/owners/login', async (req, res) => {
  try {
    const { email, password } = req.body;

    const owner = await Owner.findOne({ email });

    if (!owner) {
      return res.status(404).json({ error: 'Owner not found' });
    }

    const passwordMatch = await bcrypt.compare(password, owner.password);

    if (!passwordMatch) {
      return res.status(401).json({ error: 'Invalid credentials' });
    }

    if (!owner.authorized) {
      return res.status(403).json({ 
        error: 'Your gym is not yet verified. Support team will verify the details within 24 to 48 hours.',
        message: 'Support team will verify the details within 24 to 48 hours.'
      });
    }

    if (owner.subscriptionExpireDate < new Date()) {
      return res.status(403).json({ 
        error: 'Your plan validity is expired. Please renew your plan to continue using the application.',
        message: 'Please renew your plan to continue using the application.'
      });
    }

    // Add charCode to cookies
    //res.cookie('charCode', owner.charCode, { domain: '.vercel.app', secure: true });
    res.cookie('charCode', owner.charCode, {
      secure: true,
      httpOnly: true,
    });

    res.json({ message: 'Login successful', ownerId: owner.ownerId, charCode:owner.charCode });
  } catch (error) {
    console.error('Error while owner login:', error);
    res.status(500).json({ error: 'An error occurred while logging in' });
  }
});

app.post('/owners/forgot-password', async (req, res) => {
  try {
    const { email, aadhar } = req.body;

    const owner = await Owner.findOne({ email, aadhar });

    if (!owner) {
      return res.status(404).json({ error: 'User not found' });
    }

    function generateNumericOTP(length) {
      const digits = '123456789';
      let otp = '';
      for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * digits.length);
        otp += digits[randomIndex];
      }
      return otp;
    }

    const otp = generateNumericOTP(6);

    const otpTimestamp = Date.now();

    await TempForgot.create({
      email,
      otp,
      otpTimestamp,
    });
    let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");
    const mailOptions = {
      from: 'your-email@gmail.com',
      to: email,
      subject: 'FitHub Password Reset OTP', 
      html: `
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>FitHub password reset</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
      <![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
       h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
       .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
       .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
       .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
       .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
       </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
      <td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><a target="_blank" href="https://viewstripo.email" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#EDEDED;font-size:14px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></a> </td></tr></table></td></tr></table></td></tr></table></td></tr></table>
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#fcfdfe" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#fcfdfe;width:600px" role="none"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Dear ${owner.name},</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p>
      <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Your OTP for password reset is:</p></td></tr> <tr><td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="" style="height:41px; v-text-anchor:middle; width:141px" arcsize="50%" stroke="f" fillcolor="#4550be"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>458745</center> </v:roundrect></a>
      <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#4550be;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#4550be;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #4550be">${otp}</a></span> <!--<![endif]--></td></tr> <tr>
      <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Please note that this OTP is valid for 2 minutes.</p></td></tr></table></td></tr></table></td></tr></table></td></tr></table> <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr>
      <td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://viewstripo.email" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr><td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr>
      <td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">If you have any questions or need assistance with this process, our friendly support team is here to help.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr><td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr>
      <td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:8121050345" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:8121050345" style="height:47px; v-text-anchor:middle; width:214px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:17px; font-weight:400; line-height:17px; mso-text-raise:1px'>+91 8121050345</center> </v:roundrect></a>
      <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:8121050345" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 8121050345</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
      <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:fithub@gmail.com" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:fithub@gmail.com" style="height:41px; v-text-anchor:middle; width:238px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>fithub@gmail.com</center> </v:roundrect></a>
      <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:fithub@gmail.com" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">fithub@gmail.com</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
       <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
      <td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
      <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">fithub@gmail.com</p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+(</a>91) 8121050345</p></td></tr> <tr>
      <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Hyderabad</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td><td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/fithub_72" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>
      `
  };
  res.json({ message: 'Forgot OTP sent to your email.' });

  await sendMailAsync(mailOptions);

  } catch (error) {
    console.error('Error while sending forgot password OTP:', error);
    res.status(500).json({ error: 'An error occurred while sending OTP' });
  }
});

// Reset Password route (Tested)
app.post('/owners/reset-password', async (req, res) => {
  try {
    const { email, otp, password, reenterPassword } = req.body;

    const tempForgot = await TempForgot.findOne({ email });

    if (!tempForgot || tempForgot.otp !== otp) {
      return res.status(400).json({ error: 'Invalid OTP' });
    }

    const currentTimestamp = Date.now();
    const otpTimestamp = tempForgot.otpTimestamp;

    if (currentTimestamp - otpTimestamp > 2 * 60 * 1000) {
      await TempForgot.findOneAndDelete({ email });
      return res.status(400).json({ error: 'OTP has expired' });
    }

    if (password !== reenterPassword) {
      return res.status(400).json({ error: 'Passwords do not match' });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    await Owner.updateOne(
      { email },
      { $set: { password: hashedPassword } }
    );

    await TempForgot.findOneAndDelete({ email });

    res.json({ message: 'Password reset successful' });
  } catch (error) {
    console.error('Error while resetting password:', error);
    res.status(500).json({ error: 'An error occurred while resetting password' });
  }
});

const formatDate = (dateString) => {
  const dateObject = new Date(dateString);
  const day = dateObject.getDate().toString().padStart(2, '0');
  const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
  const year = dateObject.getFullYear();

  return `${day}-${month}-${year}`;
};

// Add members to the gym specific gym using charCode (Tested)
/*app.post('/members/addMember', async (req, res) => {
  try {
    const { charCode, name, email, contactNumber, age, gender, planChoice, membershipFee, paymentMethod, preferredSlot, services, address } = req.body;


    if (!charCode) {
      return res.status(400).json({ error: 'Account not found. Please Login!' });
    }

    // Check if charCode exists in owners
    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }
    const Member = new mongoose.model(`${charCode}_Members`, memberSchema);
const History = new mongoose.model(`${charCode}_History`, historySchema);
 const DeactivatedMember = new mongoose.model(`${charCode}_deactivated`, deactivatedSchema);
      
     // Check if memberId is already associated with an active or deactivated user
     const existingActiveUser = await Member.findOne({ contactNumber});
     const existingDeactivatedUser = await DeactivatedMember.findOne({ contactNumber});
 
     if (existingActiveUser) {
       return res.status(400).json({ error: 'This member is already exist' });
     }
 
     if ( existingDeactivatedUser) {
       return res.status(400).json({ error: 'This member is already exist. And this member status is Deactivated!' });
     }

    // Generate memberId using charCode as prefix
    const memberId = await sequencing.getSequenceNextValue("user_id");

    // Set registrationDate and lastRenewalDate to current date
    const currentDate = new Date();
    const formattedDate = currentDate.toLocaleDateString('en-GB'); //

    let validUptoDate;

switch (planChoice) {
  case 'Basic':
    validUptoDate = new Date(currentDate);
    validUptoDate.setMonth(validUptoDate.getMonth() + 1);
    break;
  case 'Silver':
    validUptoDate = new Date(currentDate);
    validUptoDate.setMonth(validUptoDate.getMonth() + 3);
    break;
  case 'Gold':
    validUptoDate = new Date(currentDate);
    validUptoDate.setMonth(validUptoDate.getMonth() + 6);
    break;
  case 'Platinum':
    validUptoDate = new Date(currentDate);
    validUptoDate.setMonth(validUptoDate.getMonth() + 12);
    break;
  default:
    return res.status(400).json({ error: 'Invalid planChoice' });
}



const user = new Member({
  memberId,
  name,
  email,
  contactNumber,
  age,
  gender,
  planChoice,
  membershipFee,
  paymentMethod,
  preferredSlot,
  services,
  validUptoDate,
  address
});

await user.save();

const id = await sequencing.getSequenceNextValue("user_id");
const refNo = await sequencing.getSequenceNextValue("user_id");

const historyRecord = new History({
  id,
  memberId: user.memberId,
  name,
  membershipFee,
  contactNumber,
  planChoice,
  preferredSlot,
  validUptoDate,
  paymentMethod,
  services,
  avatar: user.avatar,
  monthYear: user.monthYear
});

let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");
await historyRecord.save();


    let ownerEmail=owner.email;
    const mailOptions = {
      from: 'your-email@gmail.com',
      to: email, ownerEmail,
      subject: `Welcome to ${owner.gymName}! Your Fitness Journey Begins!`,
      html: 
      `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection">Add new member</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
<![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
 h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
 .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
 .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
 .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
 .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
 </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
<td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></td> </tr></table></td></tr></table></td></tr></table></td></tr></table> <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr>
<td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Welcome to ${owner.gymName}! Your Fitness Journey Begins!</h1> </td></tr><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Dear ${user.name},<br></p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">We are thrilled to welcome you to ${owner.gymName}! Thank you for choosing us as your fitness destination. Here are the details of your membership:</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Member Id : ${user.memberId}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Name : ${user.name}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Status : Active</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Action : New Registration</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Admission Date : ${formatDate(user.admissionDate)}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Paid For : ${formatDate(user.admissionDate)}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Valid Upto : ${formatDate(user.validUptoDate)}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Phone Number : ${user.contactNumber}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Email : ${user.email}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Age : ${user.age}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Gender : ${user.gender}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Plan Choice : ${user.planChoice}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Membership Fee : ${user.membershipFee}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Payment Method :  ${user.paymentMethod}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Time Slot : ${user.preferredSlot}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Services : ${user.services}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Address : ${user.address}</p></td></tr></table></td></tr></table></td></tr></table></td></tr></table>
 <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr><td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr>
<td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr><td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">We are delighted to have you on board and look forward to supporting you on your fitness journey. If you have any questions or need assistance, feel free to reach out to our team.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
<td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:+91 ${owner.number}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:+91 ${owner.number}" style="height:41px; v-text-anchor:middle; width:193px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>+91 ${owner.number}</center> </v:roundrect></a>
<![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:+91 ${owner.number}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 ${owner.number}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
<td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:${owner.email}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:${owner.email}" style="height:41px; v-text-anchor:middle; width:218px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>${owner.email}</center> </v:roundrect></a>
<![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:${owner.email}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">${owner.email}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
 <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
<td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
<td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" href="mailto:info@lingualink.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px">${owner.email}</a></p></td></tr> <tr>
<td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+91 ${owner.number}</a></p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">${owner.street} ${owner.city} ${owner.pincode}</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
<td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr>
<td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr>
<td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`
    };




    
    

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error('Error sending welcome email:', error);
        return res.status(500).json({ error: 'An error occurred while sending the welcome email' });
      }

      console.log('Welcome Email sent to member:', info.response);
      res.json({ message: 'Member added successfully', memberId: user.memberId, name: user.name });
    });
  } catch (error) {
    console.error('Error while adding member:', error);
    res.status(500).json({ error: 'An error occurred while adding the member' });
  }
});*/

app.post('/members/addMember', async (req, res) => {
  try {
    const { charCode, name, email, contactNumber, age, gender, planChoice, membershipFee, paymentMethod, preferredSlot, services, address } = req.body;

    if (!charCode) {
      return res.status(400).json({ error: 'Account not found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_Members`, memberSchema);
    const History = new mongoose.model(`${charCode}_History`, historySchema);
    const DeactivatedMember = new mongoose.model(`${charCode}_deactivated`, deactivatedSchema);

    const existingActiveUser = await Member.findOne({ contactNumber });
    const existingDeactivatedUser = await DeactivatedMember.findOne({ contactNumber });

    if (existingActiveUser || existingDeactivatedUser) {
      const errorMessage = existingDeactivatedUser
        ? 'This member is already exist. And this member status is Deactivated!'
        : 'This member is already exist';
      return res.status(400).json({ error: errorMessage });
    }

    const memberId = await sequencing.getSequenceNextValue("user_id");
    const currentDate = new Date();
    const validUptoDate = getValidUptoDate(currentDate, planChoice);

    const user = new Member({
      memberId,
      name,
      email,
      contactNumber,
      age,
      gender,
      planChoice,
      membershipFee,
      paymentMethod,
      preferredSlot,
      services,
      validUptoDate,
      address
    });

    await user.save();

    const id = await sequencing.getSequenceNextValue("user_id");
    let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");

    const historyRecord = new History({
      id,
      memberId: user.memberId,
      name,
      membershipFee,
      contactNumber,
      planChoice,
      preferredSlot,
      validUptoDate,
      paymentMethod,
      services,
      avatar: user.avatar,
      monthYear: user.monthYear
    });

    await historyRecord.save();
    let ownerEmail=owner.email;
    const mailOptions = {
      from: 'your-email@gmail.com',
      to: email, ownerEmail,
      subject: `Welcome to ${owner.gymName}! Your Fitness Journey Begins!`,
      html: 
      `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>New Registrations</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
<![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
 h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
 .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
 .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
 .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
 .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
 </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
<td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></td> </tr></table></td></tr></table></td></tr></table></td></tr></table> <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr>
<td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Welcome to ${owner.gymName}! Your Fitness Journey Begins!</h1> </td></tr><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Dear ${user.name},<br></p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">We are thrilled to welcome you to ${owner.gymName}! Thank you for choosing us as your fitness destination. Here are the details of your membership:</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Member Id : ${user.memberId}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Name : ${user.name}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Status : Active</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Action : New Registration</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Admission Date : ${formatDate(user.admissionDate)}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Paid For : ${formatDate(user.admissionDate)}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Valid Upto : ${formatDate(user.validUptoDate)}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Phone Number : ${user.contactNumber}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Email : ${user.email}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Age : ${user.age}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Gender : ${user.gender}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Plan Choice : ${user.planChoice}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Membership Fee : ${user.membershipFee}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Payment Method :  ${user.paymentMethod}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Time Slot : ${user.preferredSlot}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Services : ${user.services}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Address : ${user.address}</p></td></tr></table></td></tr></table></td></tr></table></td></tr></table>
 <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr><td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr>
<td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr><td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">We are delighted to have you on board and look forward to supporting you on your fitness journey. If you have any questions or need assistance, feel free to reach out to our team.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
<td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:+91 ${owner.number}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:+91 ${owner.number}" style="height:41px; v-text-anchor:middle; width:193px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>+91 ${owner.number}</center> </v:roundrect></a>
<![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:+91 ${owner.number}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 ${owner.number}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
<td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:${owner.email}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:${owner.email}" style="height:41px; v-text-anchor:middle; width:218px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>${owner.email}</center> </v:roundrect></a>
<![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:${owner.email}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">${owner.email}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
 <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
<td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
<td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" href="mailto:info@lingualink.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px">${owner.email}</a></p></td></tr> <tr>
<td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+91 ${owner.number}</a></p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">${owner.street} ${owner.city} ${owner.pincode}</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
<td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr>
<td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr>
<td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`
    };

    res.json({ message: 'Member added successfully', memberId: user.memberId, name: user.name });

    await sendMailAsync(mailOptions);
  } catch (error) {
    console.error('Error while adding member:', error);
    res.status(500).json({ error: 'An error occurred while adding the member' });
  }
});



const getValidUptoDate = (currentDate, planChoice) => {
  const validUptoDate = new Date(currentDate);

  switch (planChoice) {
    case 'Basic':
      validUptoDate.setMonth(validUptoDate.getMonth() + 1);
      break;
    case 'Silver':
      validUptoDate.setMonth(validUptoDate.getMonth() + 3);
      break;
    case 'Gold':
      validUptoDate.setMonth(validUptoDate.getMonth() + 6);
      break;
    case 'Platinum':
      validUptoDate.setMonth(validUptoDate.getMonth() + 12);
      break;
    default:
      throw new Error('Invalid planChoice');
  }

  return validUptoDate;
};

/*app.post('/members/renewMember', async (req, res) => {
  try {
    const { charCode, memberId, planChoice, membershipFee, paymentMethod, preferredSlot, services } = req.body;

    if (!charCode) {
      return res.status(400).json({ error: 'Account not found. Please Login!' });
    }

    // Check if charCode exists in owners
    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_members`, memberSchema);
    const member = await Member.findOne({ memberId });

    if (!member) {
      return res.status(404).json({ error: 'Member not found.' });
    }

    const History = new mongoose.model(`${charCode}_History`, historySchema);

    const paidFor = new Date(member.validUptoDate);
    let validUptoDate;
  
    switch (planChoice) {
      case 'Basic':
        validUptoDate = member.validUptoDate;
        validUptoDate.setMonth(validUptoDate.getMonth() + 1);
        break;
      case 'Silver':
        validUptoDate = member.validUptoDate;
        validUptoDate.setMonth(validUptoDate.getMonth() + 3);
        break;
      case 'Gold':
        validUptoDate = member.validUptoDate;
        validUptoDate.setMonth(validUptoDate.getMonth() + 6);
        break;
      case 'Platinum':
        validUptoDate = member.validUptoDate;
        validUptoDate.setMonth(validUptoDate.getMonth() + 12);
        break;
      default:
        return res.status(400).json({ error: 'Invalid planChoice' });
    }

    const id = await sequencing.getSequenceNextValue("user_id");

    // Update the status to "Active" in the members table
    await Member.updateOne({ memberId }, { lastRenewalDate: paidFor, status: 'Active', validUptoDate: validUptoDate,services:services,  planChoice: planChoice, membershipFee: membershipFee, preferredSlot:preferredSlot, paymentMethod: paymentMethod });

    const historyRecord = new History({
      id,
      memberId: memberId,
      name: member.name,
      contactNumber: member.contactNumber,
      action: 'Renewal',
      planChoice,

      membershipFee,
      validUptoDate,
      // Use member data for contactNumber
     
      
      paymentMethod,
      services,


      avatar: member.avatar,
      paidFor,
      preferredSlot,
    });

    await historyRecord.save();

    let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");
await historyRecord.save();


    let ownerEmail=owner.email;
    const mailOptions = {
      from: 'your-email@gmail.com',
      to: member.email,
      subject: `Your ${owner.gymName} Membership Has Been Successfully Renewed!`,
      html: 
      `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>Membership renewal</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
<![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
 h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
 .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
 .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
 .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
 .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
 </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
<td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></td> </tr></table></td></tr></table></td></tr></table></td></tr></table> <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr>
<td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Your ${owner.gymName} Membership Has Been Successfully Renewed!</h1> </td></tr><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Dear ${member.name},<br></p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">We are delighted to inform you that your ${owner.gymName} membership has been successfully renewed, marking the continuation of your fitness journey with us! Here are the updated details of your renewed membership:</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Member Id : ${member.memberId}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Name : ${member.name}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Status : Active</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Action : Renewal</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Admission Date : ${formatDate(member.admissionDate)}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Paid For : ${formatDate(paidFor)}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Valid Upto : ${formatDate(member.validUptoDate)}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Phone Number : ${member.contactNumber}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Email : ${member.email}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Age : ${member.age}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Gender : ${member.gender}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Plan Choice : ${member.planChoice}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Membership Fee : ${member.membershipFee}</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Payment Method :  ${member.paymentMethod}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Time Slot : ${member.preferredSlot}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Services : ${member.services}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Address : ${member.address}</p></td></tr></table></td></tr></table></td></tr></table></td></tr></table>
 <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr><td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr>
<td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr><td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">We are delighted to have you on board and look forward to supporting you on your fitness journey. If you have any questions or need assistance, feel free to reach out to our team.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
<td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:+91 ${owner.number}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:+91 ${owner.number}" style="height:41px; v-text-anchor:middle; width:193px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>+91 ${owner.number}</center> </v:roundrect></a>
<![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:+91 ${owner.number}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 ${owner.number}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
<td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
<td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:${owner.email}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:${owner.email}" style="height:41px; v-text-anchor:middle; width:218px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>${owner.email}</center> </v:roundrect></a>
<![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:${owner.email}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">${owner.email}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
 <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
<td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
<td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" href="mailto:info@lingualink.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px">${owner.email}</a></p></td></tr> <tr>
<td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+91 ${owner.number}</a></p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">${owner.street} ${owner.city} ${owner.pincode}</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
<td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr>
<td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr>
<td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`
    };




    
    

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error('Error sending welcome email:', error);
        return res.status(500).json({ error: 'An error occurred while sending the welcome email' });
      }

      console.log('Welcome Email sent to member:', info.response);
    res.json({ success: true, message: 'Member renewed successfully' });
    });
  } catch (error) {
    console.error('Error while renewing member:', error);
    res.status(500).json({ error: 'An error occurred while renewing member' });
  }
});*/

app.post('/members/renewMember', async (req, res) => {
  try {
    const { charCode, memberId, planChoice, membershipFee, paymentMethod, preferredSlot, services } = req.body;

    if (!charCode) {
      return res.status(400).json({ error: 'Account not found. Please Login!' });
    }

    // Check if charCode exists in owners
    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_members`, memberSchema);
    const member = await Member.findOne({ memberId });

    if (!member) {
      return res.status(404).json({ error: 'Member not found.' });
    }

    const History = new mongoose.model(`${charCode}_History`, historySchema);

    const paidFor = new Date(member.validUptoDate);

    // Calculate validUptoDate based on planChoice
    const planDurationMap = {
      'Basic': 1,
      'Silver': 3,
      'Gold': 6,
      'Platinum': 12,
    };

    if (!(planChoice in planDurationMap)) {
      return res.status(400).json({ error: 'Invalid planChoice' });
    }

    const durationMonths = planDurationMap[planChoice];
    const validUptoDate = new Date(member.validUptoDate);
    validUptoDate.setMonth(validUptoDate.getMonth() + durationMonths);

    // Update the status to "Active" in the members table
    const updateResult = await Member.updateOne(
      { memberId },
      {
        lastRenewalDate: paidFor,
        status: 'Active',
        validUptoDate,
        services,
        planChoice,
        membershipFee,
        preferredSlot,
        paymentMethod
      }
    );

    if (updateResult.nModified === 0) {
      return res.status(500).json({ error: 'Failed to update member information' });
    }

    // Create history record
    const historyRecord = new History({
      memberId,
      name: member.name,
      contactNumber: member.contactNumber,
      action: 'Renewal',
      planChoice,
      membershipFee,
      validUptoDate,
      paymentMethod,
      services,
      avatar: member.avatar,
      paidFor,
      preferredSlot,
    });

    // Save history record
    await historyRecord.save();

    // Send renewal email using sendMailAsync
    let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");

    const mailOptions = {
      from: 'your-email@gmail.com',
      to: member.email,
      subject: `Your ${owner.gymName} Membership Has Been Successfully Renewed!`,
      html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>Membership renewal</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
      <![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
       h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
       .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
       .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
       .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
       .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
       </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
      <td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></td> </tr></table></td></tr></table></td></tr></table></td></tr></table> <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr>
      <td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Your ${owner.gymName} Membership Has Been Successfully Renewed!</h1> </td></tr><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Dear ${member.name},<br></p>
      <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">We are delighted to inform you that your ${owner.gymName} membership has been successfully renewed, marking the continuation of your fitness journey with us! Here are the updated details of your renewed membership:</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Member Id : ${member.memberId}</p>
      <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Name : ${member.name}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Status : Active</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Action : Renewal</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Admission Date : ${formatDate(member.admissionDate)}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Paid For : ${formatDate(paidFor)}</p>
      <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Valid Upto : ${formatDate(validUptoDate)}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Phone Number : ${member.contactNumber}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Email : ${member.email}</p>
      <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Age : ${member.age}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Gender : ${member.gender}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Plan Choice : ${planChoice}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Membership Fee : ${membershipFee}</p>
      <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Payment Method :  ${paymentMethod}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Time Slot : ${preferredSlot}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Services : ${services}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Address : ${member.address}</p></td></tr></table></td></tr></table></td></tr></table></td></tr></table>
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr><td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr>
      <td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr><td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">We are delighted to have you on board and look forward to supporting you on your fitness journey. If you have any questions or need assistance, feel free to reach out to our team.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
      <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:+91 ${owner.number}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:+91 ${owner.number}" style="height:41px; v-text-anchor:middle; width:193px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>+91 ${owner.number}</center> </v:roundrect></a>
      <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:+91 ${owner.number}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 ${owner.number}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
      <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
      <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:${owner.email}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:${owner.email}" style="height:41px; v-text-anchor:middle; width:218px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>${owner.email}</center> </v:roundrect></a>
      <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:${owner.email}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">${owner.email}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
       <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
      <td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
      <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" href="mailto:info@lingualink.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px">${owner.email}</a></p></td></tr> <tr>
      <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+91 ${owner.number}</a></p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">${owner.street} ${owner.city} ${owner.pincode}</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
      <td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr>
      <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr>
      <td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`
    };

    await sendMailAsync(mailOptions);

    res.json({ success: true, message: 'Member renewed successfully' });
  } catch (error) {
    console.error('Error while renewing member:', error);
    res.status(500).json({ error: 'An error occurred while renewing member' });
  }
});


app.post('/members/reactivateMember', async (req, res) => {
  try {
    const { charCode, memberId, planChoice, membershipFee, paymentMethod, preferredSlot, services } = req.body;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const DeactivatedMember = mongoose.model(`${charCode}_deactivateds`, deactivatedSchema);
    const Member = mongoose.model(`${charCode}_members`, memberSchema);
    const History = mongoose.model(`${charCode}_History`, historySchema);

    const member = await DeactivatedMember.findOne({ memberId });

    if (!member) {
      return res.status(404).json({ error: 'Deactivated member not found.' });
    }

    const id = await sequencing.getSequenceNextValue("user_id");

    const planChoiceToMonths = {
      'Basic': 1,
      'Silver': 3,
      'Gold': 6,
      'Platinum': 12,
    };

    const monthsToAdd = planChoiceToMonths[planChoice];

    if (monthsToAdd === undefined) {
      return res.status(400).json({ error: 'Invalid planChoice' });
    }

    const paidFor = new Date();
    let validUptoDate = new Date(paidFor);
    validUptoDate.setMonth(validUptoDate.getMonth() + monthsToAdd);

    const historyRecord = new History({
      id,
      memberId,
      name: member.name,
      contactNumber: member.contactNumber,
      action: 'New Registration',
      planChoice,
      membershipFee,
      validUptoDate,
      paymentMethod,
      services,
      avatar: member.avatar,
      paidFor,
      preferredSlot,
    });

    const user = new Member({
      avatar: member.avatar,
      memberId,
      name: member.name,
      email: member.email,
      contactNumber: member.contactNumber,
      age: member.age,
      gender: member.gender,
      action: 'New Registration',
      planChoice,
      membershipFee,
      paymentMethod,
      preferredSlot,
      services,
      validUptoDate,
      address: member.address,
    });

    await Promise.all([user.save(), historyRecord.save()]);

    // Delete the member from the deactivateds collection
    await DeactivatedMember.findOneAndDelete({ memberId });


    let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");    
    
        const mailOptions = {
          from: 'your-email@gmail.com',
          to: member.email,
          subject: `Your Membership Reactivation Success`,
          html: 
          `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>Reactivate member</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
    <![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
     h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
     .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
     .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
     .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
     .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
     </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
    <td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></td> </tr></table></td></tr></table></td></tr></table></td></tr></table> <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr>
    <td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Your Membership Reactivation Success</h1> </td></tr><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Dear ${user.name},<br></p>
    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">

    We are delighted to inform you that your ${owner.gymName} membership has been successfully reactivated. Your commitment to your fitness journey is highly appreciated, and we are excited to welcome you back to our fitness community. Here are the details of your reactivated membership:
    
    </p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Member Id : ${user.memberId}</p>
    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Name : ${user.name}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Status : Active</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Action : New Registration</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Admission Date : ${formatDate(user.admissionDate)}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Paid For : ${formatDate(paidFor)}</p>
    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Valid Upto : ${formatDate(validUptoDate)}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Phone Number : ${user.contactNumber}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Email : ${user.email}</p>
    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Age : ${user.age}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Gender : ${user.gender}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Plan Choice : ${planChoice}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Membership Fee : ${membershipFee}</p>
    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Payment Method :  ${paymentMethod}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Time Slot : ${preferredSlot}</p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Services : ${services}</p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Address : ${user.address}</p></td></tr></table></td></tr></table></td></tr></table></td></tr></table>
     <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr><td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr>
    <td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr><td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">We are delighted to have you on board and look forward to supporting you on your fitness journey. If you have any questions or need assistance, feel free to reach out to our team.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
    <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:+91 ${owner.number}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:+91 ${owner.number}" style="height:41px; v-text-anchor:middle; width:193px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>+91 ${owner.number}</center> </v:roundrect></a>
    <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:+91 ${owner.number}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 ${owner.number}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
    <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:${owner.email}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:${owner.email}" style="height:41px; v-text-anchor:middle; width:218px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>${owner.email}</center> </v:roundrect></a>
    <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:${owner.email}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">${owner.email}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
     <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
    <td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
    <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" href="mailto:info@lingualink.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px">${owner.email}</a></p></td></tr> <tr>
    <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+91 ${owner.number}</a></p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">${owner.street} ${owner.city} ${owner.pincode}</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
    <td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr>
    <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr>
    <td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`
        };
    

        res.json({ success: true, message: 'Member reactivated successfully' });
        await sendMailAsync(mailOptions);
        
  } catch (error) {
    console.error('Error while reactivating member:', error);
    res.status(500).json({ error: 'An error occurred while reactivating member' });
  }
});




// Delete All Owners (Tested)
app.delete('/owners/deleteAllOwners', async (req, res) => {
  try {
    // Add any necessary authentication or confirmation steps here

    // Delete all owners
    const result = await Owner.deleteMany({});
    res.json({ message: `${result.deletedCount} owners deleted successfully` });
  } catch (error) {
    console.error('Error while deleting owners:', error);
    res.status(500).json({ error: 'An error occurred while deleting owners' });
  }
});

app.get('/members', async (req, res) => {
  try {
    const { charCode } = req.query;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = mongoose.model(`${charCode}_members`, memberSchema);
    const members = await Member.find({}, {
      memberId: 1,
      name: 1,
      contactNumber: 1,
      age: 1,
      validUptoDate: 1,
      status: 1,
      avatar: 1
    })
    .sort({ memberId: -1 }) // Sort in descending order by memberId
    .lean(); // Convert Mongoose documents to plain JavaScript objects for efficiency

    res.json({ members });
  } catch (error) {
    console.error('Error while retrieving members:', error);
    res.status(500).json({ error: 'An error occurred while retrieving members' });
  }
});


app.get('/members/:memberId', async (req, res) => {
  try {
    const charCode = req.query.charCode;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    // Assuming you have an Owner model
    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_members`, memberSchema);
    const memberId = req.params.memberId;
    const member = await Member.findOne({ memberId });

    if (!member) {
      return res.status(404).json({ error: 'Member not found' });
    }

    res.json({ user: member, status: 'success' });
  } catch (error) {
    console.error('Error while retrieving member:', error);
    res.status(500).json({ error: 'An error occurred while retrieving member' });
  }
});

app.get('/deactivatedMembers/:memberId', async (req, res) => {
  try {
    const { charCode } = req.query;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Deactivated = mongoose.model(`${charCode}_deactivateds`, deactivatedSchema);
    const memberId = req.params.memberId;
    const member = await Deactivated.findOne({ memberId });

    if (!member) {
      return res.status(404).json({ error: 'Deactivated member not found' });
    }

    res.json({ user: member, status: 'success' });
  } catch (error) {
    console.error('Error while retrieving member:', error);
    res.status(500).json({ error: 'An error occurred while retrieving member' });
  }
});

app.get('/membersProfile', async (req, res) => {
  try {
    const charCode = req.query.charCode;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_members`, memberSchema);


    const members = await Member.find();
     

    res.json({ members });
  } catch (error) {
    console.error('Error while retrieving members:', error);
    res.status(500).json({ error: 'An error occurred while retrieving members' });
  }
});

app.get('/pendingMembers', async (req, res) => {
  try {
    const charCode = req.query.charCode;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_members`, memberSchema);

    // Get the current date
    const currentDate = moment();

    // Update the status of members whose validUptoDate is less than the current date
    await Member.updateMany(
      { validUptoDate: { $lt: currentDate }, status: 'Active' },
      { $set: { status: 'Pending' } }
    );

    // Retrieve pending members
    const pendingMembers = await Member.find({ status: 'Pending' })
      .select({
        memberId: 1,
        name: 1,
        contactNumber: 1,
        age: 1,
        validUptoDate: 1,
        preferredSlot: 1,
        planChoice: 1,
        status: 1,
        avatar: 1
      })
      .sort({
        validUptoDate: 1 // Sort in ascending order by validUptoDate
      });

    res.json({ members: pendingMembers });
  } catch (error) {
    console.error('Error while retrieving members:', error);
    res.status(500).json({ error: 'An error occurred while retrieving members' });
  }
});


/*app.get('/sendPendingMembersEmail', async (req, res) => {
  try {
    const charCode = req.query.charCode;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_members`, memberSchema);

    // Retrieve pending members
    const pendingMembers = await Member.find({ status: 'Pending' });

    // Initialize a variable to count the emails
    let emailsSent = 0;

    // Send email to each pending member
    for (const member of pendingMembers) {
      

      let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");
   
    
    
        let ownerEmail=owner.email;
        const mailOptions = {
          from: 'your-email@gmail.com',
          to: member.email,
          subject: `Membership Renewal Reminder`,
          html: 
          `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>Pending members</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
    <![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
     h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
     .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
     .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
     .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
     .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
     </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
    <td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></td> </tr></table></td></tr></table></td></tr></table></td></tr></table> <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr>
    <td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Membership Renewal Reminder</h1> </td></tr><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Dear ${member.name},<br></p>
    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">

    This is a friendly reminder regarding your ${owner.gymName} membership.
    
    </p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Your membership fee is pending. Kindly ensure timely payment to avoid any disruption in your access to our fitness facilities. Your commitment to your fitness journey is highly valued, and we want to ensure a seamless experience for you at ${owner.gymName}</p>
   </td></tr></table></td></tr></table></td></tr></table></td></tr></table>
     <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr><td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr>
    <td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr><td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">We are delighted to have you on board and look forward to supporting you on your fitness journey. If you have any questions or need assistance, feel free to reach out to our team.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
    <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:+91 ${owner.number}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:+91 ${owner.number}" style="height:41px; v-text-anchor:middle; width:193px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>+91 ${owner.number}</center> </v:roundrect></a>
    <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:+91 ${owner.number}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 ${owner.number}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
    <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
    <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:${owner.email}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:${owner.email}" style="height:41px; v-text-anchor:middle; width:218px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>${owner.email}</center> </v:roundrect></a>
    <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:${owner.email}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">${owner.email}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
     <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
    <td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
    <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" href="mailto:info@lingualink.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px">${owner.email}</a></p></td></tr> <tr>
    <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+91 ${owner.number}</a></p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">${owner.street} ${owner.city} ${owner.pincode}</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
    <td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr>
    <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr>
    <td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`
        };
    
    
    
    
        
        
    
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            console.error('Error sending welcome email:', error);
            return res.status(500).json({ error: 'An error occurred while sending the welcome email' });
          }
    
          console.log('Welcome Email sent to member:', info.response);
        });

      emailsSent++;
    }

    res.json({ message: `Emails sent to ${emailsSent} pending members successfully.` });
  } catch (error) {
    console.error('Error while sending emails:', error);
    res.status(500).json({ error: 'An error occurred while sending emails' });
  }
});*/
app.get('/sendPendingMembersEmail', async (req, res) => {
  try {
    const charCode = req.query.charCode;

    if (!charCode) {
      return res.status(400).json({ error: 'Account not found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_members`, memberSchema);

    // Retrieve pending members using lean()
    const pendingMembers = await Member.find({ status: 'Pending' }).lean();

    // Reuse transporter
    const transporter = nodemailer.createTransport({
      // configure your transporter options
    });

    // Initialize a variable to count the emails
    let emailsSent = 0;

    // Use Promise.all to parallelize email sending
    await Promise.all(
      pendingMembers.map(async (member) => {
        let eid = await emailSequencing.getSequenceNextValueForEmail("email_id");

        const mailOptions = {
          from: 'your-email@gmail.com',
          to: member.email,
          subject: `Membership Renewal Reminder`,
          html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>Membership fee remainder</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
          <![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
           h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
           .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
           .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
           .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
           .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
           </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
          <td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
          <td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></td> </tr></table></td></tr></table></td></tr></table></td></tr></table> <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr>
          <td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
          <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Membership Renewal Reminder</h1> </td></tr><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Dear ${member.name},<br></p>
          <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">
      
          This is a friendly reminder regarding your ${owner.gymName} membership.
          
          </p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Your membership fee is pending. Kindly ensure timely payment to avoid any disruption in your access to our fitness facilities. Your commitment to your fitness journey is highly valued, and we want to ensure a seamless experience for you at ${owner.gymName}</p>
         </td></tr></table></td></tr></table></td></tr></table></td></tr></table>
           <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr><td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
          <td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr>
          <td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr><td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">We are delighted to have you on board and look forward to supporting you on your fitness journey. If you have any questions or need assistance, feel free to reach out to our team.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
          <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
          <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:+91 ${owner.number}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:+91 ${owner.number}" style="height:41px; v-text-anchor:middle; width:193px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>+91 ${owner.number}</center> </v:roundrect></a>
          <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:+91 ${owner.number}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 ${owner.number}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
          <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
          <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:${owner.email}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:${owner.email}" style="height:41px; v-text-anchor:middle; width:218px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>${owner.email}</center> </v:roundrect></a>
          <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:${owner.email}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">${owner.email}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
           <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
          <td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
          <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" href="mailto:info@lingualink.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px">${owner.email}</a></p></td></tr> <tr>
          <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+91 ${owner.number}</a></p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">${owner.street} ${owner.city} ${owner.pincode}</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
          <td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr>
          <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr>
          <td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`,
        };

        try {
          // Send email
          await sendMailAsync(mailOptions);
          emailsSent++;
        } catch (error) {
          console.error('Error sending email:', error);
        }
      })
    );

    res.json({ message: `Emails sent to ${emailsSent} pending members successfully.` });
  } catch (error) {
    console.error('Error while sending emails:', error);
    res.status(500).json({ error: 'An error occurred while sending emails' });
  }
});


app.get('/history', async (req, res) => {
  try {
      const charCode = req.query.charCode;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });
    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const History = new mongoose.model(`${charCode}_history`, historySchema);

    const members = await History.find().sort({ id: -1 }); // Sort in descending order by memberId
     

    res.json({ members });
  } catch (error) {
    console.error('Error while retrieving history', error);
    res.status(500).json({ error: 'An error occurred while retrieving history' });
  }
});

app.get('/fee-Tracking', async (req, res) => {
  try {
    // Check if charCode exists in cookies
    const charCode = req.cookies.charCode;

    if (!charCode) {
      return res.status(400).json({ error: 'CharCode not found in cookies' });
    }

    // Find all members belonging to the owner who haven't paid their fees
    const currentDate = new Date();
    const currentTimestamp = currentDate.getTime();

    const unpaidMembers = await User.find({
      memberId: { $regex: `^${charCode}_` },
      lastRenewalDate: { $lt: currentTimestamp },
    });

    // Generate rows for each pending date
    const formattedUnpaidRows = [];

    unpaidMembers.forEach(member => {
      const lastRenewalDate = new Date(member.lastRenewalDate);
      const currentDate = new Date();

      while (lastRenewalDate < currentDate) {
        lastRenewalDate.setMonth(lastRenewalDate.getMonth() + 1);
        if (lastRenewalDate < currentDate) {
          const formattedRow = {
            memberId: member.memberId,
            name: member.name,
            mobile: member.contactNumber,
            pendingMonth: `${lastRenewalDate.getDate()}/${lastRenewalDate.getMonth() + 1}/${lastRenewalDate.getFullYear()}`,
            amount: member.membershipFee,
          };
          formattedUnpaidRows.push(formattedRow);
        }
      }
    });

    res.json({ unpaidRows: formattedUnpaidRows });
  } catch (error) {
    console.error('Error while retrieving unpaid members:', error);
    res.status(500).json({ error: 'An error occurred while retrieving unpaid members' });
  }
});

app.get('/member/:memberId', async (req, res) => {
  try {
    const charCode = req.query.charCode;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_members`, memberSchema);
    const memberId = req.params.memberId;

    if (!memberId) {
      return res.status(400).json({ error: 'Please provide a member ID' });
    }

    const member = await Member.findOne({ memberId });

    if (!member) {
      return res.status(404).json({ error: 'Member not found' });
    }

    // You can customize the fields you want to retrieve and send in the response
    const memberData = {
      memberId: member.memberId,
      name: member.name,
      contactNumber: member.contactNumber,
      age: member.age,
      validUptoDate: member.validUptoDate,
      status: member.status,
      avatar: member.avatar
      // Add more fields as needed
    };

    res.json({ member: memberData });
  } catch (error) {
    console.error('Error while retrieving member data:', error);
    res.status(500).json({ error: 'An error occurred while retrieving member data' });
  }
});

app.get('/events', async (req, res) => {
  try {
    const charCode = req.query.charCode;
    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }
    const owner = await Owner.findOne({ charCode });
    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }
    const Event = new mongoose.model(`${charCode}_events`, eventSchema);
    const events = await Event.find().select({
      title: 1,
      description: 1,
      date: 1,
      time: 1,
      sentMailStatus: 1,
      timestamp: 1
    }).sort({ id: -1 }); // Sort in descending order by memberId
     

    res.json({ events });
  } catch (error) {
    console.error('Error while retrieving members:', error);
    res.status(500).json({ error: 'An error occurred while retrieving members' });
  }
});

app.get('/expenditures', async (req, res) => {
  try {
    const charCode = req.query.charCode;
    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }
    const owner = await Owner.findOne({ charCode });
    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }
    const Expenditure = new mongoose.model(`${charCode}_expenditures`, expenditureSchema);
    const events = await Expenditure.find().sort({ id: -1 }); // Sort in descending order by memberId
     

    res.json({ events });
  } catch (error) {
    console.error('Error while retrieving expenditures', error);
    res.status(500).json({ error: 'An error occurred while retrieving expenditures' });
  }
});

function formatTimeAMPM(time24) {
  // Parse the input time string
  const [hours, minutes] = time24.split(':').map(Number);

  // Check if the time is in the AM or PM
  const period = hours >= 12 ? 'PM' : 'AM';

  // Convert hours to 12-hour format
  const hours12 = hours % 12 || 12;

  // Format the time in AM/PM
  const formattedTime = `${hours12}:${minutes.toString().padStart(2, '0')} ${period}`;

  return formattedTime;
}



// app.post('/addEvent', async (req, res) => {
//   try {
//     const { charCode, title, description, date, time, locationAddr } = req.body;

//     if (!charCode) {
//       return res.status(400).json({ error: 'CharCode not found in cookies' });
//     }
//     const Event = mongoose.model(`${charCode}_events`, eventSchema);


//     // Check if charCode exists in owners
//     const owner = await Owner.findOne({ charCode });

//     if (!owner) {
//       return res.status(404).json({ error: 'Account not found. Please Login!' });
//     }

//     // Generate memberId using charCode as prefix
//     let id = await sequencing.getSequenceNextValue("user_id");

//     const newEvent = new Event({
//       id,
//       title,
//       description,
//       date,
//       time,
//       locationAddr,
//       sentMailStatus:0
//     });

//     await newEvent.save();

//     // Get members
//     const Member = mongoose.model(`${charCode}_members`, memberSchema);
//     const members = await Member.find({}, {
//       email: 1
//     });

//     let eid= await emailSequencing.getSequenceNextValueForEmail("email_id");
//     // Send email to each member
//     const mailOptions = {
//       from: 'your-email@gmail.com',
//       subject: `Event Reminder ${owner.gymName}`,
//       html: 
//       `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>Events</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
// <![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
//  h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
//  .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
//  .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
//  .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
//  .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
//  </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
// <td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
// <td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></td> </tr></table></td></tr></table></td></tr></table></td></tr></table> <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr>
// <td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
// <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Event Reminder</h1> </td></tr><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p>
// <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Title : ${newEvent.title}</p>
// <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Description : ${newEvent.description}</p>
// <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Date : ${formatDate(newEvent.date)}</p>
// <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Time : ${formatTimeAMPM(newEvent.time)}</p>

// <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Location : ${newEvent.locationAddr}</p>

// </td></tr></table></td></tr></table></td></tr></table></td></tr></table>
//  <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr><td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
// <td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr>
// <td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr><td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">We are delighted to have you on board and look forward to supporting you on your fitness journey. If you have any questions or need assistance, feel free to reach out to our team.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
// <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
// <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:+91 ${owner.number}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:+91 ${owner.number}" style="height:41px; v-text-anchor:middle; width:193px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>+91 ${owner.number}</center> </v:roundrect></a>
// <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:+91 ${owner.number}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 ${owner.number}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
// <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
// <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:${owner.email}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:${owner.email}" style="height:41px; v-text-anchor:middle; width:218px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>${owner.email}</center> </v:roundrect></a>
// <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:${owner.email}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">${owner.email}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
//  <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
// <td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
// <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" href="mailto:info@lingualink.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px">${owner.email}</a></p></td></tr> <tr>
// <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+91 ${owner.number}</a></p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">${owner.street} ${owner.city} ${owner.pincode}</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
// <td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr>
// <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr>
// <td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`
//     };

//     let sentMailCount = 0;

//     for (const member of members) {
//       mailOptions.to = member.email;
//       console.log("hskdjf ");

//       try {
//         await transporter.sendMail(mailOptions);
//         sentMailCount++;
//         console.log("hello");
//       } catch (error) {
//         console.error(`Error sending email to ${member.email}:`, error);
//       }
//     }

//     // Update the sentMail field in the event after all emails are sent
//     await Event.updateOne({ _id: newEvent._id }, { $set: { sentMailStatus: sentMailCount } });

//     res.json({ message: 'Event added successfully and emails sent to '+sentMailCount+' members successfully' });
//   } catch (error) {
//     console.error('Error while adding event and sending emails:', error);
//     res.status(500).json({ error: 'An error occurred while adding the event and sending emails' });
//   }
// });


app.post('/addEvent', async (req, res) => {
  try {
    const { charCode, title, description, date, time, locationAddr } = req.body;

    if (!charCode) {
      return res.status(400).json({ error: 'CharCode not found in cookies' });
    }

    // Check if charCode exists in owners
    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Event = mongoose.model(`${charCode}_events`, eventSchema);
    const Member = mongoose.model(`${charCode}_members`, memberSchema);

    // Generate memberId using charCode as prefix
    let id = await sequencing.getSequenceNextValue("user_id");

    const newEvent = new Event({
      id,
      title,
      description,
      date,
      time,
      locationAddr,
      sentMailStatus: 0
    });

    await newEvent.save();

    // Get members and send emails concurrently
    let eid = await emailSequencing.getSequenceNextValueForEmail("email_id");

    const members = await Member.find({}, { email: 1 });
    const emailPromises = members.map(async (member) => {
      const mailOptions = {
        from: 'your-email@gmail.com',
        to: member.email,
        subject: `Event Reminder ${owner.gymName}`,
        html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="en"><head><meta charset="UTF-8"><meta content="width=device-width, initial-scale=1" name="viewport"><meta name="x-apple-disable-message-reformatting"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta content="telephone=no" name="format-detection"><title>Events</title> <!--[if (mso 16)]><style type="text/css">     a {text-decoration: none;}     </style><![endif]--> <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> <!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:AllowPNG></o:AllowPNG> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings> </xml>
        <![endif]--> <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css2?family=Montaga&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css2?family=Lexend&display=swap" rel="stylesheet"> <!--<![endif]--><style type="text/css">#outlook a { padding:0;}.es-button { mso-style-priority:100!important; text-decoration:none!important;}a[x-apple-data-detectors] { color:inherit!important; text-decoration:none!important; font-size:inherit!important; font-family:inherit!important; font-weight:inherit!important; line-height:inherit!important;}.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0; mso-hide:all;} @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120%!important } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left }
         h3 { font-size:20px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important; text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important; text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important; text-align:left } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important }
         .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:inline-block!important } a.es-button, button.es-button { font-size:18px!important; display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0!important } .es-m-p0r { padding-right:0!important } .es-m-p0l { padding-left:0!important }
         .es-m-p0t { padding-top:0!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; max-height:inherit!important } .es-m-p5 { padding:5px!important } .es-m-p5t { padding-top:5px!important }
         .es-m-p5b { padding-bottom:5px!important } .es-m-p5r { padding-right:5px!important } .es-m-p5l { padding-left:5px!important } .es-m-p10 { padding:10px!important } .es-m-p10t { padding-top:10px!important } .es-m-p10b { padding-bottom:10px!important } .es-m-p10r { padding-right:10px!important } .es-m-p10l { padding-left:10px!important } .es-m-p15 { padding:15px!important } .es-m-p15t { padding-top:15px!important } .es-m-p15b { padding-bottom:15px!important } .es-m-p15r { padding-right:15px!important } .es-m-p15l { padding-left:15px!important } .es-m-p20 { padding:20px!important } .es-m-p20t { padding-top:20px!important } .es-m-p20r { padding-right:20px!important } .es-m-p20l { padding-left:20px!important } .es-m-p25 { padding:25px!important } .es-m-p25t { padding-top:25px!important } .es-m-p25b { padding-bottom:25px!important } .es-m-p25r { padding-right:25px!important } .es-m-p25l { padding-left:25px!important }
         .es-m-p30 { padding:30px!important } .es-m-p30t { padding-top:30px!important } .es-m-p30b { padding-bottom:30px!important } .es-m-p30r { padding-right:30px!important } .es-m-p30l { padding-left:30px!important } .es-m-p35 { padding:35px!important } .es-m-p35t { padding-top:35px!important } .es-m-p35b { padding-bottom:35px!important } .es-m-p35r { padding-right:35px!important } .es-m-p35l { padding-left:35px!important } .es-m-p40 { padding:40px!important } .es-m-p40t { padding-top:40px!important } .es-m-p40b { padding-bottom:40px!important } .es-m-p40r { padding-right:40px!important } .es-m-p40l { padding-left:40px!important } }@media screen and (max-width:384px) {.mail-message-content { width:414px!important } }</style>
         </head> <body style="width:100%;font-family:Lexend, Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0"><div dir="ltr" class="es-wrapper-color" lang="en" style="background-color:#EDEDED"> <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#EDEDED"></v:fill> </v:background><![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#EDEDED"><tr>
        <td valign="top" style="padding:0;Margin:0"><table class="es-header" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-header-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2C2C2C;width:600px"><tr><td align="left" style="padding:5px;Margin:0"><table cellspacing="0" cellpadding="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
        <td class="es-m-p0r" valign="top" align="center" style="padding:0;Margin:0;width:590px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="https://ffbkvms.stripocdn.email/content/guids/CABINET_0f809f53c322ad839de1e55f09574f772646265ea6b33f25b0ea3558952430b8/images/ff.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="350" height="93"></td> </tr></table></td></tr></table></td></tr></table></td></tr></table> <table class="es-content" cellspacing="0" cellpadding="0" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr>
        <td align="center" style="padding:0;Margin:0"><table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:30px;padding-right:30px"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0;width:540px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
        <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Event Reminder</h1> </td></tr><tr><td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p>
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Title : ${newEvent.title}</p>
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Description : ${newEvent.description}</p>
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Date : ${formatDate(newEvent.date)}</p>
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Time : ${formatTimeAMPM(newEvent.time)}</p>
        
        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><br></p> <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Location : ${newEvent.locationAddr}</p>
        
        </td></tr></table></td></tr></table></td></tr></table></td></tr></table>
         <table cellpadding="0" cellspacing="0" class="es-content" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%"><tr><td align="center" style="padding:0;Margin:0"><table bgcolor="#2C2C2C" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#2c2c2c;width:600px" role="none"><tr><td align="left" style="padding:10px;Margin:0"><table cellpadding="0" cellspacing="0" width="100%" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
        <td align="center" valign="top" style="padding:0;Margin:0;width:580px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0px"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/guids/CABINET_26f7726bcadd97a2ab03d54f86a8b630ca314b98bfcd8c5969ac3c828e7d2b89/images/question.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="50" height="50"></a> </td></tr><tr>
        <td align="center" class="es-m-txt-c" style="padding:0;Margin:0;padding-bottom:15px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#ffffff">We here to help</h1></td></tr><tr><td align="center" class="es-m-p0r es-m-p0l" style="padding:0;Margin:0;padding-left:40px;padding-right:40px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#ffffff;font-size:14px">We are delighted to have you on board and look forward to supporting you on your fitness journey. If you have any questions or need assistance, feel free to reach out to our team.</p></td></tr></table></td></tr></table></td></tr> <tr><td align="left" style="padding:0;Margin:0;padding-bottom:10px;padding-left:30px;padding-right:30px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
        <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-left" align="left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
        <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="tel:+91 ${owner.number}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="tel:+91 ${owner.number}" style="height:41px; v-text-anchor:middle; width:193px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>+91 ${owner.number}</center> </v:roundrect></a>
        <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="tel:+91 ${owner.number}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">+91 ${owner.number}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
        <td style="width:260px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:260px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr>
        <td align="center" style="padding:0;Margin:0"> <!--[if mso]><a href="mailto:${owner.email}" target="_blank" hidden> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" esdevVmlButton href="mailto:${owner.email}" style="height:41px; v-text-anchor:middle; width:218px" arcsize="50%" stroke="f" fillcolor="#ff4a4a"> <w:anchorlock></w:anchorlock> <center style='color:#ffffff; font-family:Lexend, Arial, sans-serif; font-size:15px; font-weight:400; line-height:15px; mso-text-raise:1px'>${owner.email}</center> </v:roundrect></a>
        <![endif]--> <!--[if !mso]><!-- --><span class="msohide es-button-border" style="border-style:solid;border-color:#2CB543;background:#FF4A4A;border-width:0px;display:inline-block;border-radius:30px;width:auto;mso-hide:all"><a href="mailto:${owner.email}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#FFFFFF;font-size:18px;display:inline-block;background:#FF4A4A;border-radius:30px;font-family:Lexend, Arial, sans-serif;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;padding:10px 30px 10px 30px;mso-padding-alt:0;mso-border-alt:10px solid #FF4A4A">${owner.email}</a> </span> <!--<![endif]--></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table>
         <table cellpadding="0" cellspacing="0" class="es-footer" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top"><tr><td align="center" style="padding:0;Margin:0"><table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#939393;width:600px"><tr><td align="left" style="Margin:0;padding-left:30px;padding-right:30px;padding-top:35px;padding-bottom:35px"> <!--[if mso]><table style="width:540px" cellpadding="0" cellspacing="0"><tr>
        <td style="width:235px" valign="top"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="es-left" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left"><tr><td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:235px"><table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Contact Us</h1> </td></tr><tr>
        <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-top:15px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" href="mailto:info@lingualink.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px">${owner.email}</a></p></td></tr> <tr>
        <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#2C2C2C;font-size:14px" href="tel:+(000)123456789">+91 ${owner.number}</a></p></td></tr><tr><td align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">${owner.street} ${owner.city} ${owner.pincode}</p></td></tr></table></td></tr></table> <!--[if mso]></td><td style="width:20px"></td>
        <td style="width:285px" valign="top"><![endif]--><table cellpadding="0" cellspacing="0" class="es-right" align="right" role="none" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right"><tr><td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0;padding-bottom:5px"><h1 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Montaga, Arial, serif;font-size:30px;font-style:normal;font-weight:normal;color:#2C2C2C">Follow Us&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</h1></td></tr> <tr>
        <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;font-size:0"><table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="center" valign="top" style="padding:0;Margin:0"><a target="_blank" href="https://www.instagram.com/${owner.insta}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#2C2C2C;font-size:14px"><img src="https://ffbkvms.stripocdn.email/content/assets/img/social-icons/rounded-black/instagram-rounded-black.png" alt="Ig" title="Instagram" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a></td></tr></table></td></tr></table></td></tr> <tr>
        <td align="left" style="padding:0;Margin:0;width:285px"><table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"><tr><td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Lexend, Arial, sans-serif;line-height:21px;color:#2C2C2C;font-size:14px">Ref : ${eid}</p></td></tr></table></td></tr></table> <!--[if mso]></td></tr></table><![endif]--></td></tr></table></td></tr></table></td></tr></table></div></body></html>`
      };

      try {
        await sendMailAsync(mailOptions); // Assuming you have a sendMailAsync function
        return 1; // Success flag
      } catch (error) {
        console.error(`Error sending email to ${member.email}:`, error);
        return 0; // Error flag
      }
    });

    // Wait for all emails to be sent
    const emailResults = await Promise.all(emailPromises);

    // Count successful email sends
    const sentMailCount = emailResults.reduce((count, result) => count + result, 0);

    // Update the sentMailStatus for the event
    await Event.updateOne({ _id: newEvent._id }, { $inc: { sentMailStatus: sentMailCount } });

    res.json({ message: `Event added successfully and emails sent to ${sentMailCount} members successfully` });
  } catch (error) {
    console.error('Error while adding event and sending emails:', error);
    res.status(500).json({ error: 'An error occurred while adding the event and sending emails' });
  }
});




app.post('/addExpenditure', async (req, res) => {
  try {
    const { charCode, title, description, amount} = req.body;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }
    const Expenditure = mongoose.model(`${charCode}_expenditures`, expenditureSchema);


    // Check if charCode exists in owners
    const owner = await Owner.findOne({ charCode });

    console.log(amount);

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    // Generate memberId using charCode as prefix
    let id = await sequencing.getSequenceNextValue("user_id");

    const newEvent = new Expenditure({
      id,
      title,
      description,
      amount
    });

    await newEvent.save();

    res.json({ message: 'Expenditure added successfully' });
  } catch (error) {
    console.error('Error while adding expenditure', error);
    res.status(500).json({ error: 'An error occurred while adding the expenditure' });
  }
});


app.get('/profile', async (req, res) => {
  try {
    const charCode = req.query.charCode;
    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }
    const owner = await Owner.findOne({ charCode }).select({
      ownerId: 1,
        name: 1,
        gymName: 1,
        email: 1,
        number: 1,

        charCode: 1,
        city: 1,
        street: 1,
        pincode:1,
        subscriptionType: 1,
        insta: 1,
        subscriptionExpireDate: 1,
    });
    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }
    res.json({ owner });
  } catch (error) {
    console.error('Error while retrieving members:', error);
    res.status(500).json({ error: 'An error occurred while retrieving members' });
  }
});

app.get('/deactivatedMembers', async (req, res) => {
  try {
    const charCode = req.query.charCode;

    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_members`, memberSchema);
    const DeactivatedMember = mongoose.model(`${charCode}_deactivated`, deactivatedSchema);
    const id = await sequencing.getSequenceNextValue("user_id");

    // Get the current date
    const currentDate = moment();

    // Find members whose validUptoDate is 3 months ago from now
    const membersToDeactivate = await Member.find({
      validUptoDate: { $lt: moment().subtract(3, 'months') }
    });

    const History = new mongoose.model(`${charCode}_history`, historySchema);

    // Process each member and add to deactivated collection, then delete from members table
    for (const member of membersToDeactivate) {
      // Calculate monthsNotPaid
      const validUptoDate = moment(member.validUptoDate);
      const monthsNotPaid = currentDate.diff(validUptoDate, 'months');




      // Add the member to the deactivated collection
      await DeactivatedMember.create({
        avatar: member.avatar,
        memberId: member.memberId,
        status: 'Deactivated',
        name: member.name,
        admissionDate: member.admissionDate,
        lastRenewalDate: member.lastRenewalDate,
        validUptoDate: member.validUptoDate,
        email: member.email,
        contactNumber: member.contactNumber,
        age: member.age,
        gender: member.gender,
        planChoice: member.planChoice,
        membershipFee: member.membershipFee,
        preferredSlot: member.preferredSlot,
        services: member.services,
        address: member.address,
        monthsNotPaid: monthsNotPaid,
        monthYear: member.monthYear,
        deactivatedDate: currentDate
      });


      const historyRecord = new History({
        id,
        memberId: member.memberId,
        name: member.name,
        contactNumber: member.contactNumber,
        action: 'Deactivated',
        planChoice: member.planChoice,
  
        membershipFee:0,
        validUptoDate:member.validUptoDate,
        // Use member data for contactNumber
       
        
        paymentMethod: member.paymentMethod,
        services:member.services,
  
  
        avatar: member.avatar,
        paidFor: member.lastRenewalDate,
        preferredSlot: member.preferredSlot,
      });
  
      await historyRecord.save();
      // Delete the member from the members table
      await Member.deleteOne({ memberId: member.memberId });
    }

    

    // Retrieve deactivated members
    const deactivatedMembers = await DeactivatedMember.find()
      .sort({
        deactivatedDate: -1 // Sort in descending order by deactivatedDate
      });

    res.json({ members: deactivatedMembers });
  } catch (error) {
    console.error('Error while retrieving deactivated members:', error);
    res.status(500).json({ error: 'An error occurred while retrieving deactivated members' });
  }
});

app.get('/financial-strategy', async (req, res) => {
  try {
    const { charCode, expectedMonthlySalary } = req.query;
    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });

    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const Member = new mongoose.model(`${charCode}_Members`, memberSchema);
    const History = new mongoose.model(`${charCode}_History`, historySchema);
    const Expenditure = new mongoose.model(`${charCode}_Expenditure`, expenditureSchema);

    const monthlyRevenue = await History.aggregate([
      {
        $group: {
          _id: '$monthYear',
          totalMembershipFee: { $sum: '$membershipFee' },
          totalMembers: { $sum: 1 },
        },
      },
      {
        $project: {
          monthYear: '$_id',
          totalMembershipFee: 1,
          totalMembers: 1,
          _id: 0,
        },
      },
      {
        $sort: {
          monthYear: 1,
        },
      },
    ]);

    const totalRevenue = Math.round(monthlyRevenue.reduce((acc, month) => acc + month.totalMembershipFee, 0));
    const totalMonths = monthlyRevenue.length;
    const avgMonthlyRevenue = Math.round(totalRevenue / totalMonths);

    const totalExpenditure = await Expenditure.aggregate([
      { $group: { _id: null, totalAmount: { $sum: '$amount' } } }
    ]).then(result => (result[0] ? result[0].totalAmount : 0))
      .catch(err => {
        console.error('Error during expenditure aggregation:', err);
        return 0;
      });

    const avgMonthlyExpenditure = Math.round(totalExpenditure / totalMonths);
    const totalProfit = Math.round(totalRevenue - totalExpenditure);
    const avgMonthlyProfit = Math.round(totalProfit / totalMonths);

    const totalMembers = await Member.countDocuments({});
    const extra = expectedMonthlySalary - avgMonthlyProfit;

    let shouldChargeExtra = "";
    if (extra > 0) {
      shouldChargeExtra = "Yes";
    } else {
      shouldChargeExtra = "No";
    }
    const howMuchFromEachMember = Math.round(extra / totalMembers);

    res.json({
      success: true,
      message: 'Financial strategy calculation successful.',
      data: {
        totalRevenue,
        avgMonthlyRevenue,
        totalExpenditure,
        avgMonthlyExpenditure,
        totalProfit,
        avgMonthlyProfit,
        totalMembers,
        totalMonths,
        expectedMonthlySalary,
        shouldChargeExtra,
        monthlyRevenue,
        howMuchFromEachMember
      },
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, error: 'Internal Server Error' });
  }
});







































//overview
async function totalMembers(charCode) {
  try {
    const Member = mongoose.model(`${charCode}_members`, memberSchema);
    const History = mongoose.model(`${charCode}_history`, historySchema);

    const currentYear = new Date().getFullYear();
    const lastYear = currentYear - 1;

    // Count members and pending members
    const [count, pendingCount] = await Promise.all([
      Member.countDocuments(),
      Member.countDocuments({ status: 'Pending' }),
    ]);

    // Count the number of plan choices for the current year and last year
    const [planChoiceCountsCurrentYear, planChoiceCountsLastYear] = await Promise.all([
      Member.aggregate([
        { $match: { monthYear: { $regex: new RegExp(`${currentYear}`), $options: 'i' } } },
        { $group: { _id: '$planChoice', count: { $sum: 1 } } },
      ]),
      Member.aggregate([
        { $match: { monthYear: { $regex: new RegExp(`${lastYear}`), $options: 'i' } } },
        { $group: { _id: '$planChoice', count: { $sum: 1 } } },
      ]),
    ]);

    const planChoiceCountMapCurrentYear = new Map(planChoiceCountsCurrentYear.map(({ _id, count }) => [_id, count]));
    const planChoiceCountMapLastYear = new Map(planChoiceCountsLastYear.map(({ _id, count }) => [_id, count]));

    // Count the number of male and female members
    const genderCounts = await Member.aggregate([
      { $group: { _id: '$gender', count: { $sum: 1 } } },
    ]);
    
    const totalCountgender = genderCounts.reduce((total, { count }) => total + count, 0);
    
    const genderCountMap = new Map(
      genderCounts.map(({ _id, count }) => [String(_id), Math.round((count / totalCountgender * 100))])
    );

    // Count the number of active and pending members
    const activePendingCounts = await Member.aggregate([
      { $group: { _id: '$status', count: { $sum: 1 } } },
    ]);

    const totalCountActivePending = activePendingCounts.reduce((total, { count }) => total + count, 0);

    const activePendingCountMap = new Map(
      activePendingCounts.map(({ _id, count }) => [String(_id), Math.round((count / totalCountActivePending * 100))])
    );

    const activeP = activePendingCountMap.get("Active") || 5;
    const pendingP = activePendingCountMap.get("Pending") || 0.5;

    // Convert values to numbers
    for (const [key, value] of genderCountMap.entries()) {
      genderCountMap.set(key, Number(value));
    }

    // Count the number of members for each monthYear for the current year and last year
    const [monthYearCountsCurrentYear, monthYearCountsLastYear] = await Promise.all([
      Member.aggregate([
        { $match: { monthYear: { $regex: new RegExp(`${currentYear}`), $options: 'i' } } },
        { $group: { _id: '$monthYear', count: { $sum: 1 } } },
      ]),
      Member.aggregate([
        { $match: { monthYear: { $regex: new RegExp(`${lastYear}`), $options: 'i' } } },
        { $group: { _id: '$monthYear', count: { $sum: 1 } } },
      ]),
    ]);

    const monthYearCountMapCurrentYear = new Map(monthYearCountsCurrentYear.map(({ _id, count }) => [_id, count]));
    const monthYearCountMapLastYear = new Map(monthYearCountsLastYear.map(({ _id, count }) => [_id, count]));

    // Count the number of members for each age group
    const ageGroupCounts = await Member.aggregate([
      {
        $group: {
          _id: {
            $switch: {
              branches: [
                { case: { $and: [{ $gte: ['$age', 10] }, { $lte: ['$age', 18] }] }, then: '10-18' },
                { case: { $and: [{ $gt: ['$age', 18] }, { $lte: ['$age', 24] }] }, then: '18-24' },
                { case: { $gt: ['$age', 24] }, then: '>24' },
              ],
              default: 'Unknown',
            },
          },
          count: { $sum: 1 },
        },
      },
    ]);

    const totalCount = ageGroupCounts.reduce((total, group) => total + group.count, 0);

    // Calculate and output the percentages in a consolidated object with rounded values
    const ageGroupPercentages = {};
    ageGroupCounts.forEach(group => {
      ageGroupPercentages[group._id] = Math.round((totalCount > 0 ? (group.count / totalCount) * 100 : 0));
    });

    // Count the number of members for each preferred slot for the current year and last year
    const [preferredSlotCountsCurrentYear, preferredSlotCountsLastYear] = await Promise.all([
      Member.aggregate([
        { $match: { monthYear: { $regex: new RegExp(`${currentYear}`), $options: 'i' } } },
        { $group: { _id: '$preferredSlot', count: { $sum: 1 } } },
      ]),
      Member.aggregate([
        { $match: { monthYear: { $regex: new RegExp(`${lastYear}`), $options: 'i' } } },
        { $group: { _id: '$preferredSlot', count: { $sum: 1 } } },
      ]),
    ]);

    const preferredSlotCountMapCurrentYear = new Map(preferredSlotCountsCurrentYear.map(({ _id, count }) => [_id, count]));
    const preferredSlotCountMapLastYear = new Map(preferredSlotCountsLastYear.map(({ _id, count }) => [_id, count]));



    return [
      count,
      pendingCount,
      planChoiceCountMapCurrentYear,
      planChoiceCountMapLastYear,
      genderCountMap,
      monthYearCountMapCurrentYear,
      monthYearCountMapLastYear,
      ageGroupPercentages,
      preferredSlotCountMapCurrentYear,
      preferredSlotCountMapLastYear,
      activeP,
      pendingP,
    ];
  } catch (error) {
    console.error('Error in totalMembers function:', error);
    throw error;
  }
}

async function totalRevenueCurrYear(charCode) {
  try {
    const History = mongoose.model(`${charCode}_history`, historySchema);

    const currentYear = new Date().getFullYear();

    // Calculate total revenue for the current year using aggregation
    const totalRevenueCurrYear = await History.aggregate([
      { $match: { monthYear: { $regex: new RegExp(`${currentYear}`), $options: 'i' } } },
      { $group: { _id: null, totalRevenueCurrYear: { $sum: '$membershipFee' } } },
      { $project: { _id: 0, totalRevenueCurrYear: 1 } },
    ]);

    return totalRevenueCurrYear.length > 0 ? totalRevenueCurrYear[0].totalRevenueCurrYear : 0;
  } catch (error) {
    console.error('Error in totalRevenueCurrYear function:', error);
    throw error;
  }
}

async function getServiceCounts(charCode) {
  try {
    const Member = mongoose.model(`${charCode}_members`, memberSchema);

    const serviceCounts = await Member.aggregate([
      { $unwind: '$services' }, // Break the array of services into separate documents
      {
        $group: {
          _id: '$services',
          count: { $sum: 1 },
        },
      },
    ]);

    const serviceCountMap = new Map(serviceCounts.map(({ _id, count }) => [_id, count]));

    return Object.fromEntries(serviceCountMap);
  } catch (error) {
    console.error('Error in getServiceCounts function:', error);
    throw error;
  }
}


async function totalExpenditureCurrYear(charCode) {
  try {
    const Expenditure = mongoose.model(`${charCode}_expenditure`, expenditureSchema);

    // Get the current year
    const currentYear = new Date().getFullYear();

    // Find members with monthYear in the current year
    const expenditures = await Expenditure.find({
      monthYear: { $regex: new RegExp(`${currentYear}`), $options: 'i' },
    });

    // Calculate total revenue for the current year
    const totalExpenditureCurrYear = expenditures.reduce((total, expenditure) => {
      return total + (expenditure.amount || 0);
    }, 0);
    return totalExpenditureCurrYear;
  } catch (error) {
    // Handle any potential errors, e.g., log or throw the error
    console.error('Error in totalRevenueCurrYear function:', error);
    throw error;
  }
}
async function calculateExpenditurePercentage(charCode, totalRevenue) {
  try {
    const Expenditure = mongoose.model(`${charCode}_expenditure`, expenditureSchema);

    // Get the current year
    const currentYear = new Date().getFullYear();

    // Find expenditures with monthYear in the current year
    const expenditures = await Expenditure.find({
      monthYear: { $regex: new RegExp(`${currentYear}`), $options: 'i' },
    });

    // Calculate total expenditure for the current year
    const totalExpenditureCurrYear = expenditures.reduce((total, expenditure) => {
      return total + (expenditure.amount || 0);
    }, 0);

    // Calculate the percentage of expenditure from total revenue
    const expenditurePercentage = (totalExpenditureCurrYear / totalRevenue) * 100;

    return expenditurePercentage;
  } catch (error) {
    // Handle any potential errors, e.g., log or throw the error
    console.error('Error in calculateExpenditurePercentage function:', error);
    throw error;
  }
}

app.get('/overview', async (req, res) => {
  try {
    const charCode = req.query.charCode;
    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });
    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const [
      count,
      pendingCount,
      planChoiceCountMapCurrentYear,
      planChoiceCountMapLastYear,
      genderCountMap,
      monthYearCountMapCurrentYear,
      monthYearCountMapLastYear,
      ageGroupPercentages,

      preferredSlotCountMapCurrentYear,
      preferredSlotCountMapLastYear,
      activeP,
      pendingP,

      
    ] = await totalMembers(charCode);

    const totalRevenueCurrYearVal = await totalRevenueCurrYear(charCode);
    const totalExpenditureCurrYearVal = await totalExpenditureCurrYear(charCode);
    const expenditurePercentageCurrYearVal = ((totalExpenditureCurrYearVal / totalRevenueCurrYearVal) * 100).toFixed(2);
    const totalProfitCurrYear = totalRevenueCurrYearVal - totalExpenditureCurrYearVal;
    const recentMembers = await getRecentMembers(charCode);
    const serviceCounts = await getServiceCounts(charCode);

    res.json({
      count,
      pendingCount,
      planChoiceCountMapCurrentYear : Object.fromEntries(planChoiceCountMapCurrentYear),
      planChoiceCountMapLastYear : Object.fromEntries(planChoiceCountMapLastYear),
      genderCountMap: Object.fromEntries(genderCountMap),
      monthYearCountMapCurrentYear: Object.fromEntries(monthYearCountMapCurrentYear),
      monthYearCountMapLastYear: Object.fromEntries(monthYearCountMapLastYear),
      ageGroupPercentages,
      preferredSlotCountMapCurrentYear: Object.fromEntries(preferredSlotCountMapCurrentYear),
      preferredSlotCountMapLastYear: Object.fromEntries(preferredSlotCountMapLastYear),
      totalRevenueCurrYearVal,
      totalExpenditureCurrYearVal,
      expenditurePercentageCurrYearVal,
      totalProfitCurrYear,
      recentMembers,
      serviceCounts,
      ageGroupPercentages,
      activeP,
      pendingP,

 
    });
  } catch (error) {
    console.error('Error while retrieving members:', error);
    res.status(500).json({ error: 'An error occurred while retrieving members' });
  }
});

const { differenceInSeconds, differenceInMinutes, differenceInHours, differenceInDays } = require('date-fns');

async function getRecentMembers(charCode) {
  try {
    const Member = mongoose.model(`${charCode}_members`, memberSchema);

    // Get the 5 most recent members with name and avatar fields
    const recentMembers = await Member.find({}, { name: 1, avatar: 1, _id: 1 })
      .sort({ _id: -1 }) // Sort in descending order based on _id to get the most recent records first
      .limit(5); // Limit the result to 5 records

    const now = new Date();

    // Calculate the time difference for each recent member using the _id field
    const recentMembersWithTimeDiff = recentMembers.map(member => {
      const insertedAt = member._id.getTimestamp(); // Extract the timestamp from the _id field

      // Calculate time difference in seconds, minutes, hours, and days
      const secondsDifference = differenceInSeconds(now, insertedAt);
      const minutesDifference = differenceInMinutes(now, insertedAt);
      const hoursDifference = differenceInHours(now, insertedAt);
      const daysDifference = differenceInDays(now, insertedAt);

      // Choose the appropriate unit based on the time difference
      let timeDifference;
      if (daysDifference > 0) {
        timeDifference = `${daysDifference} day${daysDifference > 1 ? 's' : ''}`;
      } else if (hoursDifference > 0) {
        timeDifference = `${hoursDifference} hour${hoursDifference > 1 ? 's' : ''}`;
      } else if (minutesDifference > 0) {
        timeDifference = `${minutesDifference} minute${minutesDifference > 1 ? 's' : ''}`;
      } else {
        timeDifference = `${secondsDifference} second${secondsDifference > 1 ? 's' : ''}`;
      }

      return {
        name: member.name,
        avatar: member.avatar,
        insertedAt: insertedAt,
        timeDifference: timeDifference, // Time difference string
      };
    });

    // Create a single object with all the data


    return recentMembersWithTimeDiff;
  } catch (error) {
    console.error('Error in getRecentMembers function:', error);
    throw error;
  }
}




















































// revenue page (effiecient both frontend and backend)
async function getRevenueForCurrentAndLastYear(charCode) {
  try {
    const currentYear = new Date().getFullYear();
    const lastYear = currentYear - 1;

    const revenueByMonthCurrYear = {};
    const revenueByMonthLastYear = {};
    let totalRevenueCurrYear = 0;
    let totalRevenueLastYear = 0;

    const promises = [];

    const monthsCurrYear = Array.from({ length: 12 }, (_, month) => {
      const monthYear = `${new Date(currentYear, month).toLocaleString('default', { month: 'long' })}-${currentYear}`;
      return { month, monthYear };
    });

    const monthsLastYear = Array.from({ length: 12 }, (_, month) => {
      const monthYear = `${new Date(lastYear, month).toLocaleString('default', { month: 'long' })}-${lastYear}`;
      return { month, monthYear };
    });

    const historyModel = mongoose.model(`${charCode}_history`, historySchema);

    // Fetch revenue for current year
    const currYearPromises = monthsCurrYear.map(async ({ month, monthYear }) => {
      const totalRevenue = await historyModel.aggregate([
        {
          $match: {
            monthYear: { $regex: new RegExp(`${monthYear}`), $options: 'i' },
          },
        },
        {
          $group: {
            _id: null,
            total: { $sum: { $ifNull: ['$membershipFee', 0] } },
          },
        },
      ]);
      revenueByMonthCurrYear[monthYear] = totalRevenue.length > 0 ? totalRevenue[0].total : 0;
      totalRevenueCurrYear += revenueByMonthCurrYear[monthYear];
    });
    promises.push(...currYearPromises);

    // Fetch revenue for last year
    const lastYearPromises = monthsLastYear.map(async ({ month, monthYear }) => {
      const totalRevenue = await historyModel.aggregate([
        {
          $match: {
            monthYear: { $regex: new RegExp(`${monthYear}`), $options: 'i' },
          },
        },
        {
          $group: {
            _id: null,
            total: { $sum: { $ifNull: ['$membershipFee', 0] } },
          },
        },
      ]);
      revenueByMonthLastYear[monthYear] = totalRevenue.length > 0 ? totalRevenue[0].total : 0;
      totalRevenueLastYear += revenueByMonthLastYear[monthYear];
    });
    promises.push(...lastYearPromises);

    await Promise.all(promises);

    return {
      revenueByMonthCurrYear,
      revenueByMonthLastYear,
      totalRevenueCurrYear,
      totalRevenueLastYear,
    };
  } catch (error) {
    console.error('Error in getRevenueForCurrentAndLastYear function:', error);
    throw error;
  }
}

app.get('/revenuePage', async (req, res) => {
  try {
    const charCode = req.query.charCode;
    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    // Assuming you have a mongoose model named 'Owner'
    const owner = await Owner.findOne({ charCode });
    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const result = await getRevenueForCurrentAndLastYear(charCode);

    res.json({
      revenueByMonthCurrYear: result.revenueByMonthCurrYear,
      revenueByMonthLastYear: result.revenueByMonthLastYear,
      totalRevenueCurrYear: result.totalRevenueCurrYear,
      totalRevenueLastYear: result.totalRevenueLastYear,
    });
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});


// members ( effiecient both front end and backend )

async function getActionCountsByMonth(charCode, action, year) {
  try {
    const History = mongoose.model(`${charCode}_history`, historySchema);

    const actionCountsByMonth = await History.aggregate([
      {
        $match: {
          action,
          monthYear: { $regex: new RegExp(`${year}`), $options: 'i' },
        },
      },
      {
        $group: {
          _id: '$monthYear',
          count: { $sum: 1 },
        },
      },
    ]);

    return actionCountsByMonth.reduce((acc, entry) => {
      acc[entry._id] = entry.count;
      return acc;
    }, {});
  } catch (error) {
    console.error(`Error in getActionCountsByMonth function:`, error);
    throw error;
  }
}

async function getTotalCountsByAction(charCode, action, year) {
  try {
    const History = mongoose.model(`${charCode}_history`, historySchema);

    const totalCountsByAction = await History.aggregate([
      {
        $match: {
          action,
          monthYear: { $regex: new RegExp(`${year}`), $options: 'i' },
        },
      },
      {
        $group: {
          _id: null,
          totalCount: { $sum: 1 },
        },
      },
    ]);

    return totalCountsByAction.length > 0 ? totalCountsByAction[0].totalCount : 0;
  } catch (error) {
    console.error(`Error in getTotalCountsByAction function:`, error);
    throw error;
  }
}

app.get('/registrationAndRenewalCount', async (req, res) => {
  try {
    const charCode = req.query.charCode;
    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });
    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const currentYear = new Date().getFullYear();
    const lastYear = currentYear - 1;

    const newRegistrationsByMonthCurrYear = await getActionCountsByMonth(charCode, 'New Registration', currentYear);
    const renewalsByMonthCurrYear = await getActionCountsByMonth(charCode, 'Renewal', currentYear);

    const newRegistrationsByMonthLastYear = await getActionCountsByMonth(charCode, 'New Registration', lastYear);
    const renewalsByMonthLastYear = await getActionCountsByMonth(charCode, 'Renewal', lastYear);

    // Additional data fetching for total counts
    const totalNewRegistrationsCurrYear = await getTotalCountsByAction(charCode, 'New Registration', currentYear);
    const totalRenewalsCurrYear = await getTotalCountsByAction(charCode, 'Renewal', currentYear);
    const totalNewRegistrationsLastYear = await getTotalCountsByAction(charCode, 'New Registration', lastYear);
    const totalRenewalsLastYear = await getTotalCountsByAction(charCode, 'Renewal', lastYear);

    res.json({
      newRegistrationsByMonthCurrYear,
      renewalsByMonthCurrYear,
      newRegistrationsByMonthLastYear,
      renewalsByMonthLastYear,
      totalNewRegistrationsCurrYear,
      totalRenewalsCurrYear,
      totalNewRegistrationsLastYear,
      totalRenewalsLastYear,
    });
  } catch (error) {
    console.error('Error in /registrationAndRenewalCount route:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});


// expenditure (efficient frontend and backend )
async function getExpenditureForCurrentAndLastYear(charCode) {
  try {
    const currentYear = new Date().getFullYear();
    const lastYear = currentYear - 1;

    const expenditureByMonthCurrYear = {};
    const expenditureByMonthLastYear = {};
    let totalExpenditureCurrYear = 0;
    let totalExpenditureLastYear = 0;

    const promises = [];

    const monthsCurrYear = Array.from({ length: 12 }, (_, month) => {
      const monthYear = `${new Date(currentYear, month).toLocaleString('default', { month: 'long' })}-${currentYear}`;
      return { month, monthYear };
    });

    const monthsLastYear = Array.from({ length: 12 }, (_, month) => {
      const monthYear = `${new Date(lastYear, month).toLocaleString('default', { month: 'long' })}-${lastYear}`;
      return { month, monthYear };
    });

    const expenditureModel = mongoose.model(`${charCode}_expenditure`, expenditureSchema);

    // Combine the aggregation stages for current and last year
    const allYearPromises = monthsCurrYear.concat(monthsLastYear).map(async ({ month, monthYear }) => {
      const totalExpenditure = await expenditureModel.aggregate([
        {
          $match: {
            monthYear: { $regex: new RegExp(`${monthYear}`), $options: 'i' },
          },
        },
        {
          $group: {
            _id: null,
            total: { $sum: '$amount' },
          },
        },
      ]);
      const total = totalExpenditure.length > 0 ? totalExpenditure[0].total : 0;

      if (currentYear === parseInt(monthYear.split('-')[1])) {
        expenditureByMonthCurrYear[monthYear] = total;
        totalExpenditureCurrYear += total;
      } else {
        expenditureByMonthLastYear[monthYear] = total;
        totalExpenditureLastYear += total;
      }
    });
    promises.push(...allYearPromises);

    await Promise.all(promises);

    return {
      expenditureByMonthCurrYear,
      expenditureByMonthLastYear,
      totalExpenditureCurrYear,
      totalExpenditureLastYear,
    };
  } catch (error) {
    console.error('Error in getExpenditureForCurrentAndLastYear function:', error);
    throw error;
  }
}

app.get('/expenditurePage', async (req, res) => {
  try {
    const charCode = req.query.charCode;
    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });
    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    const result = await getExpenditureForCurrentAndLastYear(charCode);

    res.json({
      expenditureByMonthCurrYear: result.expenditureByMonthCurrYear,
      expenditureByMonthLastYear: result.expenditureByMonthLastYear,
      totalExpenditureCurrYear: result.totalExpenditureCurrYear,
      totalExpenditureLastYear: result.totalExpenditureLastYear,
    });
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// profit (efficient frontend and backend)
app.get('/profitPage', async (req, res) => {
  try {
    const charCode = req.query.charCode;
    if (!charCode) {
      return res.status(400).json({ error: 'Account nor found. Please Login!' });
    }

    const owner = await Owner.findOne({ charCode });
    if (!owner) {
      return res.status(404).json({ error: 'Account not found. Please Login!' });
    }

    // Fetch revenue for current year
    const revenueResult = await getRevenueForCurrentAndLastYear(charCode);

    // Fetch expenditure for current year
    const expenditureResult = await getExpenditureForCurrentAndLastYear(charCode);

    // Calculate profit for each month of the current year
    const profitByMonthCurrYear = {};
    for (const monthYear in revenueResult.revenueByMonthCurrYear) {
      const revenue = revenueResult.revenueByMonthCurrYear[monthYear];
      const expenditure = expenditureResult.expenditureByMonthCurrYear[monthYear];
      profitByMonthCurrYear[monthYear] = revenue - expenditure;
    }

    // Calculate profit for each month of the last year
    const profitByMonthLastYear = {};
    for (const monthYear in revenueResult.revenueByMonthLastYear) {
      const revenue = revenueResult.revenueByMonthLastYear[monthYear];
      const expenditure = expenditureResult.expenditureByMonthLastYear[monthYear];
      profitByMonthLastYear[monthYear] = revenue - expenditure;
    }

    res.json({
      profitByMonthCurrYear,
      profitByMonthLastYear,
      totalProfitCurrYear: revenueResult.totalRevenueCurrYear - expenditureResult.totalExpenditureCurrYear,
      totalProfitLastYear: revenueResult.totalRevenueLastYear - expenditureResult.totalExpenditureLastYear,
    });
  } catch (error) {
    console.error('Error in /profitPage route:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});


app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});