const mongoose = require('mongoose');

const historySchema = new mongoose.Schema({
  id: Number,
  memberId: String,
  name: String,
  preferredSlot: { type: String, required: true },
  contactNumber: { type: String, required: true, maxlength: 10 },
  paidFor: {type :Date, default: Date.now }, // Store the date fee was paid in "dd/month name/yyyy" format
  action: {type : String, default:'New Registration'},
  planChoice: String,
  membershipFee: { type: Number, required: true },
  validUptoDate: Date,
  paymentMethod: String,
  services: { type: Array, default: [] },
  timestamp: { type: Date, default: Date.now },// Store timestamp in "dd/mm/yyyy-hh:mm:ss" format,
  avatar: {
    type: String,
  },
  monthYear: {
    type: String,
    default: () => {
      const now = new Date();
      const monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];
      return `${monthNames[now.getMonth()]}-${now.getFullYear()}`;
    },
  },

});

module.exports = historySchema;
