// models/member.js
const mongoose = require('mongoose');

const expenditureSchema = new mongoose.Schema({
  id: { type: Number },
  title: { type: String }, // Add memberId field
  description: { type: String },
  amount : { type : Number},
  timestamp: { type: Date, default: Date.now },
  monthYear: {
    type: String,
    default: () => {
      const now = new Date();
      const monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];
      return `${monthNames[now.getMonth()]}-${now.getFullYear()}`;
    },
  },
});

module.exports = expenditureSchema;
