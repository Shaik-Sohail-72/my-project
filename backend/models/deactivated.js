// models/deactivated.js
const mongoose = require('mongoose');

const deactivatedSchema = new mongoose.Schema({
  avatar: {
    type: String,
  },
  memberId: { type: Number },
  status: { type: String, default: 'Deactivated' },
  name: { type: String, maxlength: 255 },
  admissionDate: { type: Date },
  lastRenewalDate: { type: Date },
  validUptoDate: { type: Date },
  email: { type: String, maxlength: 255 },
  contactNumber: { type: String, maxlength: 10 },
  age: { type: Number, min: 5, max: 100 },
  gender: { type: String, maxlength: 255 },
  planChoice: { type: String, maxlength: 255 },
  membershipFee: { type: Number },
  preferredSlot: { type: String },
  services: { type: Array, default: [] },
  address: { type: String, default: 'India' },
  // Add memberId field
  monthYear: {
    type: String,
    default: () => {
      const now = new Date();
      const monthNames = [
        'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',
      ];
      return `${monthNames[now.getMonth()]}-${now.getFullYear()}`;
    },
  },
  monthsNotPaid: { type: Number },
  deactivatedDate: { type: Date },
});

module.exports = deactivatedSchema;
