// models/member.js
const mongoose = require('mongoose');
const avatars = [
  '/assets/avatars/avatar-alcides-antonio.png',
  '/assets/avatars/avatar-anika-visser.png',
  '/assets/avatars/avatar-cao-yu.png',
  '/assets/avatars/avatar-carson-darrin.png',
  '/assets/avatars/avatar-chinasa-neo.png',
  '/assets/avatars/avatar-fran-perez.png',
  '/assets/avatars/avatar-iulia-albu.png',
  '/assets/avatars/avatar-jane-rotanson.png',
  '/assets/avatars/avatar-jie-yan-song.png',
  '/assets/avatars/avatar-marcus-finn.png',
  '/assets/avatars/avatar-miron-vitold.png',
  '/assets/avatars/avatar-nasimiyu-danai.png',
  '/assets/avatars/avatar-neha-punita.png',
  '/assets/avatars/avatar-omar-darboe.png',
  '/assets/avatars/avatar-penjani-inyene.png',
  '/assets/avatars/avatar-seo-hyeon-ji.png',
  '/assets/avatars/avatar-siegbert-gottfried.png',
];

const memberSchema = new mongoose.Schema({
  memberId: { type: Number, required: true }, // Add memberId field
  name: { type: String, required: true, maxlength: 255 },
  email: { type: String, required: true, maxlength: 255 },
  contactNumber: { type: String, required: true, maxlength: 10 },
  age: { type: Number, required: true, min: 5, max: 100 },
  gender: { type: String, required: true, maxlength: 255 },
  planChoice: { type: String, required: true, maxlength: 255 },
  membershipFee: { type: Number, required: true },
  paymentMethod: { type: String, required: true },
  preferredSlot: { type: String, required: true },
  services: { type: Array, default: [] },
  admissionDate: { type: Date, default: Date.now },
  lastRenewalDate: { type: Date, default: Date.now },
  validUptoDate: { type: Date },
  monthYear: {
    type: String,
    default: () => {
      const now = new Date();
      const monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];
      return `${monthNames[now.getMonth()]}-${now.getFullYear()}`;
    },
  },
  status: { type: String, default: 'Active' },
  avatar: {
    type: String,
    default: () => avatars[Math.floor(Math.random() * avatars.length)],
  },
  address: { type: String, default: 'India'},

});
module.exports = memberSchema;
