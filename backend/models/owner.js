const mongoose = require('mongoose');

const ownerSchema = new mongoose.Schema({
  ownerId:String,
  name: String,
  gymName: String, // Added gymName field
  email: String,
  number: String,
  aadhar: String,
  password: String,
  charCode: String,
  city: String,
  street: String,
  pincode: String,
  authorized: {
    type: Boolean,
    default: false
  },
  subscriptionExpireDate: {
    type: Date,
    default: function() {
      return new Date(Date.now() + 60 * 60 * 24 * 60 * 1000); // Two months from now
    }
  },
  subscriptionType: {
    type: String,
    enum: ['free', 'basic', 'starter', 'growth'], // Add the four subscription plans
    default: 'free' // Set a default value (e.g., 'free') if needed
  },
  insta: {
    type: String
  }
});

module.exports = mongoose.model('Owner', ownerSchema);
