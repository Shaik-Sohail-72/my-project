const mongoose = require('mongoose');

const tempForgotSchema = new mongoose.Schema({
  email: String,
  otp: String,
  otpTimestamp: Number,
});

const TempForgot = mongoose.model('TempForgot', tempForgotSchema);

module.exports = TempForgot;
