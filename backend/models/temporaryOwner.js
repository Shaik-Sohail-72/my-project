const mongoose = require('mongoose');

const temporaryOwnerSchema = new mongoose.Schema({
  name: String,
  gymName: String, // Added gymName field
  email: String,
  number: String,
  aadhar: String,
  password: String,
  charCode: String,
  city: String,
  street: String,
  pincode: String,
  otp: String,
  otpTimestamp: Number, // Added timestamp for OTP expiration
  insta: String
});

const TemporaryOwner = mongoose.model('TemporaryOwner', temporaryOwnerSchema);

module.exports = TemporaryOwner;
