// models/member.js
const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema({
  id: { type: Number },
  title: { type: String }, // Add memberId field
  description: { type: String },
  date: { type: Date },
  time: { type: String },
  locationAddr: { type: String },
  sentMailStatus: { type: Number, required:true}, // Corrected the typo here
  timestamp: { type: Date, default: Date.now },
  monthYear: {
    type: String,
    default: () => {
      const now = new Date();
      const monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];
      return `${monthNames[now.getMonth()]}-${now.getFullYear()}`;
    },
  },
});

module.exports = eventSchema;
