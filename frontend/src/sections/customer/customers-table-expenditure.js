import PropTypes from 'prop-types';
import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow
} from '@mui/material';
import { Scrollbar } from 'src/components/scrollbar';
import { SeverityPill } from 'src/components/severity-pill';


export const CustomersTableExpenditure = (props) => {

  
  const formatDateTime = (dateString) => {
    const dateObject = new Date(dateString);
  
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    let hours = dateObject.getHours().toString().padStart(2, '0');
    const minutes = dateObject.getMinutes().toString().padStart(2, '0');
    const seconds = dateObject.getSeconds().toString().padStart(2, '0');
    
    // Determine AM/PM
    const ampm = hours >= 12 ? 'PM' : 'AM';
  
    // Convert hours to 12-hour format
    hours = (hours % 12) || 12;
  
    return `${day}-${month}-${year} ${hours}:${minutes}:${seconds} ${ampm}`;
  };


  
  function formatNumberWithCommas(number) {
    if (number && number !== null) {
      return number.toLocaleString('en-IN'); // 'en-IN' represents the Indian English locale
    }
    // Handle the case when number is null, return an appropriate value or an empty string
    return '';
  }
  const {
    currentYearTotal,
    lastYearTotal,
    count = 0,
    items = [],

    onPageChange = () => {},
    onRowsPerPageChange,

    page = 0,
    rowsPerPage = 0,
    selected = []
  } = props;


  return (
    <Card>
      <Scrollbar>
        <Box sx={{ minWidth: 800 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Title
                </TableCell>
                <TableCell>
                  Description
                </TableCell>
                <TableCell>
                
                Amount
                </TableCell>
                <TableCell>
                Time Stamp
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map((customer) => {
                const isSelected = selected.includes(customer.id);
   

                return (
                  <TableRow
                    hover
                    key={customer.id}
                    selected={isSelected}
                  >
 

                    
                    <TableCell>
                      {customer.title}
                    </TableCell>
                    <TableCell>
                    {customer.description}
                    </TableCell>
                    <TableCell>
                    <SeverityPill color={"error"}>
                    ₹ {formatNumberWithCommas(customer.amount)}
                    </SeverityPill>
                    </TableCell>
                    <TableCell>
                    {formatDateTime(customer.timestamp)}
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
           
          </Table>
        </Box>
      </Scrollbar>
      <TablePagination
        component="div"
        count={count}
        onPageChange={onPageChange}
        onRowsPerPageChange={onRowsPerPageChange}
        page={page}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={[5, 10, 25, count]}
      />
    </Card>
  );
};

CustomersTableExpenditure.propTypes = {
  count: PropTypes.number,
  items: PropTypes.array,
  onDeselectOne: PropTypes.func,
  onPageChange: PropTypes.func,
  onRowsPerPageChange: PropTypes.func,

  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  selected: PropTypes.array
};
