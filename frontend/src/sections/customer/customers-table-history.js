import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Card,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Button
} from '@mui/material';
import { Scrollbar } from 'src/components/scrollbar';
import { getInitials } from 'src/utils/get-initials';

import { SeverityPill } from 'src/components/severity-pill';




export const CustomersTableHistory = (props) => {
  const {
    count = 0,
    items = [],
    onPageChange = () => {},
    onRowsPerPageChange,
    page = 0,
    rowsPerPage = 0,
    selected = []
  } = props;




  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    return `${day}-${month}-${year}`;
  };

  const formatDateTime = (dateString) => {
    const dateObject = new Date(dateString);
  
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    let hours = dateObject.getHours().toString().padStart(2, '0');
    const minutes = dateObject.getMinutes().toString().padStart(2, '0');
    const seconds = dateObject.getSeconds().toString().padStart(2, '0');
    
    // Determine AM/PM
    const ampm = hours >= 12 ? 'PM' : 'AM';
  
    // Convert hours to 12-hour format
    hours = (hours % 12) || 12;
  
    return `${day}-${month}-${year} ${hours}:${minutes}:${seconds} ${ampm}`;
  };

  return (
    <Card>
      <Scrollbar>
        <Box sx={{ minWidth:2000 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Member ID
                </TableCell>
                <TableCell>
                  Name
                </TableCell>
               
                <TableCell>
                  Phone Number
                </TableCell>

                <TableCell>
                  Action
                </TableCell>
                <TableCell>
                  Plan Choice
                </TableCell>
                <TableCell>
                  Membership Fee
                </TableCell>
                <TableCell>
                  Paid For
                </TableCell>
                <TableCell>
                  Valid Upto
                </TableCell>
                <TableCell>
                  Payment Method
                </TableCell>
                <TableCell>
                  Services
                </TableCell>
                
                <TableCell>
                  TimeStamp
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map((customer) => {
                const isSelected = selected.includes(customer.id);

                return (
                  <TableRow
                    hover
                    key={customer.id}
                    selected={isSelected}
                  >
                    <TableCell variant="subtitle2">
                      <Typography variant="subtitle2">
                       
                          {customer.memberId}
                      
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Stack
                        alignItems="center"
                        direction="row"
                        spacing={1}
                      >
                        <Avatar src={customer.avatar}>
                          {getInitials(customer.name)}
                        </Avatar>
                        <Typography variant="subtitle2">
                          {customer.name}
                        </Typography>
                      </Stack>
                    </TableCell>

                    <TableCell>
                      {customer.contactNumber}
                    </TableCell>

                    <TableCell>
                      {customer.action}
                    </TableCell>
                    <TableCell>
                      {customer.planChoice}
                    </TableCell>
                    <TableCell>
                    <SeverityPill color={"success"}>
                    {customer.membershipFee}
                  </SeverityPill>
                      
                    </TableCell>
                    <TableCell>
                      {formatDate(customer.paidFor)}
                    </TableCell>
                    <TableCell>
                      {formatDate(customer.validUptoDate)}
                    </TableCell>
                    <TableCell>
                      {customer.paymentMethod}
                    </TableCell>
                    <TableCell>
                      {Array.isArray(customer.services) ? customer.services.join(', ') : ''}
                    
                    </TableCell>
                    
                    <TableCell>
                      {formatDateTime(customer.timestamp)}
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </Scrollbar>
      <TablePagination
        component="div"
        count={count}
        onPageChange={onPageChange}
        onRowsPerPageChange={onRowsPerPageChange}
        page={page}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={[5, 10, 25, 50, count]}
      />
    </Card>
  );
};

CustomersTableHistory.propTypes = {
  count: PropTypes.number,
  items: PropTypes.array,
  onPageChange: PropTypes.func,
  onRowsPerPageChange: PropTypes.func,
  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  selected: PropTypes.array
};
