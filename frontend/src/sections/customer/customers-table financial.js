import PropTypes from 'prop-types';
import axios from 'axios';
import {
  Avatar,
  Box,
  Card,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Button
} from '@mui/material';
import { Scrollbar } from 'src/components/scrollbar';
import { getInitials } from 'src/utils/get-initials';

import { SeverityPill } from 'src/components/severity-pill';

const statusMap = {
  Pending: 'warning',
  Active: 'success',
  Deactivated: 'error'
};

export const CustomersTableFinancial = (props) => {
  const {
    count = 0,
    items = [],
    onPageChange = () => {},
    onRowsPerPageChange,
    page = 0,
    rowsPerPage = 0,
    selected = []
  } = props;

  const handleMemberId = async (id) => {

    const charCode = sessionStorage.getItem("charCode");
    console.log(id);
    let responseData;
    const response = await axios.get(`https://fithubbackend.onrender.com/userInfo/${id}?charCode=${charCode}`).then((res)=>{
      console.log(res);
      responseData=res;
    });
  
    console.log("printing res"+responseData.data.members);
    // location.href="/userInfo";

  };
  
  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    return `${day}-${month}-${year}`;
  };

  return (
    <Card>
      <Scrollbar>
        <Box sx={{ minWidth: 800 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Member ID
                </TableCell>
                <TableCell>
                  Name
                </TableCell>
                <TableCell>
                  Phone Number
                </TableCell>
                <TableCell>
                  Age
                </TableCell>
                <TableCell>
                  Valid Upto
                </TableCell>


                <TableCell>
                  Status
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map((customer) => {
                const isSelected = selected.includes(customer.id);

                return (
                  <TableRow
                    hover
                    key={customer.id}
                    selected={isSelected}
                  >
                    <TableCell variant="subtitle2">
                    <Typography variant="subtitle2">
                    <Button variant="contained"
color="primary"
onClick={() => handleMemberId(customer.memberId)}>
                      {customer.memberId}
                    </Button>
                  </Typography>
                    </TableCell>
                    <TableCell>
                      <Stack
                        alignItems="center"
                        direction="row"
                        spacing={1}
                      >
                        <Avatar src={customer.avatar}>
                          {getInitials(customer.name)}
                        </Avatar>
                        <Typography variant="subtitle2">
                          {customer.name}
                        </Typography>
                      </Stack>
                    </TableCell>
                    <TableCell>
                      {customer.contactNumber}
                    </TableCell>
                    <TableCell>
                      {customer.age}
                    </TableCell>
                    <TableCell>
                      {formatDate(customer.validUptoDate)}
                    </TableCell>

    
                    <TableCell>
                      <SeverityPill color={statusMap[customer.status]}>
                        {customer.status}
                      </SeverityPill>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </Scrollbar>
      <TablePagination
        component="div"
        count={count}
        onPageChange={onPageChange}
        onRowsPerPageChange={onRowsPerPageChange}
        page={page}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={[5, 10, 25, 50, count]}
      />
    </Card>
  );
};

CustomersTableFinancial.propTypes = {
  count: PropTypes.number,
  items: PropTypes.array,
  onPageChange: PropTypes.func,
  onRowsPerPageChange: PropTypes.func,
  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  selected: PropTypes.array
};
