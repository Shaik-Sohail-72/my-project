// UserInfoComponent.js
import React from 'react';
import { useParams } from 'react-router-dom';

const UserInfoComponent = () => {
  const { memberId } = useParams();

  // Print the memberId to the console
  console.log('Member ID:', memberId);

  return (
    <div>
      {/* Display the memberId in the component */}
      <p>Member ID: {memberId}</p>
      {/* Your component logic here */}
    </div>
  );
};

export default UserInfoComponent;
