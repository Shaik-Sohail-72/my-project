import PropTypes from 'prop-types';
import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow
} from '@mui/material';
import { Scrollbar } from 'src/components/scrollbar';

export const CustomersTableRevenue = (props) => {
  function formatNumberWithCommas(number) {
    if (number !== null) {
      return number.toLocaleString('en-IN'); 
    }
    return '';
  }

  const {
    currentYearTotal,
    lastYearTotal,
    count = 0,
    items = [],
    onPageChange = () => {},
    onRowsPerPageChange,
    page = 0,
    rowsPerPage = 0,
    selected = []
  } = props;

  return (
    <Card>
      <Scrollbar>
        <Box sx={{ minWidth: 800 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Month
                </TableCell>
                <TableCell>
                  Revenue 2024
                </TableCell>
                <TableCell>
                  Revenue 2023
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map((customer) => {
                return (
                  <TableRow
                    hover
                    key={customer.id}
                    selected={selected.includes(customer.id)}
                  >
                    <TableCell>
                      {customer.month}
                    </TableCell>
                    <TableCell>
                      ₹ {formatNumberWithCommas(customer.curr)  || 0}
                    </TableCell>
                    <TableCell>
                      ₹ {formatNumberWithCommas(customer.prev) || 0}
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
            <TableHead>
              <TableRow>
                <TableCell>
                  Total
                </TableCell>
                <TableCell>
                  ₹ {formatNumberWithCommas(currentYearTotal) || 0}
                </TableCell>
                <TableCell>
                  ₹ {formatNumberWithCommas(lastYearTotal) || 0}
                </TableCell>
              </TableRow>
            </TableHead>
          </Table>
        </Box>
      </Scrollbar>
      <TablePagination
        component="div"
        count={count}
        onPageChange={onPageChange}
        onRowsPerPageChange={onRowsPerPageChange}
        page={page}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={[5, 10, 12]}
      />
    </Card>
  );
};

CustomersTableRevenue.propTypes = {
  count: PropTypes.number,
  items: PropTypes.array,
  onPageChange: PropTypes.func,
  onRowsPerPageChange: PropTypes.func,
  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  selected: PropTypes.array
};
