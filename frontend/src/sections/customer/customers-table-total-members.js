import PropTypes from 'prop-types';
import { Box, Card, Table, TableBody, TableCell, TableHead, TablePagination, TableRow } from '@mui/material';
import { Scrollbar } from 'src/components/scrollbar';

export const CustomersTableTotalMembers = (props) => {
  const { count = 0, items = [], onPageChange = () => {}, onRowsPerPageChange, page = 0, rowsPerPage = 0, selected = [], totalRegistraionCurrYear, totalRegistrationLastYear, totalRenewalCurrYear, totalRenewalLastYear } = props;

  function formatNumberWithCommas(number) {
    if (number && number !== null) {
      return number.toLocaleString('en-IN'); // 'en-IN' represents the Indian English locale
    }
    // Handle the case when number is null, return an appropriate value or an empty string
    return '';
  }

  return (
    <Card>
      <Scrollbar>
        <Box sx={{ minWidth: 800 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Month</TableCell>
                <TableCell>New Registrations 2024</TableCell>
                <TableCell>New Registrations 2023</TableCell>
                <TableCell>Renewals 2024</TableCell>
                <TableCell>Renewals 2023</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map((customer) => (
                <TableRow hover
key={customer.id}
selected={selected.includes(customer.id)}>
                  <TableCell>{customer.month}</TableCell>
                  <TableCell>{formatNumberWithCommas(customer.curr_regis) || 0} </TableCell>
                  <TableCell>{formatNumberWithCommas(customer.prev_regis)|| 0} </TableCell>
                  <TableCell>{formatNumberWithCommas(customer.curr_ren)|| 0}</TableCell>
                  <TableCell>{formatNumberWithCommas(customer.prev_ren) || 0}</TableCell>
                </TableRow>
              ))}
            </TableBody>
            <TableHead>
              <TableRow>
                <TableCell>Total</TableCell>
                <TableCell>{formatNumberWithCommas(totalRegistraionCurrYear)||0}</TableCell>
                <TableCell>{formatNumberWithCommas(totalRegistrationLastYear)||0}</TableCell>
                <TableCell>{formatNumberWithCommas(totalRenewalCurrYear)||0}</TableCell>
                <TableCell>{formatNumberWithCommas(totalRenewalLastYear)||0}</TableCell>
              </TableRow>
            </TableHead>
          </Table>
        </Box>
      </Scrollbar>
      <TablePagination
        component="div"
        count={count}
        onPageChange={onPageChange}
        onRowsPerPageChange={onRowsPerPageChange}
        page={page}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={[5, 10, 12]}
      />
    </Card>
  );
};

CustomersTableTotalMembers.propTypes = {
  count: PropTypes.number,
  items: PropTypes.array,
  onPageChange: PropTypes.func,
  onRowsPerPageChange: PropTypes.func,
  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  selected: PropTypes.array,
  totalRegistraionCurrYear: PropTypes.number,
  totalRegistrationLastYear: PropTypes.number,
  totalRenewalCurrYear: PropTypes.number,
  totalRenewalLastYear: PropTypes.number,
};
