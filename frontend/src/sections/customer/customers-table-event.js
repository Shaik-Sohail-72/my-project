import PropTypes from 'prop-types';
import axios from 'axios';
import {
  Avatar,
  Box,
  Card,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Button
} from '@mui/material';
import { Scrollbar } from 'src/components/scrollbar';
import { getInitials } from 'src/utils/get-initials';

import { SeverityPill } from 'src/components/severity-pill';

export const CustomersTableEvent = (props) => {
  const {
    count = 0,
    items = [],
    onPageChange = () => {},
    onRowsPerPageChange,
    page = 0,
    rowsPerPage = 0,
    selected = []
  } = props;

  const formatDateTime = (dateString) => {
    const dateObject = new Date(dateString);
  
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    let hours = dateObject.getHours().toString().padStart(2, '0');
    const minutes = dateObject.getMinutes().toString().padStart(2, '0');
    const seconds = dateObject.getSeconds().toString().padStart(2, '0');
    
    // Determine AM/PM
    const ampm = hours >= 12 ? 'PM' : 'AM';
  
    // Convert hours to 12-hour format
    hours = (hours % 12) || 12;
  
    return `${day}-${month}-${year} ${hours}:${minutes}:${seconds} ${ampm}`;
  };
  function formatTimeAMPM(time24) {
    // Parse the input time string
    const [hours, minutes] = time24.split(':').map(Number);

    // Check if the time is in the AM or PM
    const period = hours >= 12 ? 'PM' : 'AM';

    // Convert hours to 12-hour format
    const hours12 = hours % 12 || 12;

    // Format the time in AM/PM
    const formattedTime = `${hours12}:${minutes.toString().padStart(2, '0')} ${period}`;

    return formattedTime;
}


  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    if (isNaN(dateObject.getTime())) {
      return 'Invalid Date';
    }
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();

    return `${day}-${month}-${year}`;
  };

  return (
    <Card>
      <Scrollbar>
        <Box sx={{ minWidth: 800 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Title</TableCell>
                <TableCell>Description</TableCell>
                <TableCell>Date</TableCell>
                <TableCell>Time</TableCell>
                <TableCell>Mail Status</TableCell>
                <TableCell>TimeStamp</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map((event) => {
                const isSelected = selected.includes(event._id);

                return (
                  <TableRow
                    hover
                    key={event._id}
                    selected={isSelected}
                  >
                    <TableCell>
                      <Stack alignItems="center"
direction="row"
spacing={1}>
                        <Typography variant="subtitle2">{event.title}</Typography>
                      </Stack>
                    </TableCell>
                    <TableCell>{event.description}</TableCell>
                    <TableCell>{formatDate(event.date)}</TableCell>
                    <TableCell>{formatTimeAMPM(event.time)}</TableCell>
                    <TableCell>
                      {/* Assuming customer.status is present in your data */}
                      <SeverityPill color={"success"}>
                        Sent - Active {event.sentMailStatus}
                      </SeverityPill>
                    </TableCell>
                    <TableCell>{formatDateTime(event.timestamp)}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Box>
      </Scrollbar>
      <TablePagination
        component="div"
        count={count}
        onPageChange={onPageChange}
        onRowsPerPageChange={onRowsPerPageChange}
        page={page}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={[5, 10, 25, 50, count]}
      />
    </Card>
  );
};

CustomersTableEvent.propTypes = {
  count: PropTypes.number,
  items: PropTypes.array,
  onPageChange: PropTypes.func,
  onRowsPerPageChange: PropTypes.func,
  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  selected: PropTypes.array
};