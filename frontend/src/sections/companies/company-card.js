import PropTypes from 'prop-types';
import ArrowDownOnSquareIcon from '@heroicons/react/24/solid/ArrowDownOnSquareIcon';
import ClockIcon from '@heroicons/react/24/solid/ClockIcon';
import { Avatar, Box, Card, CardContent, Divider, Stack, SvgIcon, Typography } from '@mui/material';
import { SeverityPill } from 'src/components/severity-pill';


const statusMap = {
  Pending: 'warning',
  Active: 'success',
  Deactivated: 'error'
};


export const CompanyCard = (props) => {
  const { company } = props;


  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    return `${day}-${month}-${year}`;
  };


  return (
    <Card
      sx={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        width: '100%'
      }}
    >
      <CardContent>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            pb: 3
          }}
        >
          <Avatar
            src={company.avatar}
            variant="circle"
            sx={{
              width: '100px',
              height: '100px',
            }}
          />
        </Box>
        <Typography
          align="center"
          gutterBottom
          variant="h5"
        >
          {company.memberId}
        </Typography>

        <Typography
        variant="body1"
        align="center"
        gutterBottom
      >
    <SeverityPill color={statusMap[company.status]}>
        {company.status}
      </SeverityPill>
      </Typography>


        <Typography
          variant="body1"
        >
          <b>Full Name : </b>{company.name}
        </Typography>



        <Typography
        variant="body1"
      >
        <b>Admission Date : </b>{formatDate(company.admissionDate)}
      </Typography>

        <Typography
          variant="body1"
        >
          <b>Last Renewal Date: </b>{formatDate(company.lastRenewalDate)}
        </Typography>

        

        <Typography
          variant="body1"
        >
          <b>Valid Upto : </b>{formatDate(company.validUptoDate)}
        </Typography>

        <Typography
          variant="body1"
        >
          <b>Email : </b>{company.email}
        </Typography>

        <Typography
          variant="body1"
        >
          <b>Phone Number : </b>{company.contactNumber}
        </Typography>

        <Typography
          variant="body1"
        >
          <b>Age : </b>{company.age}
        </Typography>

        <Typography
          variant="body1"
        >
          <b>Gender : </b>{company.gender}
        </Typography>

        <Typography
          variant="body1"
        >
          <b>Plan Choice : </b>{company.planChoice}
        </Typography>

        <Typography
          variant="body1"
        >
          <b>Membership Fee : </b>{company.membershipFee}
        </Typography>

        <Typography
          variant="body1"
        >
          <b>Payment Method : </b>{company.paymentMethod}
        </Typography>

        <Typography
      
          variant="body1"
        >
          <b>Time Slot : </b>{company.preferredSlot}
        </Typography>

        <Typography
          variant="body1"
        >
          <b>Services : </b>{company.services}
        </Typography>
        <Typography
          variant="body1"
        >
          <b>Address : </b>{company.address}
        </Typography>

      </CardContent>
      <Box sx={{ flexGrow: 1 }} />
      
    </Card>
  );
};

CompanyCard.propTypes = {
  company: PropTypes.object.isRequired
};
