import Head from 'next/head';
import 'react-toastify/dist/ReactToastify.css';
import NextLink from 'next/link';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Layout as AuthLayout } from 'src/layouts/auth/layout';
import { ToastContainer, toast } from 'react-toastify';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  Unstable_Grid2 as Grid,
  FormControlLabel,
  Checkbox,
} from '@mui/material';


async function signUp(memberId, planChoice, membershipFee, paymentMethod, preferredSlot, services) {
  const charCode=sessionStorage.getItem("charCode");
  const requestBody = {
    charCode,
    memberId,

    planChoice,
    membershipFee,
    paymentMethod,
    preferredSlot,
    services,
  };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/members/renewMember', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });

    if (response.status === 200) {
      const responseData = await response.json();
      location.href = "/history";
      toast(responseData.message, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "success" });
    } else {
      const responseData = await response.json();
      toast(responseData.error, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "error"
      });
    }
  } catch (error) {
    console.error('Error during new member registration', error);
  }
}

const preferredSlots = [
  {
    value: 'Morning',
    label: 'Morning',
  },
  {
    value: 'Afternoon',
    label: 'Afternoon',
  },
  {
    value: 'Evening',
    label: 'Evening',
  },
  {
    value: 'Night',
    label: 'Night',
  },
];

const Genders = [
  {
    value: 'Male',
    label: 'Male',
  },
  {
    value: 'Female',
    label: 'Female',
  },
];
const paymentMethods = [
  {
    value: 'Cash',
    label: 'Cash',
  },
  {
    value: 'Mobile Payments',
    label: 'Mobile Payments',
  },
];
const planChoices = [
  {
    value: 'Basic',
    label: 'Basic',
  },
  {
    value: 'Silver',
    label: 'Silver',
  },
  {
    value: 'Gold',
    label: 'Gold',
  },
  {
    value: 'Platinum',
    label: 'Platinum',
  },
];

export const AccountProfileDetailsRenewal = () => {
  const formik = useFormik({
    initialValues: {
      memberId: '',
       planChoice: 'Basic',
       membershipFee: '',
       paymentMethod: 'Cash',
       preferredSlot: 'Morning',
       services: [],
      submit: null,
    },
    validationSchema: Yup.object({
      memberId: Yup.number().required('Member Id is required'),
      planChoice: Yup.string().max(255).required('Plan Choice is required'),
      membershipFee: Yup.number().required('Membership Fee is required'),
       paymentMethod: Yup.string().required('Payment Method is required'),
       preferredSlot: Yup.string().required('Slot is required'),
       services: Yup.array().min(1, 'Select at least one service').required('Select at least one service'),
    }),
    onSubmit: async (values, helpers) => {
      try {
        await signUp(
          values.memberId,
          values.planChoice,
          values.membershipFee,
          values.paymentMethod,
          values.preferredSlot,
          values.services,
        );
      } catch (err) {
        helpers.setStatus({ success: false });
        helpers.setSubmitting(false);
      }
    },
  });

  return (
    <form noValidate
onSubmit={formik.handleSubmit}>
      <Card>
        <CardHeader title="Profile" />
        <CardContent sx={{ pt: 0 }}>
          <Box sx={{ m: -1.5 }}>
            <Grid container
spacing={3}>
              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.memberId && formik.errors.memberId)}
                  fullWidth
                  helperText={formik.touched.memberId && formik.errors.memberId}
                  label="Member Id"
                  name="memberId"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.memberId}
                />
              </Grid>

             

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.planChoice && formik.errors.planChoice)}
                  fullWidth
                  helperText={formik.touched.planChoice && formik.errors.planChoice}
                  label="Plan Choice"
                  name="planChoice"
                  required
                  select
                  SelectProps={{ native: true }}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.planChoice}
                >
                  {planChoices.map((option) => (
                    <option key={option.value}
value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Grid>

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.membershipFee && formik.errors.membershipFee)}
                  fullWidth
                  helperText={formik.touched.membershipFee && formik.errors.membershipFee}
                  label="MemberShip Fee"
                  name="membershipFee"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.membershipFee}
                  type="number"
                />
              </Grid>

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.paymentMethod && formik.errors.paymentMethod)}
                  fullWidth
                  helperText={formik.touched.paymentMethod && formik.errors.paymentMethod}
                  label="Payment Method"
                  name="paymentMethod"
                  required
                  select
                  SelectProps={{ native: true }}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.paymentMethod}
                >
                  {paymentMethods.map((option) => (
                    <option key={option.value}
value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Grid>

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.preferredSlot && formik.errors.preferredSlot)}
                  fullWidth
                  helperText={formik.touched.preferredSlot && formik.errors.preferredSlot}
                  label="Preferred Slot"
                  name="preferredSlot"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  required
                  select
                  SelectProps={{ native: true }}
                  value={formik.values.preferredSlot}
                >
                  {preferredSlots.map((option) => (
                    <option key={option.value}
value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Grid>

             

              <Grid xs={12}
md={4}
lg={12}>
                {/* Checkboxes for Services */}
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="OnlyGym"
                      checked={formik.values.services.includes('OnlyGym')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Only Gym"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="Cardio"
                      checked={formik.values.services.includes('Cardio')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Cardio"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="GroupClasses"
                      checked={formik.values.services.includes('GroupClasses')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Group Classes"
                />
                {/* Add more checkboxes for other services as needed */}
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="PersonalTraning"
                      checked={formik.values.services.includes('PersonalTraning')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Personal Traning"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="Zumba"
                      checked={formik.values.services.includes('Zumba')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Zumba"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="Yoga"
                      checked={formik.values.services.includes('Yoga')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Yoga"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="IndoorCycling"
                      checked={formik.values.services.includes('IndoorCycling')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Indoor Cycling"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="CrossFit"
                      checked={formik.values.services.includes('CrossFit')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Cross Fit"
                />
              </Grid>
            </Grid>
          </Box>
        </CardContent>
        <Divider />
        <CardActions sx={{ justifyContent: 'flex-end' }}>
          <Button size="large"
type="submit"
variant="contained">
            Renew
          </Button>
        </CardActions>
      </Card>
      <ToastContainer />
    </form>
  );
};
