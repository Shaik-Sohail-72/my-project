

import React, { useState } from 'react';
import { Avatar, Box, Button, Card, CardActions, CardContent, Divider, TextField, Typography } from '@mui/material';
import { useFormik } from 'formik';
import { SeverityPill } from 'src/components/severity-pill';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const statusMap = {
  Pending: 'warning',
  Active: 'success',
  Deactivated: 'error'
};

export const AccountProfileReactivate = () => {
  const [user, setUser] = useState({});
  const [isDataLoaded, setIsDataLoaded] = useState(false);

  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    return `${day}-${month}-${year}`;
  };

  const formik = useFormik({
    initialValues: {
      memberId: '', // Set initial memberId to an empty string
    },
    onSubmit: () => {
      handleFetchData();
    },
  });

  const handleFetchData = () => {
    const USER_PROFILE_API = `https://fithubbackend.onrender.com/deactivatedMembers/${formik.values.memberId}?charCode=${sessionStorage.getItem("charCode")}`;

    fetch(USER_PROFILE_API)
      .then(response => response.json())
      .then(data => {
        if (data.status === 'success') {
          setUser(data.user);
          setIsDataLoaded(true);
          // Toast success message
          toast.success(data.message, {
            position: 'top-right',
            autoClose: 5000,
            hideProgressBar: false,
            newestOnTop: false,
            closeOnClick: true,
            rtl: false,
            pauseOnFocusLoss: true,
            draggable: true,
            pauseOnHover: true,
            theme: 'light',
          });
        } else {
          console.error('Error fetching user profile data:', data.error);
          setIsDataLoaded(false);
          // Toast error message
          toast.error(data.error, {
            position: 'top-right',
            autoClose: 5000,
            hideProgressBar: false,
            newestOnTop: false,
            closeOnClick: true,
            rtl: false,
            pauseOnFocusLoss: true,
            draggable: true,
            pauseOnHover: true,
            theme: 'light',
          });
        }
      })
      .catch(error => {
        console.error('Error during user profile data fetch:', error);
        setIsDataLoaded(false);
        // Toast error message
        toast.error('An error occurred while fetching user profile data', {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          newestOnTop: false,
          closeOnClick: true,
          rtl: false,
          pauseOnFocusLoss: true,
          draggable: true,
          pauseOnHover: true,
          theme: 'light',
        });
      });
  };

  return (
    <Card>
      <CardActions>
        <TextField
          label="Enter Member ID"
          variant="outlined"
          name="memberId"
          value={formik.values.memberId}
          onChange={formik.handleChange}
        />
        <Button
          style={{ marginLeft: '30px' }}
          variant="contained"
          color="primary"
          onClick={formik.handleSubmit}
        >
          Find
        </Button>
      </CardActions>
      {isDataLoaded && (
        <CardContent>
        <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          pb: 3
        }}
      >
        <Avatar
          src={user.avatar}
          variant="circle"
          sx={{
            width: '100px',
            height: '100px',
          }}
        />
      </Box>
      <Typography
        align="center"
        gutterBottom
        variant="h5"
      >
        {user.memberId}
      </Typography>

      <Typography
      variant="body1"
      align="center"
      gutterBottom
    >
  <SeverityPill color={statusMap[user.status]}>
      {user.status}
    </SeverityPill>
    </Typography>


      <Typography
        variant="body1"
      >
        <b>Full Name : </b>{user.name}
      </Typography>



      <Typography
      variant="body1"
    >
      <b>Admission Date : </b>{formatDate(user.admissionDate)}
    </Typography>

      <Typography
        variant="body1"
      >
        <b>Last Renewal Date: </b>{formatDate(user.lastRenewalDate)}
      </Typography>

      

      <Typography
        variant="body1"
      >
        <b>Valid Upto : </b>{formatDate(user.validUptoDate)}
      </Typography>

      <Typography
        variant="body1"
      >
        <b>Email : </b>{user.email}
      </Typography>

      <Typography
        variant="body1"
      >
        <b>Phone Number : </b>{user.contactNumber}
      </Typography>

      <Typography
        variant="body1"
      >
        <b>Age : </b>{user.age}
      </Typography>

      <Typography
        variant="body1"
      >
        <b>Gender : </b>{user.gender}
      </Typography>

      <Typography
        variant="body1"
      >
        <b>Plan Choice : </b>{user.planChoice}
      </Typography>

      <Typography
        variant="body1"
      >
        <b>Membership Fee : </b>{user.membershipFee}
      </Typography>



      <Typography
    
        variant="body1"
      >
        <b>Time Slot : </b>{user.preferredSlot}
      </Typography>

      <Typography
        variant="body1"
      >
        <b>Services : </b>{user.services}
      </Typography>
      <Typography
        variant="body1"
      >
        <b>Address : </b>{user.address}
      </Typography>

      <Typography
        variant="body1"
      >
        <b>Months Not Paid : </b>{user.monthsNotPaid}
      </Typography>

      <Typography
        variant="body1"
      >
        <b>Deactivated Date : </b>{formatDate(user.deactivatedDate)}
      </Typography>

        </CardContent>
      )}
      <Divider />
    </Card>
  );
};
