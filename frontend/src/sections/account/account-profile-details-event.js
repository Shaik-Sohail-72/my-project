/*import 'react-toastify/dist/ReactToastify.css';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { ToastContainer, toast } from 'react-toastify';
import { Container, Stack, SvgIcon, Typography } from '@mui/material';
import PlusIcon from '@heroicons/react/24/solid/PlusIcon';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';

import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  Unstable_Grid2 as Grid,
  FormControlLabel,
  Checkbox,
} from '@mui/material';


async function signUp(title, description, date, time,locationAddr) {
  const charCode=sessionStorage.getItem("charCode");
  const requestBody = {
    charCode,
    title,
    date,
    time,
    description,
    locationAddr
  };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/addEvent', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });

    console.log(response);

    if (response.status === 200) {
      const responseData = await response.json();
       location.href = "/eventCenter";
      toast(responseData.message, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "success" });
    } else {
      const responseData = await response.json();
      toast(responseData.error, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "error"
      });
    }
  } catch (error) {
    console.error('Error during new member registration', error);
  }
}

export const AccountProfileDetailsEvent = () => {
  const formik = useFormik({
    initialValues: {
      title: '',
      description: '',
      date: '',
      time: '',
      locationAddr: '',
      submit: null,
    },
    validationSchema: Yup.object({
      title: Yup.string().max(255).required('Title is required'),
      description: Yup.string().max(255).required('Description is required'),
      date: Yup.string().required('Date is required'),
      time: Yup.string().required('Time is required'),
      locationAddr: Yup.string().required('Location is required')

    }),
    onSubmit: async (values, helpers) => {
      try {
        await signUp(
          values.title,
          values.description,
          values.date,
          values.time,
          values.locationAddr,
        );
      } catch (err) {
        helpers.setStatus({ success: false });
        helpers.setSubmitting(false);
      }
    },
  });

  return (
    <form noValidate onSubmit={formik.handleSubmit}>
      <Card>
        <CardHeader title="Event Details" />
        <CardContent sx={{ pt: 0 }}>
          <Box sx={{ m: -1.5 }}>
            <Grid container spacing={3}>
              <Grid xs={12} md={4}>
                <TextField
                  error={!!(formik.touched.title && formik.errors.title)}
                  fullWidth
                  helperText={formik.touched.title && formik.errors.title}
                  label="Title"
                  name="title"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.title}
                />
              </Grid>

              <Grid xs={12} md={8}>
                <TextField
                  error={!!(formik.touched.description && formik.errors.description)}
                  helperText={formik.touched.description && formik.errors.description}
                  fullWidth
                  label="Description"
                  name="description"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.description}
                />
              </Grid>

              <Grid xs={12} md={4}>
                <TextField
                  error={!!(formik.touched.date && formik.errors.date)}
                  fullWidth
                  helperText={formik.touched.date && formik.errors.date}
                  name="date"
                  type="date"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.date}
                />
              </Grid>

              <Grid xs={12} md={4}>
                <TextField
                  error={!!(formik.touched.time && formik.errors.time)}
                  fullWidth
                  helperText={formik.touched.time && formik.errors.time}
                  name="time"
                  type="time"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.time}
                />
              </Grid>

              <Grid xs={12} md={4}>
                <TextField
                  error={!!(formik.touched.locationAddr && formik.errors.locationAddr)}
                  fullWidth
                  helperText={formik.touched.locationAddr && formik.errors.locationAddr}
                  label="Location"
    
                  name="locationAddr"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.locationAddr}
                />
              </Grid>
            </Grid>
          </Box>
        </CardContent>
        <Divider />
        <CardActions sx={{ justifyContent: 'flex-end' }}>
          <Button  startIcon={
            <SvgIcon fontSize="small">
            <NotificationsActiveIcon />
            </SvgIcon>
          } size="large" type="submit" variant="contained">

         

            Send & Save 
          </Button>
        </CardActions>
      </Card>
      <ToastContainer />
    </form>
  );
};
*/


import 'react-toastify/dist/ReactToastify.css';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { ToastContainer, toast } from 'react-toastify';
import { Container, Stack, SvgIcon, Typography } from '@mui/material';
import PlusIcon from '@heroicons/react/24/solid/PlusIcon';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';

import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  Unstable_Grid2 as Grid,
  FormControlLabel,
  Checkbox,
} from '@mui/material';

async function signUp(title, description, date, time, locationAddr) {
  const charCode = sessionStorage.getItem("charCode");
  const requestBody = {
    charCode,
    title,
    date,
    time,
    description,
    locationAddr
  };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/addEvent', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });

    console.log(response);

    if (response.status === 200) {
      const responseData = await response.json();
      location.href = "/eventCenter";
      toast(responseData.message, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "success"
      });
    } else {
      const responseData = await response.json();
      toast(responseData.error, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "error"
      });
    }
  } catch (error) {
    console.error('Error during new member registration', error);
  }
}

export const AccountProfileDetailsEvent = () => {
  const formik = useFormik({
    initialValues: {
      title: '',
      description: '',
      date: '',
      time: '',
      locationAddr: '',
      submit: null,
    },
    validationSchema: Yup.object({
      title: Yup.string().max(255).required('Title is required'),
      description: Yup.string().max(255).required('Description is required'),
      date: Yup.string().required('Date is required'),
      time: Yup.string().required('Time is required'),
      locationAddr: Yup.string().required('Location is required')

    }),
    onSubmit: async (values, helpers) => {
      try {
        await signUp(
          values.title,
          values.description,
          values.date,
          values.time,
          values.locationAddr,
        );
      } catch (err) {
        helpers.setStatus({ success: false });
        helpers.setSubmitting(false);
      }
    },
  });

  return (
    <form noValidate
onSubmit={formik.handleSubmit}>
      <Card>
        <CardHeader title="Event Details" />
        <CardContent sx={{ pt: 0 }}>
          <Box sx={{ m: -1.5 }}>
            <Grid container
spacing={3}>
            <Grid xs={12}
md={4}>
            <TextField
              error={!!(formik.touched.title && formik.errors.title)}
              fullWidth
              helperText={formik.touched.title && formik.errors.title}
              label="Title"
              name="title"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.title}
            />
          </Grid>

          <Grid xs={12}
md={8}>
            <TextField
              error={!!(formik.touched.description && formik.errors.description)}
              helperText={formik.touched.description && formik.errors.description}
              fullWidth
              label="Description"
              name="description"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.description}
            />
          </Grid>

          <Grid xs={12}
md={4}>
            <TextField
              error={!!(formik.touched.date && formik.errors.date)}
              fullWidth
              helperText={formik.touched.date && formik.errors.date}
              name="date"
              type="date"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.date}
            />
          </Grid>

          <Grid xs={12}
md={4}>
            <TextField
              error={!!(formik.touched.time && formik.errors.time)}
              fullWidth
              helperText={formik.touched.time && formik.errors.time}
              name="time"
              type="time"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.time}
            />
          </Grid>

          <Grid xs={12}
md={4}>
            <TextField
              error={!!(formik.touched.locationAddr && formik.errors.locationAddr)}
              fullWidth
              helperText={formik.touched.locationAddr && formik.errors.locationAddr}
              label="Location"

              name="locationAddr"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.locationAddr}
            />
          </Grid>
            </Grid>
          </Box>
        </CardContent>
        <Divider />
        <CardActions sx={{ justifyContent: 'flex-end' }}>
          <Button
            startIcon={
              <SvgIcon fontSize="small">
                <NotificationsActiveIcon />
              </SvgIcon>
            }
            size="large"
            type="submit"
            variant="contained"
            disabled={formik.isSubmitting} // Disable the button when submitting
          >
            Send & Save
          </Button>
        </CardActions>
      </Card>
      <ToastContainer />
    </form>
  );
};
