import 'react-toastify/dist/ReactToastify.css';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { ToastContainer, toast } from 'react-toastify';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  Unstable_Grid2 as Grid,
  FormControlLabel,
  Checkbox,
} from '@mui/material';

async function signUp(name, email, contactNumber, age, gender, planChoice, membershipFee, paymentMethod, preferredSlot, services, address) {
  const charCode=sessionStorage.getItem("charCode");
  const requestBody = {
    charCode,
    name,
    email,
    contactNumber,
    age,
    gender,
    planChoice,
    membershipFee,
    paymentMethod,
    preferredSlot,
    services,
    address
  };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/members/addMember', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });

    if (response.status === 200) {
      const responseData = await response.json();
      location.href = "/members";
      toast(responseData.message, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "success" });
    } else {
      const responseData = await response.json();
      toast(responseData.error, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "error"
      });
    }
  } catch (error) {
    console.error('Error during new member registration', error);
  }
}

const preferredSlots = [
  {
    value: 'Morning',
    label: 'Morning',
  },
  {
    value: 'Afternoon',
    label: 'Afternoon',
  },
  {
    value: 'Evening',
    label: 'Evening',
  },
  {
    value: 'Night',
    label: 'Night',
  },
];

const Genders = [
  {
    value: 'Male',
    label: 'Male',
  },
  {
    value: 'Female',
    label: 'Female',
  },
];
const paymentMethods = [
  {
    value: 'Cash',
    label: 'Cash',
  },
  {
    value: 'Mobile Payments',
    label: 'Mobile Payments',
  },
];
const planChoices = [
  {
    value: 'Basic',
    label: 'Basic',
  },
  {
    value: 'Silver',
    label: 'Silver',
  },
  {
    value: 'Gold',
    label: 'Gold',
  },
  {
    value: 'Platinum',
    label: 'Platinum',
  },
];

export const AccountProfileDetails = () => {
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      contactNumber: '',
      age: '',
      gender: 'Male',
      planChoice: 'Basic',
      membershipFee: '',
      paymentMethod: 'Cash',
      preferredSlot: 'Morning',
      services: [],
      address: 'India',
      submit: null,
    },
    validationSchema: Yup.object({
      name: Yup.string().max(255).required('Full Name is required'),
      email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
      contactNumber: Yup.string().matches(/^[0-9]{10}$/, 'Phone number must be exactly 10 digits').required(
        'Phone Number is required'
      ),
      age: Yup.number().max(100).min(5).required('Age is required'),
      gender: Yup.string().max(255).required('Gender is required'),
      planChoice: Yup.string().max(255).required('Plan Choice is required'),
      membershipFee: Yup.number().required('Membership Fee is required'),
      paymentMethod: Yup.string().required('Payment Method is required'),
      preferredSlot: Yup.string().required('Slot is required'),
      services: Yup.array().min(1, 'Select at least one service').required('Select at least one service'),
    }),
    onSubmit: async (values, helpers) => {
      try {
        await signUp(
          values.name,
          values.email,
          values.contactNumber,
          values.age,
          values.gender,
          values.planChoice,
          values.membershipFee,
          values.paymentMethod,
          values.preferredSlot,
          values.services,
          values.address
        );
      } catch (err) {
        helpers.setStatus({ success: false });
        helpers.setSubmitting(false);
      }
    },
  });

  return (
    <form noValidate
onSubmit={formik.handleSubmit}>
      <Card>
        <CardHeader title="Profile" />
        <CardContent sx={{ pt: 0 }}>
          <Box sx={{ m: -1.5 }}>
            <Grid container
spacing={3}>
              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.name && formik.errors.name)}
                  fullWidth
                  helperText={formik.touched.name && formik.errors.name}
                  label="Full Name"
                  name="name"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.name}
                />
              </Grid>

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.email && formik.errors.email)}
                  helperText={formik.touched.email && formik.errors.email}
                  fullWidth
                  label="Email Address"
                  name="email"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="email"
                  value={formik.values.email}
                />
              </Grid>

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.contactNumber && formik.errors.contactNumber)}
                  fullWidth
                  helperText={formik.touched.contactNumber && formik.errors.contactNumber}
                  label="Phone Number"
                  name="contactNumber"
                  type="number"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.contactNumber}
                />
              </Grid>

              <Grid xs={12}
md={3}>
                <TextField
                  error={!!(formik.touched.age && formik.errors.age)}
                  fullWidth
                  helperText={formik.touched.age && formik.errors.age}
                  label="Age"
                  type="number"
                  name="age"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.age}
                />
              </Grid>

              <Grid xs={12}
md={3} >
                <TextField
                  error={!!(formik.touched.gender && formik.errors.gender)}
                  fullWidth
                  helperText={formik.touched.gender && formik.errors.gender}
                  label="Gender"
                  name="gender"
                  required
                  select
                  SelectProps={{ native: true }}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.gender}
                >
                  {Genders.map((option) => (
                    <option key={option.value}
value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Grid>

              <Grid xs={12}
md={3}>
                <TextField
                  error={!!(formik.touched.planChoice && formik.errors.planChoice)}
                  fullWidth
                  helperText={formik.touched.planChoice && formik.errors.planChoice}
                  label="Plan Choice"
                  name="planChoice"
                  required
                  select
                  SelectProps={{ native: true }}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.planChoice}
                >
                  {planChoices.map((option) => (
                    <option key={option.value}
value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Grid>

              <Grid xs={12}
md={3}>
                <TextField
                  error={!!(formik.touched.membershipFee && formik.errors.membershipFee)}
                  fullWidth
                  helperText={formik.touched.membershipFee && formik.errors.membershipFee}
                  label="MemberShip Fee"
                  name="membershipFee"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.membershipFee}
                  type="number"
                />
              </Grid>

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.paymentMethod && formik.errors.paymentMethod)}
                  fullWidth
                  helperText={formik.touched.paymentMethod && formik.errors.paymentMethod}
                  label="Payment Method"
                  name="paymentMethod"
                  required
                  select
                  SelectProps={{ native: true }}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.paymentMethod}
                >
                  {paymentMethods.map((option) => (
                    <option key={option.value}
value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Grid>

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.preferredSlot && formik.errors.preferredSlot)}
                  fullWidth
                  helperText={formik.touched.preferredSlot && formik.errors.preferredSlot}
                  label="Preferred Slot"
                  name="preferredSlot"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  required
                  select
                  SelectProps={{ native: true }}
                  value={formik.values.preferredSlot}
                >
                  {preferredSlots.map((option) => (
                    <option key={option.value}
value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </TextField>
              </Grid>

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.address && formik.errors.address)}
                  fullWidth
                  helperText={formik.touched.address && formik.errors.address}
                  label="Address"
                  name="address"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.address}
                />
              </Grid>

              <Grid xs={12}
md={4}
lg={12}>
                {/* Checkboxes for Services */}
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="OnlyGym"
                      checked={formik.values.services.includes('OnlyGym')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Only Gym"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="Cardio"
                      checked={formik.values.services.includes('Cardio')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Cardio"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="GroupClasses"
                      checked={formik.values.services.includes('GroupClasses')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Group Classes"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="PersonalTraning"
                      checked={formik.values.services.includes('PersonalTraning')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Personal Traning"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="Zumba"
                      checked={formik.values.services.includes('Zumba')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Zumba"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="Yoga"
                      checked={formik.values.services.includes('Yoga')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Yoga"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="IndoorCycling"
                      checked={formik.values.services.includes('IndoorCycling')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Indoor Cycling"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="services"
                      value="CrossFit"
                      checked={formik.values.services.includes('CrossFit')}
                      onChange={formik.handleChange}
                    />
                  }
                  label="Cross Fit"
                />
              </Grid>
            </Grid>
          </Box>
        </CardContent>
        <Divider />
        <CardActions sx={{ justifyContent: 'flex-end' }}>
          <Button size="large"
type="submit"
variant="contained">
            Save details
          </Button>
        </CardActions>
      </Card>
      <ToastContainer />
    </form>
  );
};
