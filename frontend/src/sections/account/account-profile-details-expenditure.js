import 'react-toastify/dist/ReactToastify.css';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { ToastContainer, toast } from 'react-toastify';

import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  Unstable_Grid2 as Grid,
  FormControlLabel,
  Checkbox,
} from '@mui/material';


async function signUp(title, description, amount) {
  const charCode=sessionStorage.getItem("charCode");
  const requestBody = {
    charCode,
    title,
    description,
    amount
  };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/addExpenditure', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });


    if (response.status === 200) {
      const responseData = await response.json();
      location.href = "/expenditure";
      toast(responseData.message, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "success" });
    } else {
      const responseData = await response.json();
      toast(responseData.error, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "error"
      });
    }
  } catch (error) {
    console.error('Error during adding expenditure', error);
  }
}

export const AccountProfileDetailsExpenditure = () => {
  const formik = useFormik({
    initialValues: {
      title: '',
      description: '',
      amount:'',
      submit: null,
    },
    validationSchema: Yup.object({
      title: Yup.string().max(255).required('Title is required'),
      description: Yup.string().max(255).required('Description is required'),
      amount: Yup.number().required('Amount is required')

    }),
    onSubmit: async (values, helpers) => {
      try {
        await signUp(
          values.title,
          values.description,
          values.amount
        
        );
      } catch (err) {
        helpers.setStatus({ success: false });
        helpers.setSubmitting(false);
      }
    },
  });

  return (
    <form noValidate
onSubmit={formik.handleSubmit}>
      <Card>
        <CardHeader title="Expenditure details" />
        <CardContent sx={{ pt: 0 }}>
          <Box sx={{ m: -1.5 }}>
            <Grid container
spacing={3}>
              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.title && formik.errors.title)}
                  fullWidth
                  helperText={formik.touched.title && formik.errors.title}
                  label="Title"
                  name="title"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.title}
                />
              </Grid>

              <Grid xs={12}
md={4}>
                <TextField
                  error={!!(formik.touched.description && formik.errors.description)}
                  helperText={formik.touched.description && formik.errors.description}
                  fullWidth
                  label="Description"
                  name="description"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.description}
                />
              </Grid> <Grid xs={12}
md={4}>
              <TextField
                error={!!(formik.touched.amount && formik.errors.amount)}
                helperText={formik.touched.amount && formik.errors.amount}
                fullWidth
                label="Amount"
                type="number"
                name="amount"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.amount}
              />
            </Grid>

             

         

        
            </Grid>
          </Box>
        </CardContent>
        <Divider />
        <CardActions sx={{ justifyContent: 'flex-end' }}>
          <Button 
           size="large"
type="submit"
variant="contained">

         

            Save
          </Button>
        </CardActions>
      </Card>
      <ToastContainer />
    </form>
  );
};
