import React, { useState } from 'react';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  TextField,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Stack
} from '@mui/material';
import { useFormik } from 'formik';
import { Hourglass } from 'react-loader-spinner'; // Import the Hourglass component


const FinancialInformation = ({ user }) => (
  <CardContent style={{ paddingBottom: '25px', textAlign: 'center' }}>
    <Typography variant="h6"
gutterBottom>
      Financial Information
    </Typography>
    <TableContainer>
      <Table>
        <TableBody>
          <TableRow>
            <TableCell><b>Total Revenue:</b></TableCell>
            <TableCell>₹ {user.totalRevenue}</TableCell>
          </TableRow>
          {/* ... (other rows) ... */}
          <TableRow>
            <TableCell><b>Average Monthly Revenue:</b></TableCell>
            <TableCell>₹ {user.avgMonthlyRevenue}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell><b>Total Expenditure:</b></TableCell>
            <TableCell>₹ {user.totalExpenditure}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell><b>Average Monthly Expenditure:</b></TableCell>
            <TableCell>₹ {user.avgMonthlyExpenditure}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell><b>Total Profit:</b></TableCell>
            <TableCell>₹ {user.totalProfit}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell><b>Average Monthly Profit:</b></TableCell>
            <TableCell>₹ {user.avgMonthlyProfit}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell><b>Total Members:</b></TableCell>
            <TableCell>{user.totalMembers}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell><b>Total Months:</b></TableCell>
            <TableCell>₹ {user.totalMonths}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell><b>Expected Monthly Salary:</b></TableCell>
            <TableCell>₹ {user.expectedMonthlySalary}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell><b>Should Charge Extra:</b></TableCell>
            <TableCell>{user.shouldChargeExtra}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell><b>Extra Charge From Each Member:</b></TableCell>
            <TableCell>₹ {user.howMuchFromEachMember}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  </CardContent>
);

const MonthlyRevenue = ({ tableData }) => (
  <CardContent style={{ textAlign: 'center' }}>

    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Month Year</TableCell>
            <TableCell>Monthly Revenue</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {tableData.map((row) => (
            <TableRow key={row.monthYear}>
              <TableCell>{row.monthYear}</TableCell>
              <TableCell>₹ {row.totalMembershipFee}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  </CardContent>
);

export const AccountProfileFinancial = () => {
  const [user, setUser] = useState({});
  const [tableData, setTableData] = useState([]);
  const [isDataLoaded, setIsDataLoaded] = useState(false);
  const [loading, setLoading] = useState(false);

  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();

    return `${day}-${month}-${year}`;
  };

  const formik = useFormik({
    initialValues: {
      expectedMonthlySalary: '',
    },
    onSubmit: () => {
      handleFetchData();
      setLoading(true);
    },
  });

  const handleFetchData = () => {
    const USER_PROFILE_API = `https://fithubbackend.onrender.com/financial-strategy?charCode=${sessionStorage.getItem("charCode")}&expectedMonthlySalary=${formik.values.expectedMonthlySalary}`;

    fetch(USER_PROFILE_API)
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        if (data.success) {
          setUser(data.data);
          setTableData(data.data.monthlyRevenue);
          setIsDataLoaded(true);
        } else {
          console.error('Error fetching user profile data:', data.message);
          setIsDataLoaded(false);
        }
      })
      .catch(error => {
        console.error('Error during user profile data fetch:', error);
        setIsDataLoaded(false);
      })
      .finally(() => {
        setLoading(false); // Set loading to false regardless of success or failure
      });
  };

  return (
    <Card>
      {loading && ( // Show loading spinner
        <Box
          sx={{
            flexGrow: 1,
            py: 8,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Hourglass
            visible={true}
            height="80"
            width="80"
            ariaLabel="hourglass-loading"
            wrapperStyle={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
            wrapperClass=""
            colors={['#306cce', '#72a1ed']}
          />
        </Box>
      )}

      <CardActions style={{ justifyContent: 'center' }}>
        <TextField
          label="Enter Expected Monthly Salary"
          variant="outlined"
          name="expectedMonthlySalary"
          value={formik.values.expectedMonthlySalary}
          onChange={formik.handleChange}
          style={{ width: '300px' }} // Adjust the width value as needed
        />
        <Button
          style={{ marginLeft: '30px' }}
          variant="contained"
          color="primary"
          onClick={formik.handleSubmit}
        >
          Get
        </Button>
      </CardActions>

      {isDataLoaded && !loading && ( // Show content only if data is loaded and not loading
        <>
          <FinancialInformation user={user} />
          <Divider />
          <MonthlyRevenue tableData={tableData} />
        </>
      )}
    </Card>
  );
};