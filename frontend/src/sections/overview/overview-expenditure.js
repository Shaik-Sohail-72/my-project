import PropTypes from 'prop-types';
import GroupRemoveIcon from '@mui/icons-material/GroupRemove';
import CurrencyExchangeIcon from '@mui/icons-material/CurrencyExchange';
import ArrowPathIcon from '@heroicons/react/24/solid/ArrowPathIcon';
import ArrowRightIcon from '@heroicons/react/24/solid/ArrowRightIcon';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  SvgIcon
} from '@mui/material';
import {
  Avatar,
  Box,

  LinearProgress,
  Stack,
  Typography
} from '@mui/material';

export const OverviewExpenditure = (props) => {
  const { value, sx, color } = props;
  const currentYear =new Date().getFullYear();

  return (
    <Card sx={sx}>
      <CardContent>
        <Stack
          alignItems="flex-start"
          direction="row"
          justifyContent="space-between"
          spacing={3}
        >
          <Stack spacing={1}>
            <Typography
              color="text.secondary"
              gutterBottom
              variant="overline"
            >
              Expenditures
            </Typography>
            <Typography variant="h4">
              {value} %
            </Typography>
          </Stack>
          <Avatar
            sx={{
              backgroundColor: 'error.main',
              height: 56,
              width: 56
            }}
          >
            <SvgIcon>
              <CurrencyExchangeIcon />
            </SvgIcon>
          </Avatar>
        </Stack>
        <Box sx={{ mt: 3 }}>
          <LinearProgress
          color='error'
            value={value} 
            variant="determinate"
          />
        </Box>

        <Typography
              color="text.secondary"
              variant="caption"
            >
              Total Expenditure [ {currentYear} ]
            </Typography>
        
      </CardContent>
      <Button
          color="inherit"
          href="/overviewExpenditurePage"
          endIcon={(
            <SvgIcon fontSize="small">
              <ArrowRightIcon />
            </SvgIcon>
          )}
          size="small"
        >
          Overview
        </Button>
    </Card>
  );
};

OverviewExpenditure.propTypes = {
  value: PropTypes.number.isRequired,
  sx: PropTypes.object
};
