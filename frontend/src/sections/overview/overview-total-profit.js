import PropTypes from 'prop-types';
import CurrencyDollarIcon from '@heroicons/react/24/solid/CurrencyDollarIcon';
import { Avatar, Card, Box, LinearProgress, CardContent, Stack, SvgIcon,Button, Typography } from '@mui/material';

import ArrowPathIcon from '@heroicons/react/24/solid/ArrowPathIcon';
import ArrowRightIcon from '@heroicons/react/24/solid/ArrowRightIcon';
import CurrencyRupeeIcon from '@mui/icons-material/CurrencyRupee';
export const OverviewTotalProfit = (props) => {
  const { value, sx, color } = props;
  const currentYear =new Date().getFullYear();

  return (
    <Card sx={sx}>
      <CardContent>
        <Stack
          alignItems="flex-start"
          direction="row"
          justifyContent="space-between"
          spacing={3}
        >
          <Stack spacing={1}>
            <Typography
              color="text.secondary"
              variant="overline"
            >
              Profit
            </Typography>
            <Typography variant="h4">
              {value}
            </Typography>
          </Stack>
          <Avatar
            sx={{
              backgroundColor: 'success.main',
              height: 56,
              width: 56
            }}
          >
            <SvgIcon>
            <CurrencyRupeeIcon />
            </SvgIcon>
          </Avatar>
        </Stack>
        <Box sx={{ mt: 3 }}>
          <LinearProgress
            value='100'
            color='success'
            variant="determinate"
          />
        </Box>
        
        <Typography
              color="text.secondary"
              variant="caption"
            >
              Total Profit [ {currentYear} ]
            </Typography>
            
      </CardContent>
      <Button
      color="inherit"
      href="/overviewProfitPage"
      endIcon={(
        <SvgIcon fontSize="small">
          <ArrowRightIcon />
        </SvgIcon>
      )}
      size="small"
    >
      Overview
    </Button>
    </Card>
  );
};

OverviewTotalProfit.propTypes = {
  value: PropTypes.string,
  sx: PropTypes.object
};
