import PropTypes from 'prop-types';
import ArrowDownIcon from '@heroicons/react/24/solid/ArrowDownIcon';
import ArrowUpIcon from '@heroicons/react/24/solid/ArrowUpIcon';
import CurrencyDollarIcon from '@heroicons/react/24/solid/CurrencyDollarIcon';
import { Avatar, Card, CardContent, Stack, SvgIcon, Typography } from '@mui/material';
import ArrowRightIcon from '@heroicons/react/24/solid/ArrowRightIcon';
import CurrencyRupeeIcon from '@mui/icons-material/CurrencyRupee';

import {
  Box,
  Button,
  LinearProgress,
} from '@mui/material';

export const OverviewRevenue = (props) => {
  const { difference, positive = false, sx, value } = props;
  const currentYear =new Date().getFullYear();
  return (
    
    <Card sx={sx}>
      <CardContent>
        <Stack
          alignItems="flex-start"
          direction="row"
          justifyContent="space-between"
          spacing={0}
        >
          <Stack spacing={1}>
            <Typography
              color="text.secondary"
              variant="overline"
            >
              Revenue 
            </Typography>
            <Typography variant="h4">
              {value}
            </Typography>
          </Stack>
          <Avatar
            sx={{
              backgroundColor: 'warning.main',
              height: 56,
              width: 56
            }}
          >
            <SvgIcon>
              <CurrencyRupeeIcon />
            </SvgIcon>
          </Avatar>
        </Stack>
        {difference && (
          <Stack
            alignItems="center"
            direction="row"
            spacing={2}
            sx={{ mt: 2 }}
          >
            <Stack
              alignItems="center"
              direction="row"
              spacing={0.5}
            >
              <SvgIcon
                color={positive ? 'success' : 'error'}
                fontSize="small"
              >
                {positive ? <ArrowUpIcon /> : <ArrowDownIcon />}
              </SvgIcon>
              
            </Stack>
            
            <Typography
              color="text.secondary"
              variant="caption"
            >
              Total Renenew [ {currentYear} ]
            </Typography>
          </Stack>
        )}
      </CardContent>
       <Button
      color="inherit"
      href="/overviewRevenuePage"
      endIcon={(
        <SvgIcon fontSize="small">
          <ArrowRightIcon />
        </SvgIcon>
      )}
      size="small"
    >
      Overview
    </Button>
    </Card>
  );
};

OverviewRevenue.prototypes = {
  difference: PropTypes.number,
  positive: PropTypes.bool,
  sx: PropTypes.object,
  value: PropTypes.string.isRequired
};
