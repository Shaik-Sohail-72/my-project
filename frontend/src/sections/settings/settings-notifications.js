import { useCallback, useState } from 'react';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  FormControlLabel,
  Stack,
  Typography,
  Unstable_Grid2 as Grid
} from '@mui/material';

export const SettingsNotifications = () => {
  const [selectedOptions, setSelectedOptions] = useState('');

  const handleCheckboxChange = useCallback(
    (event, option) => {
      if (event.target.checked) {
        setSelectedOptions((prevOptions) => prevOptions + option + ' ');
      } else {
        setSelectedOptions((prevOptions) =>
          prevOptions.replace(new RegExp(option + ' ', 'g'), '')
        );
      }
    },
    []
  );

  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();
      // You can use the selectedOptions state as needed (e.g., send it to the server).
      console.log('Selected Options:', selectedOptions);
    },
    [selectedOptions]
  ); 

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader subheader="Manage the notifications"
title="Notifications" />
        <Divider />
        <CardContent>
          <Grid container
spacing={6}
wrap="wrap">
            <Grid xs={12}
sm={6}
md={4}>
              <Stack spacing={1}>
                <Typography variant="h6">Notifications</Typography>
                <Stack>
                  <FormControlLabel
                    control={
                      <Checkbox
                        defaultChecked
                        onChange={(event) => handleCheckboxChange(event, 'Email')}
                      />
                    }
                    label="Email"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        defaultChecked
                        onChange={(event) => handleCheckboxChange(event, 'Push Notifications')}
                      />
                    }
                    label="Push Notifications"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        onChange={(event) => handleCheckboxChange(event, 'Text Messages')}
                      />
                    }
                    label="Text Messages"
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        defaultChecked
                        onChange={(event) => handleCheckboxChange(event, 'Phone calls')}
                      />
                    }
                    label="Phone calls"
                  />
                </Stack>
              </Stack>
            </Grid>
            {/* ... (similar structure for the second grid) */}
          </Grid>
        </CardContent>
        <Divider />
        <CardActions sx={{ justifyContent: 'flex-end' }}>
          <Button type="submit"
variant="contained">
            Save
          </Button>
        </CardActions>
      </Card>
    </form>
  );
};
