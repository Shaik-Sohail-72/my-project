import Head from 'next/head';
import { Box, Container, Typography, Link } from '@mui/material';
import PhoneOutlined from '@mui/icons-material/PhoneOutlined';
import WhatsApp from '@mui/icons-material/WhatsApp'; // Corrected import
import { Layout as DashboardLayout } from 'src/layouts/dashboard/layout';

const Page = () => {
  

  return (
    <>
      <Head>
        <title>Support</title>
      </Head>
      
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
export default Page;
