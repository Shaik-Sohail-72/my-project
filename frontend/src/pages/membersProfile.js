import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import ArrowUpOnSquareIcon from '@heroicons/react/24/solid/ArrowUpOnSquareIcon';
import ArrowDownOnSquareIcon from '@heroicons/react/24/solid/ArrowDownOnSquareIcon';
import PlusIcon from '@heroicons/react/24/solid/PlusIcon';
import { Hourglass } from 'react-loader-spinner'; // Import the Hourglass component

import jsPDF from 'jspdf';
import {
  Box,
  Button,
  Container,
  CircularProgress,
  Pagination,
  Stack,
  SvgIcon,
  Typography,
  Grid
} from '@mui/material';
import axios from 'axios';
import { Layout as DashboardLayout } from 'src/layouts/dashboard/layout';
import { CompanyCard } from 'src/sections/companies/company-card';
import { CompaniesSearch } from 'src/sections/companies/companies-search';
import 'jspdf-autotable';

const companiesPerPage = 6;

const CompaniesPage = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [companyData, setCompanyData] = useState({
    original: [],
    filtered: [],
  });
  const [loading, setLoading] = useState(true);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const charCode = sessionStorage.getItem("charCode");
        const response = await axios.get(`https://fithubbackend.onrender.com/membersprofile?charCode=${charCode}`);
        const originalCompanies = response.data.members;
        setCompanyData({
          original: originalCompanies,
          filtered: originalCompanies,
        });
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };
  const handleAddButtonClick = () => {
    location.href = '/addMember';
  };

  const handleSearchChange = (value) => {
    setSearchTerm(value);

    const newFilteredCompanies = companyData.original.filter((company) => {
      const memberIdMatches = company.memberId.toString().toLowerCase().includes(value.toLowerCase());
      const fullNameMatches = company.name.toLowerCase().includes(value.toLowerCase());
      const phoneNumberMatches = company.contactNumber.toString().includes(value);

      return memberIdMatches || fullNameMatches || phoneNumberMatches;
    });

    setCompanyData({
      ...companyData,
      filtered: newFilteredCompanies,
    });
    setCurrentPage(1);

    // Check if the search term is empty and reset the filtered companies to the original list
    if (value === '') {
      setCompanyData({
        ...companyData,
        filtered: companyData.original,
      });
    }
  };
  const formatTime = (dateObject) => {
    const hours = dateObject.getHours() % 12 || 12;
    const minutes = dateObject.getMinutes().toString().padStart(2, '0');
    const seconds = dateObject.getSeconds().toString().padStart(2, '0');
    const ampm = dateObject.getHours() >= 12 ? 'PM' : 'AM';

    return `${hours}:${minutes}:${seconds} ${ampm}`;
  };

  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();

    return `${day}-${month}-${year}`;
  };

  const downloadPDF = () => {
    const headers = [
      'Member ID',
      'Name',
      'Admission Date',
      'Last Renewal Date',
      'Valid Upto',
      'Email',
      'Phone Number',
      'Age',
      'Gender',
      'Plan Choice',
      'Membership Fee',
      'Payment Method',
      'Time Slot',
      'Services',
      'Address',
    ];

    const doc = new jsPDF({
      format: [750, 500],
      orientation: 'landscape',
    });

    doc.setProperties({
      title: 'All Members Exported Data',
    });

    doc.autoTable({
      head: [headers],
      body: companyData.filtered.map((company) => [
        company.memberId,
        company.name,
        formatDate(company.admissionDate),
        formatDate(company.lastRenewalDate),
        formatDate(company.validUptoDate),
        company.email,
        company.contactNumber,
        company.age,
        company.gender,
        company.planChoice,
        company.membershipFee,
        company.paymentMethod,
        company.preferredSlot,
        company.services,
        company.address,
      ]),
    });

    const additionalContent = `Date and Time    ${formatDate(new Date())} ${formatTime(new Date())}`;
    doc.text(additionalContent, 10, doc.autoTable.previous.finalY + 10);

    doc.save('All Members with details Exported Data.pdf');
  };

  const startIndex = (currentPage - 1) * companiesPerPage;
  const endIndex = startIndex + companiesPerPage;

  return (
    <>
      <Head>
        <title>All members profile</title>
      </Head>
      <Box
      component="main"
      sx={{
        flexGrow: 1,
        py: 8,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center', // Center the loading spinner
      }}
    >
      {loading ? (
        <Hourglass
          visible={true}
          height="80"
          width="80"
          ariaLabel="hourglass-loading"
          wrapperStyle={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
          wrapperClass=""
          colors={['#306cce', '#72a1ed']}
        />
      ) : (
        <Container maxWidth="xl">
          <Stack spacing={3}>
            <Stack
              direction="row"
              justifyContent="space-between"
              spacing={4}
            >
              <Stack spacing={1}>
                <Typography variant="h4">Members Profile</Typography>
                <Stack alignItems="center"
direction="row"
spacing={1}>
                  <Button
                    onClick={() => downloadPDF()}
                    color="inherit"
                    startIcon={
                      <SvgIcon fontSize="small">
                        <ArrowDownOnSquareIcon />
                      </SvgIcon>
                    }
                  >
                    Export
                  </Button>
                </Stack>
              </Stack>
              <div>
                <Button
                  startIcon={
                    <SvgIcon fontSize="small">
                      <PlusIcon />
                    </SvgIcon>
                  }
                  variant="contained"
                  onClick={handleAddButtonClick}
                >
                  Add
                </Button>
              </div>
            </Stack>
            <CompaniesSearch onChange={handleSearchChange} />

              <Grid container
spacing={3}>
                {companyData.filtered.slice(startIndex, endIndex).map((company) => (
                  <Grid item
xs={12}
md={6}
lg={4}
key={company.memberId}>
                    <CompanyCard company={company} />
                  </Grid>
                ))}
              </Grid>
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                marginTop: 2,
              }}
            >
              <Pagination
                count={Math.ceil(companyData.filtered.length / companiesPerPage)}
                page={currentPage}
                size="small"
                onChange={handlePageChange}
              />
            </Box>
          </Stack>
        </Container>
      )}
      </Box>
    </>
  );
};

CompaniesPage.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default CompaniesPage;



