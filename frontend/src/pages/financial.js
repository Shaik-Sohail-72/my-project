import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { Box, Container, Stack, Typography, Button, SvgIcon, Card, OutlinedInput, InputAdornment } from '@mui/material';
import ArrowDownOnSquareIcon from '@heroicons/react/24/solid/ArrowDownOnSquareIcon';
import PlusIcon from '@heroicons/react/24/solid/PlusIcon';
import MagnifyingGlassIcon from '@heroicons/react/24/solid/MagnifyingGlassIcon';
import { useSelection } from 'src/hooks/use-selection';
import { Layout as DashboardLayout } from 'src/layouts/dashboard/layout';
import { CustomersTableFinancial } from 'src/sections/customer/customers-table financial';
import axios from 'axios';
import { Hourglass } from 'react-loader-spinner';

const FinancialPage = () => {
  const [loading, setLoading] = useState(true);
  const [financialData, setFinancialData] = useState({});
  const [expectedSalary, setExpectedSalary] = useState('');
  const [searchTerm, setSearchTerm] = useState('');
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  // Similar to the member page, you can define your custom hooks, functions, and useEffects here.

  useEffect(() => {
    // Fetch financial data based on the expected salary
    const fetchData = async () => {
      try {
        const response = await axios.get(`https://fithubbackend.onrender.com/financial-strategy?charCode=KUIS&expectedMonthlySalary=${expectedSalary}`);
        const responseData = response.data;
        setFinancialData(responseData.data);
        console.log(setFinancialData);
      } catch (error) {
        console.error('Error during financial data fetch', error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [expectedSalary]);

  // Define your other functions and hooks here

  return (
    <>
      <Head>
        <title>Financial Strategy</title>
      </Head>
      <Box component="main"
sx={{ flexGrow: 1, py: 8, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        {loading ? (
          <Hourglass
            visible={true}
            height="80"
            width="80"
            ariaLabel="hourglass-loading"
            wrapperStyle={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
            wrapperClass=""
            colors={['#306cce', '#72a1ed']}
          />
        ) : (
          <Container maxWidth="xl">
            <Stack spacing={3}>
              {/* Your page content here */}
              <Stack direction="row"
justifyContent="space-between"
spacing={4}>
                <Stack spacing={1}>
                  <Typography variant="h4">Financial Strategy</Typography>
                  <OutlinedInput
                    value={expectedSalary}
                    onChange={(e) => setExpectedSalary(e.target.value)}
                    fullWidth
                    placeholder="Enter Expected Monthly Salary"
                    startAdornment={(
                      <InputAdornment position="start">
                        <SvgIcon color="action"
fontSize="small">
                          <MagnifyingGlassIcon />
                        </SvgIcon>
                      </InputAdornment>
                    )}
                  />
                </Stack>
                <div>
                  <Button
                    color="inherit"
                    onClick={() => downloadPDF(financialData.monthlyRevenue || [])}
                    startIcon={
                      <SvgIcon fontSize="small">
                        <ArrowDownOnSquareIcon />
                      </SvgIcon>
                    }
                  >
                    Export
                  </Button>
                </div>
              </Stack>
              {/* Other components such as FinancialTable */}
              <CustomersTableFinancial
                // Other props as needed
              />
            </Stack>
          </Container>
        )}
      </Box>
    </>
  );
};

FinancialPage.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default FinancialPage;
