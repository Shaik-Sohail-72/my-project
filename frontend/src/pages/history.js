import React, { useCallback, useMemo, useState, useEffect } from 'react';
import Head from 'next/head';
import ArrowDownOnSquareIcon from '@heroicons/react/24/solid/ArrowDownOnSquareIcon';
import { Box, Button, Container, Stack, SvgIcon, Typography } from '@mui/material';
import { useSelection } from 'src/hooks/use-selection';
import { Layout as DashboardLayout } from 'src/layouts/dashboard/layout';
import { CustomersSearch } from 'src/sections/customer/customers-search';
import { applyPagination } from 'src/utils/apply-pagination';
import axios from 'axios';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Hourglass } from 'react-loader-spinner'; // Import the Hourglass component
import { CustomersTableHistory } from 'src/sections/customer/customers-table-history';

const Page = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchTerm, setSearchTerm] = useState('');
  const [memberData, setMemberData] = useState([]);
  const [loading, setLoading] = useState(true); // Added loading state

  const useCustomers = useMemo(() => {
    const data = memberData.members || [];

    if (searchTerm) {
      const filteredData = data.filter((customer) => {
        const memberIdMatches = customer.memberId.toString().toLowerCase().includes(searchTerm.toLowerCase());
        const phoneNumberMatches = customer.contactNumber && customer.contactNumber.toString().includes(searchTerm);
        const fullNameMatches = customer.name.toLowerCase().includes(searchTerm.toLowerCase());
      
        return memberIdMatches || phoneNumberMatches || fullNameMatches;
      });

      return applyPagination(filteredData, page, rowsPerPage);
    }

    return applyPagination(data, page, rowsPerPage);
  }, [page, rowsPerPage, searchTerm, memberData]);

  const customersIds = useMemo(() => {
    return memberData.members ? memberData.members.map((customer) => customer.id) : [];
  }, [memberData.members]);

  const customersSelection = useSelection(customersIds);

  const fetchMemberData = async () => {
    try {
      const charCode = sessionStorage.getItem("charCode");
      const response = await axios.get(`https://fithubbackend.onrender.com/history?charCode=${charCode}`);
      const responseData = response.data;
      setMemberData(responseData);
    } catch (error) {
      console.error('Error during member data fetch', error);
    } finally {
      setLoading(false); // Set loading to false whether the request succeeds or fails
    }
  };

  useEffect(() => {
    fetchMemberData();
  }, []);

  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();

    return `${day}-${month}-${year}`;
  };

  const formatDateTime = (dateString) => {
    const dateObject = new Date(dateString);
  
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    let hours = dateObject.getHours().toString().padStart(2, '0');
    const minutes = dateObject.getMinutes().toString().padStart(2, '0');
    const seconds = dateObject.getSeconds().toString().padStart(2, '0');
    
    // Determine AM/PM
    const ampm = hours >= 12 ? 'PM' : 'AM';
  
    // Convert hours to 12-hour format
    hours = (hours % 12) || 12;
  
    return `${day}-${month}-${year} ${hours}:${minutes}:${seconds} ${ampm}`;
  };

  const formatTime = (dateObject) => {
    const hours = dateObject.getHours() % 12 || 12;
    const minutes = dateObject.getMinutes().toString().padStart(2, '0');
    const seconds = dateObject.getSeconds().toString().padStart(2, '0');
    const ampm = dateObject.getHours() >= 12 ? 'PM' : 'AM';

    return `${hours}:${minutes}:${seconds} ${ampm}`;
};


const downloadPDF = () => {
  const headers = [
    'Member ID',
    'Name',
    'Phone Number',
    'Action',
    'Plan Choice',
    'Membership Fee',
    'Paid For',
    'Valid Upto',
    'Payment Method',
    'Services',
    'Timestamp',
  ];

  const columnWidths = [50, 50, 30, 35, 30, 30, 30, 30, 30, 40, 40, 35, 35]; // Set explicit widths for each column

  const doc = new jsPDF({
    orientation: 'landscape',
    unit: 'pt', // Set unit to points
    format: [1400, 500], // Set a custom width and height in points
  });
  

  // Set document properties
  doc.setProperties({
    title: 'History exported successfully',
  });

  // Create table with specific column width
  doc.autoTable({
    head: [headers],
    body: useCustomers.map((customer) => [
      customer.memberId,
      customer.name,
      customer.contactNumber,
      customer.action,
      customer.planChoice,
      customer.membershipFee,
      formatDate(customer.paidFor),
      formatDate(customer.validUptoDate),
      customer.paymentMethod,
      customer.services,
      formatDateTime(customer.timestamp),
    
    ]),
    columnWidths: columnWidths,  // Set explicit column widths
  });

  // Add additional content below the table with current date and time
  const additionalContent = `Date and Time    ${formatDate(new Date())} ${formatTime(new Date())}`;
  doc.text(additionalContent, 10, doc.autoTable.previous.finalY + 10);

  // Save the PDF file
  doc.save('History.pdf');
};



  


  const handlePageChange = useCallback((event, value) => {
    setPage(value);
  }, []);

  const handleRowsPerPageChange = useCallback((event) => {
    setRowsPerPage(event.target.value);
    setPage(0); // Reset to the first page when rows per page changes
  }, []);

  

  const handleSearchChange = useCallback((value) => {
    setSearchTerm(value);
  }, []);

  return (
    <>
      <Head>
        <title>History</title>
      </Head>
      <Box component="main"
sx={{ flexGrow: 1, py: 8, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        {loading ? (
          <Hourglass
            visible={true}
            height="80"
            width="80"
            ariaLabel="hourglass-loading"
            wrapperStyle={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
            wrapperClass=""
            colors={['#306cce', '#72a1ed']}
          />
        ) : (
          <Container maxWidth="xl">
            <Stack spacing={3}>
              <Stack direction="row"
justifyContent="space-between"
spacing={4}>
                <Stack spacing={1}>
                  <Typography variant="h4">History</Typography>
                  <Stack alignItems="center"
direction="row"
spacing={1}>
                    <Button
                      color="inherit"
                      onClick={() => downloadPDF(memberData.members || [])}
                      startIcon={
                        <SvgIcon fontSize="small">
                          <ArrowDownOnSquareIcon />
                        </SvgIcon>
                      }
                    >
                      Export
                    </Button>
                  </Stack>
                </Stack>
                <div>

                </div>
              </Stack>
              <CustomersSearch onChange={handleSearchChange} />
              <CustomersTableHistory
                count={memberData.members ? memberData.members.length : 0}
                items={useCustomers}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleRowsPerPageChange}

                page={page}
                rowsPerPage={rowsPerPage}
              />
            </Stack>
          </Container>
        )}
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;