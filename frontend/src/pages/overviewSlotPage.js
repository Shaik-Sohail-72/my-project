import { useCallback, useMemo, useState } from 'react';
import Head from 'next/head';
import { subDays, subHours } from 'date-fns';
import ArrowDownOnSquareIcon from '@heroicons/react/24/solid/ArrowDownOnSquareIcon';
import ArrowUpOnSquareIcon from '@heroicons/react/24/solid/ArrowUpOnSquareIcon';
import PlusIcon from '@heroicons/react/24/solid/PlusIcon';
import { Box, Button, Container, Stack, SvgIcon, Typography } from '@mui/material';
import { useSelection } from 'src/hooks/use-selection';
import { Layout as DashboardLayout } from 'src/layouts/dashboard/layout';
import { CustomersTableRevenue } from 'src/sections/customer/customers-table-revenue';
import { CustomersSearch } from 'src/sections/customer/customers-search';
import { applyPagination } from 'src/utils/apply-pagination';
import { CustomersTableExpenditure } from 'src/sections/customer/customers-table-expenditure';
import { CustomersTableProfit } from 'src/sections/customer/customers-table-profit';
import { CustomersTableSlot } from 'src/sections/customer/customers-table-slot';
import ArrowLeftIcon from '@heroicons/react/24/solid/ArrowLeftIcon';


const now = new Date();

const data = [
  {

    month: 'Morning',
    curr: '364',
    prev: '298'
  },
  {

    month: 'Afternoon',
    curr: '218',
    prev: '178'
  },
  {

    month: 'Evening',
    curr: '291',
    prev: '179'
  },
  {

    month: 'Night',
    curr: '583',
    prev: '537'
  },

];
// ... (your imports)

const useCustomers = (page, rowsPerPage, searchTerm) => {
    return useMemo(() => {
      let filteredData = data;
  
      if (searchTerm) {
        // Case-insensitive search by curr or email
        filteredData = data.filter(
          (customer) =>
            customer.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
            customer.email.toLowerCase().includes(searchTerm.toLowerCase())
        );
      }
  
      return applyPagination(filteredData, page, rowsPerPage);
    }, [page, rowsPerPage, searchTerm]);
  };
  
  const useCustomerIds = (customers) => {
    return useMemo(() => {
      return customers.map((customer) => customer.id);
    }, [customers]);
  };
  
  const Page = () => {
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [searchTerm, setSearchTerm] = useState('');
    const customers = useCustomers(page, rowsPerPage, searchTerm);
    const customersIds = useCustomerIds(customers);
    const customersSelection = useSelection(customersIds);
  
    const handlePageChange = useCallback((event, value) => {
      setPage(value);
    }, []);
  
    const handleRowsPerPageChange = useCallback((event) => {
      setRowsPerPage(event.target.value);
    }, []);

    const handleAddButtonClick = () => {
      location.href = '/addMember';
    };
  
    const handleSearchChange = useCallback((value) => {
      setSearchTerm(value);
    }, []);
  
    return (
      <>
        <Head>
          <title>Time Slot Preference Comparison: 2024 vs 2023</title>
        </Head>
        <Box component="main"
sx={{ flexGrow: 1, py: 8 }}>
          <Container maxWidth="xl">

            <Stack spacing={3}>
              <Stack direction="row"
justifyContent="space-between"
spacing={4}>
                <Stack spacing={1}>
                  <Typography variant="h4">Time Slot Preference Comparison: 2024 vs 2023</Typography>
                  <Stack alignItems="center"
direction="row"
spacing={1}>
                    
                  </Stack>
                </Stack>
                <div>
               
                </div>
              </Stack>
 
              <CustomersTableSlot
                count={data.length}
                items={customers}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleRowsPerPageChange}
                onSelectAll={customersSelection.handleSelectAll}
                onSelectOne={customersSelection.handleSelectOne}
                page={page}
                rowsPerPage={rowsPerPage}
                selected={customersSelection.selected}
              />
            </Stack>
          </Container>
        </Box>
        <Box
        sx={{
          mb: 5,
          textAlign: 'center'
        }}
      >
      <Button
    href="/"
    startIcon={(
      <SvgIcon fontSize="small">
        <ArrowLeftIcon />
      </SvgIcon>
    )}
  
    variant="contained"
  >
    Go back to dashboard
  </Button>
  </Box>
      </>
    );
  };
  
  Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
  
  export default Page;
  