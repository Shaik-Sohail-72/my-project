import React, { useCallback, useMemo, useState, useEffect } from 'react';
import Head from 'next/head';
import ArrowDownOnSquareIcon from '@heroicons/react/24/solid/ArrowDownOnSquareIcon';
import PlusIcon from '@heroicons/react/24/solid/PlusIcon';
import { Box, Button, Container, Stack, SvgIcon, Typography } from '@mui/material';
import { useSelection } from 'src/hooks/use-selection';
import { Layout as DashboardLayout } from 'src/layouts/dashboard/layout';
import { CustomersTable } from 'src/sections/customer/customers-table';
import { CustomersSearch } from 'src/sections/customer/customers-search';
import { applyPagination } from 'src/utils/apply-pagination';
import axios from 'axios';
import jsPDF from 'jspdf';

import 'jspdf-autotable';
import { Hourglass } from 'react-loader-spinner'; // Import the Hourglass component
import { CustomersTableEvent } from 'src/sections/customer/customers-table-event';
import { CustomersSearchEvent } from 'src/sections/customer/customers-search-event';

const Page = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchTerm, setSearchTerm] = useState('');
  const [memberData, setMemberData] = useState([]);
  const [loading, setLoading] = useState(true); // Added loading state

  function formatTimeAMPM(time24) {
    // Parse the input time string
    const [hours, minutes] = time24.split(':').map(Number);
  
    // Check if the time is in the AM or PM
    const period = hours >= 12 ? 'PM' : 'AM';
  
    // Convert hours to 12-hour format
    const hours12 = hours % 12 || 12;
  
    // Format the time in AM/PM
    const formattedTime = `${hours12}:${minutes.toString().padStart(2, '0')} ${period}`;
  
    return formattedTime;
  }
  

  const useCustomers = useMemo(() => {
    const data = memberData.events || [];

    if (searchTerm) {
      const filteredData = data.filter((customer) => {
            const titleMatches = customer.title.toString().toLowerCase().includes(searchTerm.toLowerCase());
            const descriptionMatches = customer.description.toString().includes(searchTerm);

            return titleMatches || descriptionMatches;
      });

      return applyPagination(filteredData, page, rowsPerPage);
    }

    return applyPagination(data, page, rowsPerPage);
  }, [page, rowsPerPage, searchTerm, memberData]);

  const customersIds = useMemo(() => {
    return memberData.events ? memberData.events.map((customer) => customer.id) : [];
  }, [memberData.events]);

  const customersSelection = useSelection(customersIds);

  const fetchMemberData = async () => {
    try {
      const charCode = sessionStorage.getItem("charCode");
      const response = await axios.get(`https://fithubbackend.onrender.com/events?charCode=${charCode}`);
      const responseData = response.data;
      console.log(responseData);
      setMemberData(responseData);
    } catch (error) {
      console.error('Error during member data fetch', error);
    } finally {
      setLoading(false); // Set loading to false whether the request succeeds or fails
    }
  };

  useEffect(() => {
    fetchMemberData();
  }, []);

  const formatDateTime = (dateString) => {
    const dateObject = new Date(dateString);
  
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    let hours = dateObject.getHours().toString().padStart(2, '0');
    const minutes = dateObject.getMinutes().toString().padStart(2, '0');
    const seconds = dateObject.getSeconds().toString().padStart(2, '0');
    
    // Determine AM/PM
    const ampm = hours >= 12 ? 'PM' : 'AM';
  
    // Convert hours to 12-hour format
    hours = (hours % 12) || 12;
  
    return `${day}-${month}-${year} ${hours}:${minutes}:${seconds} ${ampm}`;
  };

  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();

    return `${day}-${month}-${year}`;
  };

  const formatTime = (dateObject) => {
    const hours = dateObject.getHours() % 12 || 12;
    const minutes = dateObject.getMinutes().toString().padStart(2, '0');
    const seconds = dateObject.getSeconds().toString().padStart(2, '0');
    const ampm = dateObject.getHours() >= 12 ? 'PM' : 'AM';

    return `${hours}:${minutes}:${seconds} ${ampm}`;
};


const downloadPDF = () => {
  const headers = [
      'Title',
      'Description',
      'Date',
      'Time',
      'Mail Status',
      'Timestamp',
  ];

  const doc = new jsPDF({
      orientation: 'landscape',
  });

  // Set document properties
  doc.setProperties({
      title: 'All Members Exported Data',
  });

  // Create table with default settings
  doc.autoTable({
      head: [headers],
      body: useCustomers.map((customer) => [
          customer.title,
          customer.description,
          formatDate(customer.date),
          formatTimeAMPM(customer.time),
          "Sent - Active "+customer.sentMailStatus,
          formatDateTime(customer.timestamp),
          
      ]),
  });

  // Add additional content below the table with current date and time
  const additionalContent = `Date and Time    ${formatDate(new Date())} ${formatTime(new Date())}`;
  doc.text(additionalContent, 10, doc.autoTable.previous.finalY + 10);

  // Save the PDF file
  doc.save('All Events Exported Data.pdf');
};

  const handlePageChange = useCallback((event, value) => {
    setPage(value);
  }, []);

  const handleRowsPerPageChange = useCallback((event) => {
    setRowsPerPage(event.target.value);
    setPage(0); // Reset to the first page when rows per page changes
  }, []);

  const handleAddButtonClick = () => {
    location.href = '/addEvent';
  };

  const handleSearchChange = useCallback((value) => {
    setSearchTerm(value);
  }, []);

  return (
    <>
      <Head>
        <title>Event Center</title>
      </Head>
      <Box component="main"
sx={{ flexGrow: 1, py: 8, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        {loading ? (
          <Hourglass
            visible={true}
            height="80"
            width="80"
            ariaLabel="hourglass-loading"
            wrapperStyle={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
            wrapperClass=""
            colors={['#306cce', '#72a1ed']}
          />
        ) : (
          <Container maxWidth="xl">
            <Stack spacing={3}>
              <Stack direction="row"
justifyContent="space-between"
spacing={4}>
                <Stack spacing={1}>
                  <Typography variant="h4">Event Center</Typography>
                  <Stack alignItems="center"
direction="row"
spacing={1}>
                    <Button
                      color="inherit"
                      onClick={() => downloadPDF(memberData.members || [])}
                      startIcon={
                        <SvgIcon fontSize="small">
                          <ArrowDownOnSquareIcon />
                        </SvgIcon>
                      }
                    >
                      Export
                    </Button>
                  </Stack>
                </Stack>
                <div>
                  <Button
                    startIcon={
                      <SvgIcon fontSize="small">
                        <PlusIcon />
                      </SvgIcon>
                    }
                    variant="contained"
                    onClick={handleAddButtonClick}
                  >
                    Add Event
                  </Button>
                </div>
              </Stack>
              <CustomersSearchEvent onChange={handleSearchChange} />
              <CustomersTableEvent
                count={memberData.events ? memberData.events.length : 0}
                items={useCustomers}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleRowsPerPageChange}
                onSelectAll={customersSelection.handleSelectAll}
                onSelectOne={customersSelection.handleSelectOne}
                page={page}
                rowsPerPage={rowsPerPage}
                selected={customersSelection.selected}
              />
            </Stack>
          </Container>
        )}
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;