import React, { useCallback, useEffect, useState } from 'react';
import Head from 'next/head';
import ArrowLeftIcon from '@heroicons/react/24/solid/ArrowLeftIcon';
import PlusIcon from '@heroicons/react/24/solid/PlusIcon';
import { Box, Button, Container, Stack, SvgIcon, Typography } from '@mui/material';
import { Unstable_Grid2 as Grid } from '@mui/material';
import { OverviewBigButtonMembers } from 'src/sections/overview/overview-bigbutton-Members';
import { Layout as DashboardLayout } from 'src/layouts/dashboard/layout';
import { Hourglass } from 'react-loader-spinner';
import { CustomersTableTotalMembers } from 'src/sections/customer/customers-table-total-members';
import { applyPagination } from 'src/utils/apply-pagination';

const currentYear = new Date().getFullYear();
const lastYear = currentYear - 1;

const fetchData = async (charCode) => {
  try {
    const response = await fetch(`https://fithubbackend.onrender.com/registrationAndRenewalCount?charCode=${charCode}`);
    const fetchedData = await response.json();

    const currentYear = new Date().getFullYear();

    const months = [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];

    const data = months.map(month => ({
      month,
      curr_regis: fetchedData.newRegistrationsByMonthCurrYear[`${month}-${currentYear}`],
      prev_regis: fetchedData.newRegistrationsByMonthLastYear[`${month}-${currentYear - 1}`],
      curr_ren: fetchedData.renewalsByMonthCurrYear[`${month}-${currentYear}`],
      prev_ren: fetchedData.renewalsByMonthLastYear[`${month}-${currentYear - 1}`],
    }));

    const dataTotal = {
      totalRegistraionCurrYear: fetchedData.totalNewRegistrationsCurrYear,
      totalRegistrationLastYear: fetchedData.totalNewRegistrationsLastYear,
      totalRenewalCurrYear: fetchedData.totalRenewalsCurrYear,
      totalRenewalLastYear: fetchedData.totalRenewalsLastYear,
    };

    return { data, dataTotal };
  } catch (error) {
    console.error('Error fetching data:', error);
    throw error;
  }
};

const useCustomers = (page, rowsPerPage, data) => {
  return applyPagination(data, page, rowsPerPage);
};

const Page = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const charCode = sessionStorage.getItem('charCode');
  const [data, setData] = useState([]);
  const [dataTotal, setDataTotal] = useState({});
  const customers = useCustomers(page, rowsPerPage, data);

  const handlePageChange = useCallback((event, value) => {
    setPage(value);
  }, []);

  const handleRowsPerPageChange = useCallback((event) => {
    setRowsPerPage(event.target.value);
  }, []);

  useEffect(() => {
    const fetchDataAsync = async () => {
      try {
        const { data, dataTotal } = await fetchData(charCode);
        setData(data);
        setDataTotal(dataTotal);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    if (!data.length) {
      fetchDataAsync();
    }
  }, [charCode, data]);

  if (!data.length) {
    return (
      <Box component="main"
sx={{ flexGrow: 1, py: 8, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Hourglass
          visible={true}
          height="80"
          width="80"
          ariaLabel="hourglass-loading"
          wrapperStyle={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
          wrapperClass=""
          colors={['#306cce', '#72a1ed']}
        />
      </Box>
    );
  }

  return (
    <>
      <Head>
        <title>{`Yearly Gym Membership Trends: New Registrations and Renewals: ${currentYear} vs ${lastYear}`}</title>
      </Head>
      <Box component="main"
sx={{ flexGrow: 1, py: 8 }}>
        <Container maxWidth="xl">
          <Stack spacing={3}>
            <Stack direction="row"
justifyContent="space-between"
spacing={4}>
              <Stack spacing={1}>
                <Typography variant="h4">{`Yearly Gym Membership Trends: New Registrations and Renewals: ${currentYear} vs ${lastYear}`}</Typography>
                <Stack alignItems="center"
direction="row"
spacing={1}>
                </Stack>
              </Stack>
              <div>
              </div>
            </Stack>
            <Grid xs={12}
lg={8}>
              <OverviewBigButtonMembers
                chartSeries={[
                  { name: 'This year registrations', data: data.map(item => item.curr_regis) },
                  { name: 'Last year registrations', data: data.map(item => item.prev_regis) },
                  { name: 'This year renewals', data: data.map(item => item.curr_ren) },
                  { name: 'Last year renewals', data: data.map(item => item.prev_ren) }
                ]}
                sx={{ height: '100%' }}
              />
            </Grid>
            <CustomersTableTotalMembers
              totalRegistraionCurrYear={dataTotal.totalRegistraionCurrYear}
              totalRegistrationLastYear={dataTotal.totalRegistrationLastYear}
              totalRenewalCurrYear={dataTotal.totalRenewalCurrYear}
              totalRenewalLastYear={dataTotal.totalRenewalLastYear}
              count={data.length}
              items={customers}
              onPageChange={handlePageChange}
              onRowsPerPageChange={handleRowsPerPageChange}
              page={page}
              rowsPerPage={rowsPerPage}
            />
          </Stack>
        </Container>
      </Box>
      <Box
        sx={{
          mb: 5,
          textAlign: 'center'
        }}
      >
        <Button
          href="/"
          startIcon={(
            <SvgIcon fontSize="small">
              <ArrowLeftIcon />
            </SvgIcon>
          )}
          variant="contained"
        >
          Go back to dashboard
        </Button>
      </Box>
    </>
  );
};

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
