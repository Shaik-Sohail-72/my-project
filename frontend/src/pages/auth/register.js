/*import Head from 'next/head';
import 'react-toastify/dist/ReactToastify.css';
import NextLink from 'next/link';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Box, Button, Link, Stack, TextField, Typography } from '@mui/material';
import { Layout as AuthLayout } from 'src/layouts/auth/layout';
import { ToastContainer, toast } from 'react-toastify';

async function signUp(email, password, reEnterPassword, fullName, phoneNumber, aadharNumber, gymName, city, street, pinCode, insta) {
  const requestBody = {
    email,
    password,
    reenterPassword: reEnterPassword ,
    name: fullName,
    number: phoneNumber,
    aadhar: aadharNumber,
    gymName,
    city,
    street,
    pincode: pinCode,
    insta
  };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/owners/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });
    if (response.status === 200){
      const responseData = await response.json(); 
      location.href="/auth/verifyOtp";
      toast(responseData.message , {position: 'top-right',
      autoClose:5000,
      hideProgressBar:false,
      newestOnTop:false,
      closeOnClick: true,
      rtl:false,
      pauseOnFocusLoss: true,
      draggable: true,
      pauseOnHover: true,
      theme:'light', type:"success"});
    }else{
      const responseData = await response.json(); 
      toast(responseData.error , {position: 'top-right',
      autoClose:5000,
      hideProgressBar:false,
      newestOnTop:false,
      closeOnClick: true,
      rtl:false,
      pauseOnFocusLoss: true,
      draggable: true,
      pauseOnHover: true,
      theme:'light', type:"error"});
    }
  } catch (error) {
      console.error('Error during registration:', error);
  }
}

const RegisterPage = () => {
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      reEnterPassword : '',
      fullName: '',
      phoneNumber: '',
      aadharNumber : '',
      gymName: '',
      city: '',
      street: '',
      pinCode: '',
      insta: '',
      submit: null
    },
    validationSchema: Yup.object({
      email: Yup
        .string()
        .email('Must be a valid email')
        .max(255)
        .required('Email is required'),
      password: Yup.string()
      .max(255)
      .min(8, 'Password must be at least 8 characters long')
      .matches(
        /^(?=.*\d)[A-Za-z\d@$!%*?&]+$/,
        'Password must contain at least one number'
      )
      .required('Password is required'),
      reEnterPassword: Yup
        .string()
        .max(255)
        .oneOf([Yup.ref('password'), null], 'Passwords must match')
        .required('Re-Enter Password is required'),
      fullName: Yup
        .string()
        .max(255)
        .required('Full Name is required'),
      phoneNumber: Yup
        .string()
        .matches(/^[0-9]{10}$/, 'Phone number must be exactly 10 digits')
        .required('Phone Number is required'),
      aadharNumber: Yup
        .string()
        .matches(/^\d{12}$/,"Aadhar number must be exactly 12 digits")
        .required('Aadhar Number is required'),
      gymName: Yup
        .string()
        .max(255)
        .required('Gym Name is required'),
      city: Yup
        .string()
        .max(255)
        .required('Number is required'),
      street: Yup
        .string()
        .max(255)
        .required('Number is required'),
      pinCode: Yup
        .string()
        .max(255)
        .required('Number is required'),
      insta: Yup
        .string()
        .max(255)
    }),
    onSubmit: async (values, helpers) => {
  
    try {
      await signUp(values.email, values.password, values.reEnterPassword, values.fullName, values.phoneNumber, values.aadharNumber, values.gymName, values.city, values.street, values.pinCode, values.insta);

    } catch (err) {
      helpers.setStatus({ success: false });
      helpers.setSubmitting(false);
    }
  }
  });
 
  return (
    <>
      <Head>
        <title>
          Register
        </title>
      </Head>
      <Box
        sx={{
          flex: '1 1 auto',
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        <Box
          sx={{
            maxWidth: 550,
            px: 3,
            py: '100px',
            width: '100%'
          }}
        >
          <div>
            <Stack
              spacing={1}
              sx={{ mb: 3 }}
            >
              <Typography variant="h4">
                Register
              </Typography>
              <Typography
                color="text.secondary"
                variant="body2"
              >
                Already have an account?
                &nbsp;
                <Link
                  component={NextLink}
                  href="/auth/login"
                  underline="hover"
                  variant="subtitle2"
                >
                  Log in
                </Link>
              </Typography>
            </Stack>
            <form
              noValidate
              onSubmit={formik.handleSubmit}
            >
              <Stack spacing={3}>
              <TextField
                  error={!!(formik.touched.email && formik.errors.email)}
                  fullWidth
                  helperText={formik.touched.email && formik.errors.email}
                  label="Email Address"
                  name="email"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="email"
                  value={formik.values.email}
                />
                <TextField
                  error={!!(formik.touched.password && formik.errors.password)}
                  fullWidth
                  helperText={formik.touched.password && formik.errors.password}
                  label="Password"
                  name="password"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="password"
                  value={formik.values.password}
                />
                <TextField
                  error={!!(formik.touched.reEnterPassword && formik.errors.reEnterPassword)}
                  fullWidth
                  helperText={formik.touched.reEnterPassword && formik.errors.reEnterPassword}
                  label="Re-Enter Password"
                  name="reEnterPassword"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="password"
                  value={formik.values.reEnterPassword}
                />
                <TextField
                  error={!!(formik.touched.fullName && formik.errors.fullName)}
                  fullWidth
                  helperText={formik.touched.fullName && formik.errors.fullName}
                  label="Full Name"
                  name="fullName"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.fullName}
                />
                <TextField
                  error={!!(formik.touched.phoneNumber && formik.errors.phoneNumber)}
                  fullWidth
                  helperText={formik.touched.phoneNumber && formik.errors.phoneNumber}
                  label="Phone Number"
                  name="phoneNumber"
                  type='number'
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.phoneNumber}
                />
                <TextField
                  error={!!(formik.touched.aadharNumber && formik.errors.aadharNumber)}
                  fullWidth
                  helperText={formik.touched.aadharNumber && formik.errors.aadharNumber}
                  label="Aadhar Number"
                  name="aadharNumber"
                  type='number'
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.aadharNumber}
                />
                <TextField
                  error={!!(formik.touched.gymName && formik.errors.gymName)}
                  fullWidth
                  helperText={formik.touched.gymName && formik.errors.gymName}
                  label="Gym Name"
                  name="gymName"
                  type='text'
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.gymName}
                />
                <TextField
                  error={!!(formik.touched.city && formik.errors.city)}
                  fullWidth
                  helperText={formik.touched.city && formik.errors.city}
                  label="City"
                  name="city"
                  type='text'
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.city}
                />
                <TextField
                  error={!!(formik.touched.street && formik.errors.street)}
                  fullWidth
                  helperText={formik.touched.street && formik.errors.street}
                  label="Street"
                  name="street"
                  type='text'
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.street}
                />
                <TextField
                  error={!!(formik.touched.pinCode && formik.errors.pinCode)}
                  fullWidth
                  helperText={formik.touched.pinCode && formik.errors.pinCode}
                  label="Pin Code"
                  name="pinCode"
                  type='number'
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.pinCode}
                />
                <TextField
                  error={!!(formik.touched.insta && formik.errors.insta)}
                  fullWidth
                  helperText={formik.touched.insta && formik.errors.insta}
                  label="Instagram ID"
                  name="insta"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.insta}
                />
              </Stack>
              {formik.errors.submit && (
                <Typography
                  color="error"
                  sx={{ mt: 3 }}
                  variant="body2"
                >
                  {formik.errors.submit}
                </Typography>
              )}
              <Button
                fullWidth
                size="large"
                sx={{ mt: 3 }}
                type="submit"
                variant="contained"
              >
                Register
              </Button>
            </form>
          </div>
        </Box>
      </Box>
      <ToastContainer />
    </>
  );
};

RegisterPage.getLayout = (page) => (
  <AuthLayout>
    {page}
  </AuthLayout>
);

export default RegisterPage;*/

// Import statements remain unchanged
import Head from 'next/head';
import 'react-toastify/dist/ReactToastify.css';
import NextLink from 'next/link';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Box, Button, Link, Stack, TextField, Typography } from '@mui/material';
import { Layout as AuthLayout } from 'src/layouts/auth/layout';
import { ToastContainer, toast } from 'react-toastify';

async function signUp(email, password, reEnterPassword, fullName, phoneNumber, aadharNumber, gymName, city, street, pinCode, insta) {
  const requestBody = {
    email,
    password,
    reenterPassword: reEnterPassword,
    name: fullName,
    number: phoneNumber,
    aadhar: aadharNumber,
    gymName,
    city,
    street,
    pincode: pinCode,
    insta
  };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/owners/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });
    if (response.status === 200) {
      const responseData = await response.json();
      location.href = "/auth/verifyOtp";
      toast(responseData.message, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light',
        type: "success"
      });
    } else {
      const responseData = await response.json();
      toast(responseData.error, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light',
        type: "error"
      });
    }
  } catch (error) {
    console.error('Error during registration:', error);
  }
}

const RegisterPage = () => {
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      reEnterPassword: '',
      fullName: '',
      phoneNumber: '',
      aadharNumber: '',
      gymName: '',
      city: '',
      street: '',
      pinCode: '',
      insta: '',
      submit: null
    },
    validationSchema: Yup.object({
      // Validation schema remains unchanged
      email: Yup
      .string()
      .email('Must be a valid email')
      .max(255)
      .required('Email is required'),
    password: Yup.string()
    .max(255)
    .min(8, 'Password must be at least 8 characters long')
    .matches(
      /^(?=.*\d)[A-Za-z\d@$!%*?&]+$/,
      'Password must contain at least one number'
    )
    .required('Password is required'),
    reEnterPassword: Yup
      .string()
      .max(255)
      .oneOf([Yup.ref('password'), null], 'Passwords must match')
      .required('Re-Enter Password is required'),
    fullName: Yup
      .string()
      .max(255)
      .required('Full Name is required'),
    phoneNumber: Yup
      .string()
      .matches(/^[0-9]{10}$/, 'Phone number must be exactly 10 digits')
      .required('Phone Number is required'),
    aadharNumber: Yup
      .string()
      .matches(/^\d{12}$/,"Aadhar number must be exactly 12 digits")
      .required('Aadhar Number is required'),
    gymName: Yup
      .string()
      .max(255)
      .required('Gym Name is required'),
    city: Yup
      .string()
      .max(255)
      .required('Number is required'),
    street: Yup
      .string()
      .max(255)
      .required('Number is required'),
    pinCode: Yup
      .string()
      .max(255)
      .required('Number is required'),
    insta: Yup
      .string()
      .max(255)
    }),
    onSubmit: async (values, helpers) => {
      try {
        formik.setSubmitting(true); // Set isSubmitting to true during form submission
        await signUp(
          values.email,
          values.password,
          values.reEnterPassword,
          values.fullName,
          values.phoneNumber,
          values.aadharNumber,
          values.gymName,
          values.city,
          values.street,
          values.pinCode,
          values.insta
        );
        formik.setSubmitting(false); // Set isSubmitting to false after form submission
      } catch (err) {
        helpers.setStatus({ success: false });
        helpers.setSubmitting(false);
      }
    }
  });

  return (
    <>
      <Head>
        <title>
          Register
        </title>
      </Head>
      <Box
        sx={{
          flex: '1 1 auto',
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        <Box
          sx={{
            maxWidth: 550,
            px: 3,
            py: '100px',
            width: '100%'
          }}
        >
          <div>
            <Stack
              spacing={1}
              sx={{ mb: 3 }}
            >
              <Typography variant="h4">
                Register
              </Typography>
              <Typography
                color="text.secondary"
                variant="body2"
              >
                Already have an account?
                &nbsp;
                <Link
                  component={NextLink}
                  href="/auth/login"
                  underline="hover"
                  variant="subtitle2"
                >
                  Log in
                </Link>
              </Typography>
            </Stack>
            <form
              noValidate
              onSubmit={formik.handleSubmit}
            >
              <Stack spacing={3}>
              <TextField
              error={!!(formik.touched.email && formik.errors.email)}
              fullWidth
              helperText={formik.touched.email && formik.errors.email}
              label="Email Address"
              name="email"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              type="email"
              value={formik.values.email}
            />
            <TextField
              error={!!(formik.touched.password && formik.errors.password)}
              fullWidth
              helperText={formik.touched.password && formik.errors.password}
              label="Password"
              name="password"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              type="password"
              value={formik.values.password}
            />
            <TextField
              error={!!(formik.touched.reEnterPassword && formik.errors.reEnterPassword)}
              fullWidth
              helperText={formik.touched.reEnterPassword && formik.errors.reEnterPassword}
              label="Re-Enter Password"
              name="reEnterPassword"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              type="password"
              value={formik.values.reEnterPassword}
            />
            <TextField
              error={!!(formik.touched.fullName && formik.errors.fullName)}
              fullWidth
              helperText={formik.touched.fullName && formik.errors.fullName}
              label="Full Name"
              name="fullName"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.fullName}
            />
            <TextField
              error={!!(formik.touched.phoneNumber && formik.errors.phoneNumber)}
              fullWidth
              helperText={formik.touched.phoneNumber && formik.errors.phoneNumber}
              label="Phone Number"
              name="phoneNumber"
              type='number'
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.phoneNumber}
            />
            <TextField
              error={!!(formik.touched.aadharNumber && formik.errors.aadharNumber)}
              fullWidth
              helperText={formik.touched.aadharNumber && formik.errors.aadharNumber}
              label="Aadhar Number"
              name="aadharNumber"
              type='number'
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.aadharNumber}
            />
            <TextField
              error={!!(formik.touched.gymName && formik.errors.gymName)}
              fullWidth
              helperText={formik.touched.gymName && formik.errors.gymName}
              label="Gym Name"
              name="gymName"
              type='text'
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.gymName}
            />
            <TextField
              error={!!(formik.touched.city && formik.errors.city)}
              fullWidth
              helperText={formik.touched.city && formik.errors.city}
              label="City"
              name="city"
              type='text'
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.city}
            />
            <TextField
              error={!!(formik.touched.street && formik.errors.street)}
              fullWidth
              helperText={formik.touched.street && formik.errors.street}
              label="Street"
              name="street"
              type='text'
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.street}
            />
            <TextField
              error={!!(formik.touched.pinCode && formik.errors.pinCode)}
              fullWidth
              helperText={formik.touched.pinCode && formik.errors.pinCode}
              label="Pin Code"
              name="pinCode"
              type='number'
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.pinCode}
            />
            <TextField
              error={!!(formik.touched.insta && formik.errors.insta)}
              fullWidth
              helperText={formik.touched.insta && formik.errors.insta}
              label="Instagram ID"
              name="insta"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.insta}
            />
              </Stack>
              {formik.errors.submit && (
                <Typography
                  color="error"
                  sx={{ mt: 3 }}
                  variant="body2"
                >
                  {formik.errors.submit}
                </Typography>
              )}
              <Button
                fullWidth
                size="large"
                sx={{ mt: 3 }}
                type="submit"
                variant="contained"
                disabled={formik.isSubmitting} // Disable the button when submitting
              >
                Register
              </Button>
            </form>
          </div>
        </Box>
      </Box>
      <ToastContainer />
    </>
  );
};

RegisterPage.getLayout = (page) => (
  <AuthLayout>
    {page}
  </AuthLayout>
);

export default RegisterPage;
