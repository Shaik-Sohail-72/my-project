import Head from 'next/head';
import 'react-toastify/dist/ReactToastify.css';
import NextLink from 'next/link';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Box, Button, Link, Stack, TextField, Typography } from '@mui/material';
import { Layout as AuthLayout } from 'src/layouts/auth/layout';
import { ToastContainer, toast } from 'react-toastify';

async function signUp(email, otp, password, reEnterPassword, setSubmitting) {

  const requestBody = {
    email,
    otp,
    password,
    reenterPassword: reEnterPassword,
  };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/owners/reset-password', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });
    if (response.status === 200) {
      const responseData = await response.json();
      location.href = "/";
      toast(responseData.message, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "success"
      });
    } else {
      const responseData = await response.json();
      toast(responseData.error, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "error"
      });
    }
  } catch (error) {
    console.error('Error during password reset process', error);
  } finally {
    setSubmitting(false); // Enable the button after completing the request
  }
}

const RegisterPage = () => {
  const formik = useFormik({
    initialValues: {
      email: '',
      otp: '',
      password: '',
      reEnterPassword: '',
      submit: null
    },
    validationSchema: Yup.object({
      email: Yup
        .string()
        .email('Must be a valid email')
        .max(255)
        .required('Email is required'),
      otp: Yup.string()
        .matches(/^[0-9]{6}$/, 'OTP must be exactly 6 digits')
        .required('OTP is required'),
      password: Yup.string()
        .max(255)
        .min(8, 'Password must be at least 8 characters long')
        .matches(
          /^(?=.*\d)[A-Za-z\d@$!%*?&]+$/,
          'Password must contain at least one number'
        )
        .required('Password is required'),
      reEnterPassword: Yup
        .string()
        .max(255)
        .oneOf([Yup.ref('password'), null], 'Passwords must match')
        .required('Re-Enter Password is required'),

    }),
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true); // Disable the button during submission
      try {
        await signUp(values.email, values.otp, values.password, values.reEnterPassword, setSubmitting);
      } catch (err) {
        // Handle error if needed
      }
    }
  });

  return (
    <>
      <Head>
        <title>
          Reset Password
        </title>
      </Head>
      <Box
        sx={{
          flex: '1 1 auto',
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        <Box
          sx={{
            maxWidth: 550,
            px: 3,
            py: '100px',
            width: '100%'
          }}
        >
          <div>
            <Stack
              spacing={1}
              sx={{ mb: 3 }}
            >
              <Typography variant="h4">
                Reset Password
              </Typography>
              <Typography
                color="text.secondary"
                variant="body2"
              >
                Already have an account?
                &nbsp;
                <Link
                  component={NextLink}
                  href="/auth/login"
                  underline="hover"
                  variant="subtitle2"
                >
                  Log in
                </Link>
              </Typography>
            </Stack>
            <form
              noValidate
              onSubmit={formik.handleSubmit}
            >
              <Stack spacing={3}>
                <TextField
                  error={!!(formik.touched.email && formik.errors.email)}
                  fullWidth
                  helperText={formik.touched.email && formik.errors.email}
                  label="Email Address"
                  name="email"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="email"
                  value={formik.values.email}
                />
                <TextField
                  error={!!(formik.touched.otp && formik.errors.otp)}
                  fullWidth
                  helperText={formik.touched.otp && formik.errors.otp}
                  label="OTP"
                  name="otp"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.otp}
                />
                <TextField
                  error={!!(formik.touched.password && formik.errors.password)}
                  fullWidth
                  helperText={formik.touched.password && formik.errors.password}
                  label="Password"
                  name="password"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="password"
                  value={formik.values.password}
                />
                <TextField
                  error={!!(formik.touched.reEnterPassword && formik.errors.reEnterPassword)}
                  fullWidth
                  helperText={formik.touched.reEnterPassword && formik.errors.reEnterPassword}
                  label="Re-Enter Password"
                  name="reEnterPassword"
                  type='password'
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.reEnterPassword}
                />
              </Stack>
              {formik.errors.submit && (
                <Typography
                  color="error"
                  sx={{ mt: 3 }}
                  variant="body2"
                >
                  {formik.errors.submit}
                </Typography>
              )}
              <Button
                disabled={formik.isSubmitting}
                fullWidth
                size="large"
                sx={{ mt: 3 }}
                type="submit"
                variant="contained"
              >
                Reset Password
              </Button>
            </form>
          </div>
        </Box>
      </Box>
      <ToastContainer />
    </>
  );
};

RegisterPage.getLayout = (page) => (
  <AuthLayout>
    {page}
  </AuthLayout>
);

export default RegisterPage;
