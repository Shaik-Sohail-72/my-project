import Head from 'next/head';
import 'react-toastify/dist/ReactToastify.css';
import NextLink from 'next/link';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Box, Button, Link, Stack, TextField, Typography } from '@mui/material';
import { Layout as AuthLayout } from 'src/layouts/auth/layout';
import { ToastContainer, toast } from 'react-toastify';

async function signUp(aadharNumber, email, setSubmitting) {
  const requestBody = {
    aadhar: aadharNumber,
    email
  };

  try {
    const response = await fetch('https://fithubbackend.onrender.com/owners/forgot-password', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });

    if (response.status === 200) {
      const responseData = await response.json();
      location.href = "/auth/resetPassword";
      toast(responseData.message, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "success"
      });
    } else {
      const responseData = await response.json();
      toast(responseData.error, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        newestOnTop: false,
        closeOnClick: true,
        rtl: false,
        pauseOnFocusLoss: true,
        draggable: true,
        pauseOnHover: true,
        theme: 'light', type: "error"
      });
    }
  } catch (error) {
    console.error('Error during password reset process', error);
  } finally {
    setSubmitting(false); // Enable the button after completing the request
  }
}

const RegisterPage = () => {
  const formik = useFormik({
    initialValues: {
      aadharNumber: '',
      email: '',
      submit: null
    },
    validationSchema: Yup.object({
      aadharNumber: Yup
        .string()
        .matches(/^\d{12}$/, "Aadhar number must be exactly 12 digits")
        .required('Aadhar Number is required'),
      email: Yup
        .string()
        .email('Must be a valid email')
        .max(255)
        .required('Email is required'),
    }),
    onSubmit: async (values, { setSubmitting }) => {
      setSubmitting(true); // Disable the button during submission
      try {
        await signUp(values.aadharNumber, values.email, setSubmitting);
      } catch (err) {
        // Handle error if needed
      }
    }
  });

  return (
    <>
      <Head>
        <title>
          Forgot Password
        </title>
      </Head>
      <Box
        sx={{
          flex: '1 1 auto',
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        <Box
          sx={{
            maxWidth: 550,
            px: 3,
            py: '100px',
            width: '100%'
          }}
        >
          <div>
            <Stack
              spacing={1}
              sx={{ mb: 3 }}
            >
              <Typography variant="h4">
                Forgot Password
              </Typography>
              <Typography
                color="text.secondary"
                variant="body2"
              >
                Already have an account?
                &nbsp;
                <Link
                  component={NextLink}
                  href="/auth/login"
                  underline="hover"
                  variant="subtitle2"
                >
                  Log in
                </Link>
              </Typography>
            </Stack>
            <form
              noValidate
              onSubmit={formik.handleSubmit}
            >
              <Stack spacing={3}>
                <TextField
                  error={!!(formik.touched.aadharNumber && formik.errors.aadharNumber)}
                  fullWidth
                  helperText={formik.touched.aadharNumber && formik.errors.aadharNumber}
                  label="Aadhar Number"
                  name="aadharNumber"
                  type='number'
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.aadharNumber}
                />
                <TextField
                  error={!!(formik.touched.email && formik.errors.email)}
                  fullWidth
                  helperText={formik.touched.email && formik.errors.email}
                  label="Email Address"
                  name="email"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="email"
                  value={formik.values.email}
                />
              </Stack>
              {formik.errors.submit && (
                <Typography
                  color="error"
                  sx={{ mt: 3 }}
                  variant="body2"
                >
                  {formik.errors.submit}
                </Typography>
              )}
              <Button
                disabled={formik.isSubmitting}
                fullWidth
                size="large"
                sx={{ mt: 3 }}
                type="submit"
                variant="contained"
              >
                Forgot Password
              </Button>
            </form>
          </div>
        </Box>
      </Box>
      <ToastContainer />
    </>
  );
};

RegisterPage.getLayout = (page) => (
  <AuthLayout>
    {page}
  </AuthLayout>
);

export default RegisterPage;
