/*import Head from 'next/head';
import 'react-toastify/dist/ReactToastify.css';
import NextLink from 'next/link';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Box, Button, Link, Stack, TextField, Typography } from '@mui/material';
import { Layout as AuthLayout } from 'src/layouts/auth/layout';
import { ToastContainer, toast } from 'react-toastify';
async function signUp(email, otp) {
  
  const requestBody = {
    email,
    otp
  };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/owners/verify', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });
    if (response.status === 200){
      const responseData = await response.json(); 
      location.href="/auth/login";
      toast(responseData.message , {position: 'top-right',
      autoClose:5000,
      hideProgressBar:false,
      newestOnTop:false,
      closeOnClick: true,
      rtl:false,
      pauseOnFocusLoss: true,
      draggable: true,
      pauseOnHover: true,
      theme:'light', type:"success"});
    }else{
      const responseData = await response.json(); 
      toast(responseData.error , {position: 'top-right',
      autoClose:5000,
      hideProgressBar:false,
      newestOnTop:false,
      closeOnClick: true,
      rtl:false,
      pauseOnFocusLoss: true,
      draggable: true,
      pauseOnHover: true,
      theme:'light', type:"error"});
    }
  } catch (error) {
      console.error('Error during verification:', error);
  }
}

const OTPVerificationPage = () => {
  const formik = useFormik({
    initialValues: {
      email: '',
      otp: '',
      submit: null
    },
    validationSchema: Yup.object({
      email: Yup
        .string()
        .email('Must be a valid email')
        .max(255)
        .required('Email is required'),
      otp: Yup.string()
      .matches(/^[0-9]{6}$/, 'OTP must be exactly 6 digits')
      .required('OTP is required'),
    
    }),
    onSubmit: async (values, helpers) => {
  
    try {
      await signUp(values.email, values.otp);

    } catch (err) {
      helpers.setStatus({ success: false });
      helpers.setSubmitting(false);
    }
  }
  });
 
  return (
    <>
      <Head>
        <title>
          OTP Verification
        </title>
      </Head>
      <Box
        sx={{
          flex: '1 1 auto',
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        <Box
          sx={{
            maxWidth: 550,
            px: 3,
            py: '100px',
            width: '100%'
          }}
        >
          <div>
            <Stack
              spacing={1}
              sx={{ mb: 3 }}
            >
              <Typography variant="h4">
                OTP Verification
              </Typography>
              <Typography
                color="text.secondary"
                variant="body2"
              >
                Already have an account?
                &nbsp;
                <Link
                  component={NextLink}
                  href="/auth/login"
                  underline="hover"
                  variant="subtitle2"
                >
                  Log in
                </Link>
              </Typography>
            </Stack>
            <form
              noValidate
              onSubmit={formik.handleSubmit}
            >
              <Stack spacing={3}>
              <TextField
                  error={!!(formik.touched.email && formik.errors.email)}
                  fullWidth
                  helperText={formik.touched.email && formik.errors.email}
                  label="Email Address"
                  name="email"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="email"
                  value={formik.values.email}
                />
                <TextField
                  error={!!(formik.touched.otp && formik.errors.otp)}
                  fullWidth
                  helperText={formik.touched.otp && formik.errors.otp}
                  label="OTP"
                  name="otp"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.otp}
                />
              </Stack>
              {formik.errors.submit && (
                <Typography
                  color="error"
                  sx={{ mt: 3 }}
                  variant="body2"
                >
                  {formik.errors.submit}
                </Typography>
              )}
              <Button
                fullWidth
                size="large"
                sx={{ mt: 3 }}
                type="submit"
                variant="contained"
              >
                Continue
              </Button>
            </form>
          </div>
        </Box>
      </Box>
      <ToastContainer />
    </>
  );
};

OTPVerificationPage.getLayout = (page) => (
  <AuthLayout>
    {page}
  </AuthLayout>
);

export default OTPVerificationPage;
*/


import Head from 'next/head';
import 'react-toastify/dist/ReactToastify.css';
import NextLink from 'next/link';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  Box,
  Button,
  Link,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import { Layout as AuthLayout } from 'src/layouts/auth/layout';
import { ToastContainer, toast } from 'react-toastify';

const toastConfig = {
  position: 'top-right',
  autoClose: 5000,
  hideProgressBar: false,
  newestOnTop: false,
  closeOnClick: true,
  rtl: false,
  pauseOnFocusLoss: true,
  draggable: true,
  pauseOnHover: true,
  theme: 'light',
};

async function signUp(email, otp, setButtonDisabled) {
  const requestBody = { email, otp };
  try {
    const response = await fetch('https://fithubbackend.onrender.com/owners/verify', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    });

    const responseData = await response.json();

    if (response.status === 200) {
      location.href = '/auth/login';
      toast(responseData.message, { ...toastConfig, type: 'success' });
    } else {
      toast(responseData.error, { ...toastConfig, type: 'error' });
    }
  } catch (error) {
    console.error('Error during verification:', error);
  } finally {
    setButtonDisabled(false);
  }
}

const FormField = ({
  name,
  label,
  type = 'text',
  formik,
}) => (
  <TextField
    error={!!(formik.touched[name] && formik.errors[name])}
    fullWidth
    helperText={formik.touched[name] && formik.errors[name]}
    label={label}
    name={name}
    onBlur={formik.handleBlur}
    onChange={formik.handleChange}
    type={type}
    value={formik.values[name]}
  />
);

const OTPVerificationPage = () => {
  const formik = useFormik({
    initialValues: {
      email: '',
      otp: '',
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Must be a valid email')
        .max(255)
        .required('Email is required'),
      otp: Yup.string()
        .matches(/^[0-9]{6}$/, 'OTP must be exactly 6 digits')
        .required('OTP is required'),
    }),
    onSubmit: async (values, helpers) => {
      helpers.setSubmitting(true);

      try {
        await signUp(values.email, values.otp, helpers.setSubmitting);
      } catch (err) {
        helpers.setStatus({ success: false });
        helpers.setSubmitting(false);
      }
    },
  });

  return (
    <>
      <Head>
        <title>OTP Verification</title>
      </Head>
      <Box
        sx={{
          flex: '1 1 auto',
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Box
          sx={{
            maxWidth: 550,
            px: 3,
            py: '100px',
            width: '100%',
          }}
        >
          <div>
            <Stack spacing={1}
sx={{ mb: 3 }}>
              <Typography variant="h4">OTP Verification</Typography>
              <Typography color="text.secondary"
variant="body2">
                Already have an account?{' '}
                <Link
                  component={NextLink}
                  href="/auth/login"
                  underline="hover"
                  variant="subtitle2"
                >
                  Log in
                </Link>
              </Typography>
            </Stack>
            <form noValidate
onSubmit={formik.handleSubmit}>
              <Stack spacing={3}>
                <FormField name="email"
label="Email Address"
formik={formik} />
                <FormField name="otp"
label="OTP"
formik={formik} />
              </Stack>
              {formik.errors.submit && (
                <Typography color="error"
sx={{ mt: 3 }}
variant="body2">
                  {formik.errors.submit}
                </Typography>
              )}
              <Button
                fullWidth
                size="large"
                sx={{ mt: 3 }}
                type="submit"
                variant="contained"
                disabled={formik.isSubmitting}
              >
                Continue
              </Button>
            </form>
          </div>
        </Box>
      </Box>
      <ToastContainer />
    </>
  );
};

OTPVerificationPage.getLayout = (page) => <AuthLayout>{page}</AuthLayout>;

export default OTPVerificationPage;
