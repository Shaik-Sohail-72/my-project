// Import statements...
import Head from 'next/head';
import axios from 'axios';
import { Hourglass } from 'react-loader-spinner'; // Import the Hourglass component
import React, { useCallback, useMemo, useState, useEffect } from 'react';
import { subDays, subHours } from 'date-fns';
import { Box, Container, Unstable_Grid2 as Grid } from '@mui/material';
import { Layout as DashboardLayout } from 'src/layouts/dashboard/layout';
import { OverviewBudget } from 'src/sections/overview/overview-budget';
import { OverviewLatestOrders } from 'src/sections/overview/overview-latest-orders';
import { OverviewLatestProducts, OverviewRecentMemberships } from 'src/sections/overview/overview-recent-memberships';
import { OverviewSales } from 'src/sections/overview/overview-sales';
import { OverviewTasksProgress } from 'src/sections/overview/overview-tasks-progress';
import { OverviewTotalCustomers } from 'src/sections/overview/overview-total-customers';
import { OverviewTotalProfit } from 'src/sections/overview/overview-total-profit';
import { OverviewTraffic } from 'src/sections/overview/overview-traffic';
import { OverviewRevenue } from 'src/sections/overview/overview-revenue';
import { OverviewTotalMembers } from 'src/sections/overview/overview-total-members';
import { OverviewPendingMembers } from 'src/sections/overview/overview-pending-members';
import { OverviewNewMembersmonths } from 'src/sections/overview/overview-new-members-months';
import { OverviewAge } from 'src/sections/overview/Overview-age';
import { OverviewGender } from 'src/sections/overview/overview-gender';
import { OverviewPreferredSlots } from 'src/sections/overview/Overview-preferred-slots';
import { OverviewPlanChoice } from 'src/sections/overview/Overview-plan-choice';
import { OverviewActivePending } from 'src/sections/overview/overview-Active-Pending';
import { OverviewServices } from 'src/sections/overview/overview-services';
import { OverviewExpenditure } from 'src/sections/overview/overview-expenditure';

const now = new Date();
const currentYear = now.getFullYear();
const prevYear = currentYear - 1;

const Page = () => {
  const [loading, setLoading] = useState(true);


  const [revenueData, setRevenueData] = useState(null);
  const [totalMembers, setTotalMembers] = useState(null);
  const [pendingMembers, setPendingMembers] = useState(null);
  const [expenditurePercentageCurrYear, setExpenditurePercentageCurrYear] = useState(null);
  const [profitCurrYear, setProfitCurrYear] = useState(0);
  const [janCurrYearGrowth, setJanCurrYearGrowth] = useState(0);
  const [febCurrYearGrowth, setFebCurrYearGrowth] = useState(0);
  const [marCurrYearGrowth, setMarCurrYearGrowth] = useState(0);
  const [aprilCurrYearGrowth, setAprilCurrYearGrowth] = useState(0);
  const [mayCurrYearGrowth, setMayCurrYearGrowth] = useState(0);
  const [juneCurrYearGrowth, setJuneCurrYearGrowth] = useState(0);
  const [julyCurrYearGrowth, setJulyCurrYearGrowth] = useState(0);
  const [augCurrYearGrowth, setAugCurrYearGrowth] = useState(0);
  const [septCurrYearGrowth, setSeptCurrYearGrowth] = useState(0);
  const [octCurrYearGrowth, setOctCurrYearGrowth] = useState(0);
  const [novCurrYearGrowth, setNovCurrYearGrowth] = useState(0);
  const [decCurrYearGrowth, setDecCurrYearGrowth] = useState(0);

  const [janPrevYearGrowth, setJanPrevYearGrowth] = useState(0);
  const [febPrevYearGrowth, setFebPrevYearGrowth] = useState(0);
  const [marPrevYearGrowth, setMarPrevYearGrowth] = useState(0);
  const [aprilPrevYearGrowth, setAprilPrevYearGrowth] = useState(0);
  const [mayPrevYearGrowth, setMayPrevYearGrowth] = useState(0);
  const [junePrevYearGrowth, setJunePrevYearGrowth] = useState(0);
  const [julyPrevYearGrowth, setJulyPrevYearGrowth] = useState(0);
  const [augPrevYearGrowth, setAugPrevYearGrowth] = useState(0);
  const [septPrevYearGrowth, setSeptPrevYearGrowth] = useState(0);
  const [octPrevYearGrowth, setOctPrevYearGrowth] = useState(0);
  const [novPrevYearGrowth, setNovPrevYearGrowth] = useState(0);
  const [decPrevYearGrowth, setDecPrevYearGrowth] = useState(0);

  const [ageTen, setAgeTen] = useState(0);
  const [ageEighteen, setAgeEighteen] = useState(0);
  const [ageTwentyfour, setAgeTwentyfour] = useState(0);

  const [morningCurrYear, setMorningCurrYear ] = useState(0);
  const [afternoonCurrYear, setAfternoonCurrYear] = useState(0);
  const [eveningCurrYear, setEveningCurrYear] = useState(0);  
  const [nightCurrYear, setNightCurrYear] = useState(0);

  const [morningPrevYear, setMorningPrevYear ] = useState(0);
  const [afternoonPrevYear, setAfternoonPrevYear] = useState(0);
  const [eveningPrevYear, setEveningPrevYear] = useState(0);  
  const [nightPrevYear, setNightPrevYear] = useState(0);


  const [male , setMale] = useState(0);
  const [female, setFemale] = useState(0);

  const [basicCurrYear, setBasicCurrYear] = useState(0);
  const [silverCurrYear, setSilverCurrYear]=useState(0);
  const [goldCurrYear, setGoldCurrYear] = useState(0);
  const [platinumCurrYear, setPlatinumCurrYear] = useState(0);
  const [basicPrevYear, setBasicPrevYear] = useState(0);
  const [silverPrevYear, setSilverPrevYear]=useState(0);
  const [goldPrevYear, setGoldPrevYear] = useState(0);
  const [platinumPrevYear, setPlatinumPrevYear] = useState(0);

  const [active, setActive] = useState(0);
  const [pending, setPending] = useState(0);

  const [ onlyGym, setOnlyGym] = useState(0);
  const [cardio, setCardio] = useState(0);
  const [groupClasses, setGroupClasses] = useState(0);
  const [personalTraning, setPersonalTraning] = useState(0);
  const [zumba, setZumba] = useState(0);
  const [yoga, setYoga] = useState(0);
  const [indoor, setIndoor] = useState(0);
  const [crossfit, setCrossfit] = useState(0);

  const [u1name, setu1name] = useState();
  const [u1avatar, setu1avatar] = useState();
  const [u1date, setu1date] = useState();

  const [u2name, setu2name] = useState();
  const [u2avatar, setu2avatar] = useState();
  const [u2date, setu2date] = useState();

  const [u3name, setu3name] = useState();
  const [u3avatar, setu3avatar] = useState();
  const [u3date, setu3date] = useState();

  const [u4name, setu4name] = useState();
  const [u4avatar, setu4avatar] = useState();
  const [u4date, setu4date] = useState();

  const [u5name, setu5name] = useState();
  const [u5avatar, setu5avatar] = useState();
  const [u5date, setu5date] = useState();

  const fetchRevenueData = async () => {
    try {
      const charCode = sessionStorage.getItem("charCode");
      const response = await axios.get(`https://fithubbackend.onrender.com/overview?charCode=${charCode}`);
      const responseData = response.data;
      setLoading(false);

      setRevenueData(responseData.totalRevenueCurrYearVal);
      setTotalMembers(responseData.count);
      setPendingMembers(responseData.pendingCount);
      setExpenditurePercentageCurrYear(responseData.expenditurePercentageCurrYearVal);
      setProfitCurrYear(responseData.totalProfitCurrYear);
      setJanCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`January-${currentYear}`] || 0.5);
      setFebCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`February-${currentYear}`] || 0.5);
      setMarCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`March-${currentYear}`] || 0.5);
      setAprilCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`April-${currentYear}`] || 0.5);
      setMayCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`May-${currentYear}`] || 0.5);
      setJuneCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`June-${currentYear}`] || 0.5);
      setJulyCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`July-${currentYear}`] || 0.5);
      setAugCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`August-${currentYear}`] || 0.5);
      setSeptCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`September-${currentYear}`] || 0.5);
      setOctCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`October-${currentYear}`] || 0.5);
      setNovCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`November-${currentYear}`] || 0.5);
      setDecCurrYearGrowth(responseData.monthYearCountMapCurrentYear[`December-${currentYear}`] || 0.5);

      setJanPrevYearGrowth(responseData.monthYearCountMapLastYear[`January-${prevYear}`] || 0.5);
      setFebPrevYearGrowth(responseData.monthYearCountMapLastYear[`February-${prevYear}`] || 0.5);
      setMarPrevYearGrowth(responseData.monthYearCountMapLastYear[`March-${prevYear}`] || 0.5);
      setAprilPrevYearGrowth(responseData.monthYearCountMapLastYear[`April-${prevYear}`] || 0.5);
      setMayPrevYearGrowth(responseData.monthYearCountMapLastYear[`May-${prevYear}`] || 0.5);
      setJunePrevYearGrowth(responseData.monthYearCountMapLastYear[`June-${prevYear}`] || 0.5);
      setJulyPrevYearGrowth(responseData.monthYearCountMapLastYear[`July-${prevYear}`] || 0.5);
      setAugPrevYearGrowth(responseData.monthYearCountMapLastYear[`August-${prevYear}`] || 0.5);
      setSeptPrevYearGrowth(responseData.monthYearCountMapLastYear[`September-${prevYear}`] || 0.5);
      setOctPrevYearGrowth(responseData.monthYearCountMapLastYear[`October-${prevYear}`] || 0.5);
      setNovPrevYearGrowth(responseData.monthYearCountMapLastYear[`November-${prevYear}`] || 0.5);
      setDecPrevYearGrowth(responseData.monthYearCountMapLastYear[`December-${prevYear}`] || 0.5);

      setAgeTen(responseData.ageGroupPercentages["10-18"] || 0.5);
      setAgeEighteen(responseData.ageGroupPercentages["18-24"] || 0.5);
      setAgeTwentyfour(responseData.ageGroupPercentages[">24"] || 0.5);

      setMale(responseData.genderCountMap.Male || 0.5);
      setFemale(responseData.genderCountMap.Female || 0.5)

      setMorningCurrYear(responseData.preferredSlotCountMapCurrentYear[`Morning`] || 0.5);
      setAfternoonCurrYear(responseData.preferredSlotCountMapCurrentYear[`Afternoon`] || 0.5);
      setEveningCurrYear(responseData.preferredSlotCountMapCurrentYear[`Evening`] || 0.5);
      setNightCurrYear(responseData.preferredSlotCountMapCurrentYear[`Night`] || 0.5);

      setMorningPrevYear(responseData.preferredSlotCountMapLastYear[`Morning`] || 0.5);
      setAfternoonPrevYear(responseData.preferredSlotCountMapLastYear[`Afternoon`] || 0.5);
      setEveningPrevYear(responseData.preferredSlotCountMapLastYear[`Evening`] || 0.5);
      setNightPrevYear(responseData.preferredSlotCountMapLastYear[`Night`] || 0.5);

      setBasicCurrYear(responseData.planChoiceCountMapCurrentYear['Basic'] || 0.5);
      setSilverCurrYear(responseData.planChoiceCountMapCurrentYear['Silver'] || 0.5);
      setGoldCurrYear(responseData.planChoiceCountMapCurrentYear['Gold'] || 0.5);
      setPlatinumCurrYear(responseData.planChoiceCountMapCurrentYear['Platinum'] || 0.5);

      setBasicPrevYear(responseData.planChoiceCountMapLastYear['Basic'] || 0.5);
      setSilverPrevYear(responseData.planChoiceCountMapLastYear['Silver'] || 0.5);
      setGoldPrevYear(responseData.planChoiceCountMapLastYear['Gold'] || 0.5);
      setPlatinumPrevYear(responseData.planChoiceCountMapLastYear['Platinum'] || 0.5);

      setActive(responseData.activeP || 0.5);
      setPending(responseData.pendingP || 0.5);

      setOnlyGym(responseData.serviceCounts['OnlyGym'] || 0.5);
      setCardio(responseData.serviceCounts['Cardio'] || 0.5);
      setGroupClasses(responseData.serviceCounts['GroupClasses'] || 0.5)
      setPersonalTraning(responseData.serviceCounts['PersonalTraning'] || 0.5)
      setZumba(responseData.serviceCounts['Zumba'] || 0.5)
      setYoga(responseData.serviceCounts['Yoga'] || 0.5)
      setIndoor(responseData.serviceCounts['IndoorCycling'] || 0.5)
      setCrossfit(responseData.serviceCounts['CrossFit'] || 0.5)

      setu1name(responseData.recentMembers[0].name );
      setu1avatar(responseData.recentMembers[0].avatar );
      setu1date(responseData.recentMembers[0].timeDifference)

      setu2name(responseData.recentMembers[1].name );
      setu2avatar(responseData.recentMembers[1].avatar );
      setu2date(responseData.recentMembers[1].timeDifference)

      setu3name(responseData.recentMembers[2].name );
      setu3avatar(responseData.recentMembers[2].avatar );
      setu3date(responseData.recentMembers[2].timeDifference)

      setu4name(responseData.recentMembers[3].name );
      setu4avatar(responseData.recentMembers[3].avatar );
      setu4date(responseData.recentMembers[3].timeDifference)

      setu5name(responseData.recentMembers[4].name );
      setu5avatar(responseData.recentMembers[4].avatar );
      setu5date(responseData.recentMembers[4].timeDifference)


      

    } catch (error) {
      console.error('Error during revenue data fetch', error);
    }
  };

  useEffect(() => {
    fetchRevenueData();
  }, []);

  function formatNumberWithCommas(number) {
    if (number !== null) {
      return number.toLocaleString('en-IN'); // 'en-IN' represents the Indian English locale
    }
    // Handle the case when number is null, return an appropriate value or an empty string
    return '';
  }

  return (
    <>
      <Head>
        <title>
          Overview
        </title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center', // Center the loading spinner
        }}
      >
        {loading ? (
          <Hourglass
            visible={true}
            height="80"
            width="80"
            ariaLabel="hourglass-loading"
            wrapperStyle={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
            wrapperClass=""
            colors={['#306cce', '#72a1ed']}
          />
        ) : (
        <Container maxWidth="xl">
          <Grid
            container
            spacing={3}
          >
            <Grid
              xs={12}
              sm={6}
              lg={3}
            >
              <OverviewRevenue
                difference={12}
                positive
                sx={{ height: '100%' }}
                value={`₹${formatNumberWithCommas(revenueData)}`}
              />
            </Grid>
            <Grid
              xs={12}
              sm={6}
              lg={3}
            >
              <OverviewTotalMembers
                difference={`${formatNumberWithCommas(pendingMembers)}`}
                positive={false}
                sx={{ height: '100%' }}
                value={`${formatNumberWithCommas(totalMembers)}`}
              />
            </Grid>
            <Grid
              xs={12}
              sm={6}
              lg={3}
            >
              <OverviewExpenditure
                sx={{ height: '100%' }}
                value={expenditurePercentageCurrYear}
              />
            </Grid>
            <Grid
              xs={12}
              sm={6}
              lg={3}
            >
              <OverviewTotalProfit
                sx={{ height: '100%' }}
                value={`₹${formatNumberWithCommas(profitCurrYear)}`}
              />
            </Grid>
            <Grid
              xs={12}
              lg={8}
            >
              <OverviewNewMembersmonths
                chartSeries={[
                  {
                    name: 'This year',
                    data: [janCurrYearGrowth, febCurrYearGrowth, marCurrYearGrowth, aprilCurrYearGrowth,mayCurrYearGrowth, juneCurrYearGrowth, julyCurrYearGrowth, augCurrYearGrowth, septCurrYearGrowth, octCurrYearGrowth, novCurrYearGrowth, decCurrYearGrowth]
                  },
                  {
                    name: 'Last year',
                    data: [janPrevYearGrowth, febPrevYearGrowth, marPrevYearGrowth, aprilPrevYearGrowth,mayPrevYearGrowth, junePrevYearGrowth, julyPrevYearGrowth, augPrevYearGrowth,septPrevYearGrowth, octPrevYearGrowth, novPrevYearGrowth, decPrevYearGrowth]
                  }
                ]}
                sx={{ height: '100%' }}
              />
            </Grid>


            <Grid
            xs={12}
            md={6}
            lg={4}
          >
            <OverviewAge
              chartSeries={[ageTen,ageEighteen,ageTwentyfour]}
              labels={['10 - 18', '18 - 24', '> 24' ]}
              sx={{ height: '100%' }}
            />
          </Grid>


          <Grid
            xs={12}
            md={6}
            lg={4}
          >
            <OverviewGender
              chartSeries={[male,female]}
              labels={['Male', 'Female']}
              sx={{ height: '100%' }}
            />
          </Grid>

          <Grid
          xs={12}
          lg={8}
        >
          <OverviewPreferredSlots
            chartSeries={[
              {
                name: 'This year',
                data: [ morningCurrYear, afternoonCurrYear, eveningCurrYear, nightCurrYear]
              },
              {
                name: 'Last year',
                data: [morningPrevYear, afternoonPrevYear, eveningPrevYear, nightPrevYear]
              }
            ]}
            sx={{ height: '100%' }}
          />
        </Grid>


        <Grid
          xs={12}
          lg={8}
        >
          <OverviewPlanChoice
            chartSeries={[
              {
                name: 'This year',
                data: [ basicCurrYear, silverCurrYear, goldCurrYear, platinumCurrYear]
              },
              {
                name: 'Last year',
                data: [ basicPrevYear, silverPrevYear, goldPrevYear, platinumPrevYear]
              }
            ]}
            sx={{ height: '100%' }}
          />
        </Grid>

        <Grid
            xs={12}
            md={6}
            lg={4}
          >
            <OverviewActivePending
              chartSeries={[active, pending]}
              labels={['Active', 'Pending']}
              sx={{ height: '100%' }}
            />
          </Grid>

          <Grid
          xs={12}
          lg={8}
        >
          <OverviewServices
            chartSeries={[
              {
                name: 'This year',
                data: [onlyGym,cardio, groupClasses, personalTraning, zumba, yoga, indoor, crossfit]
              },

            ]}
            sx={{ height: '100%' }}
          />
        </Grid>



        <Grid
            xs={12}
            md={6}
            lg={4}
          >
            <OverviewRecentMemberships
              products={[
                {
                  image: `${u1avatar}`,
                  name: `${u1name}`,
                  updatedAt: `${u1date}`
                },
                {
                  image: `${u2avatar}`,
                  name: `${u2name}`,
                  updatedAt: `${u2date}`
                },
                {
                  image: `${u3avatar}`,
                  name: `${u3name}`,
                  updatedAt: `${u3date}`
                },
                {
                  image: `${u4avatar}`,
                  name: `${u4name}`,
                  updatedAt: `${u4date}`
                },
                {
                  image: `${u5avatar}`,
                  name: `${u5name}`,
                  updatedAt: `${u5date}`
                },
              ]}
              sx={{ height: '100%' }}
            />
          </Grid>


          </Grid>
        </Container>
        )}
      </Box>
    </>
  );
};

Page.getLayout = (page) => (
  <DashboardLayout>
    {page}
  </DashboardLayout>
);

export default Page;
