import ChartBarIcon from '@heroicons/react/24/solid/ChartBarIcon';
import CogIcon from '@heroicons/react/24/solid/CogIcon';
import LockClosedIcon from '@heroicons/react/24/solid/LockClosedIcon';
import ShoppingBagIcon from '@heroicons/react/24/solid/ShoppingBagIcon';
import UserIcon from '@heroicons/react/24/solid/UserIcon';
import UserPlusIcon from '@heroicons/react/24/solid/UserPlusIcon';
import UsersIcon from '@heroicons/react/24/solid/UsersIcon';
import CurrencyExchangeIcon from '@mui/icons-material/CurrencyExchange';

import XCircleIcon from '@heroicons/react/24/solid/XCircleIcon';
import { SvgIcon } from '@mui/material';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import AutoGraphIcon from '@mui/icons-material/AutoGraph';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import LocalAtmIcon from '@mui/icons-material/LocalAtm';
import EventNoteIcon from '@mui/icons-material/EventNote';
import GroupRemoveIcon from '@mui/icons-material/GroupRemove';
import NoFoodIcon from '@mui/icons-material/NoFood';
import SupportAgentIcon from '@mui/icons-material/SupportAgent';
import HistoryIcon from '@mui/icons-material/History';
import GetAppIcon from '@mui/icons-material/GetApp';

export const items = [
  {
    title: 'Overview',
    path: '/',
    icon: (
      <SvgIcon fontSize="small">
        <AutoGraphIcon />
      </SvgIcon>
    )
  },
  {
    title: 'Members',
    path: '/members',
    icon: (
      <SvgIcon fontSize="small">
        <GroupAddIcon/>
      </SvgIcon>
    )
  },
  {
    title: 'Members Profile',
    path: '/membersProfile',
    icon: (
      <SvgIcon fontSize="small">
        <AccountBoxIcon />
      </SvgIcon>
    )
  },
  {
    title: 'Pending Fees',
    path: '/pendingMembers',
    icon: (
      <SvgIcon fontSize="small">
        <PeopleAltIcon />
      </SvgIcon>
    )
  },
  {
    
    title: 'Financial Strategies',
    path: '/financialStrategies',
    icon: (
      <SvgIcon fontSize="small">
        <LocalAtmIcon />
      </SvgIcon>
    )
  },
  {
    title: 'Event Center',
    path: '/eventCenter',
    icon: (
      <SvgIcon fontSize="small">
        <EventNoteIcon/>
      </SvgIcon>
    )
  },
  {
    title: 'Deactivated Members',
    path: '/deactivatedMembers',
    icon: (
      <SvgIcon fontSize="small">
        <GroupRemoveIcon/>
      </SvgIcon>
    )
  },
  {
    title: 'History',
    path: '/history',
    icon: (
      <SvgIcon fontSize="small">
        <HistoryIcon />
      </SvgIcon>
    )
  },
  {
    title: 'Expenditures',
    path: '/expenditure',
    icon: (
      <SvgIcon fontSize="small">
      <CurrencyExchangeIcon />
      </SvgIcon>
    )
  },

  {
    title: 'Support',
    path: '/support',
    icon: (
      <SvgIcon fontSize="small">
        <SupportAgentIcon/>
      </SvgIcon>
    )
  }
];
