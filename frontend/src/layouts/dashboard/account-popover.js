import { useCallback, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import axios from 'axios';  // Don't forget to import axios
import {
  Box,
  Divider,
  MenuItem,
  MenuList,
  Popover,
  Typography,
} from '@mui/material';
import { useAuth } from 'src/hooks/use-auth';

export const AccountPopover = (props) => {
  const { anchorEl, onClose, open } = props;
  const router = useRouter();
  const auth = useAuth();
  const [memberData, setMemberData] = useState({});
  const [loading, setLoading] = useState(true);

  const handleSignOut = useCallback(() => {
    const now = new Date();
    now.setFullYear(now.getFullYear() - 1); // Set the date to one year ago
    document.cookie = `charCode=; expires=${now.toUTCString()}; path=/`; // Set the cookie to expire immediately
    onClose?.();
    auth.signOut();
    router.push('/auth/login');
  }, [onClose, auth, router]);

  const formatDate = (dateString) => {
    const dateObject = new Date(dateString);
    const day = dateObject.getDate().toString().padStart(2, '0');
    const month = (dateObject.getMonth() + 1).toString().padStart(2, '0');
    const year = dateObject.getFullYear();
  
    return `${day}-${month}-${year}`;
  };

  const fetchMemberData = async () => {
    try {
      const charCode = sessionStorage.getItem('charCode');
      const response = await axios.get(`https://fithubbackend.onrender.com/profile?charCode=${charCode}`);
      const responseData = response.data;
      setMemberData(responseData.owner);
    } catch (error) {
      console.error('Error during member data fetch', error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchMemberData();
  }, []);

  return (
    <Popover
      anchorEl={anchorEl}
      anchorOrigin={{
        horizontal: 'left',
        vertical: 'bottom',
      }}
      onClose={onClose}
      open={open}
      PaperProps={{ sx: { width: 300 } }}
    >
      <Box
        sx={{
          py: 1.5,
          px: 2,
        }}
      >
        <Typography variant="subtitle1"> Account</Typography>
        <Typography variant="body2">
         <b>ID : </b>{memberData.ownerId} 
        </Typography>
        <Typography variant="body2">
        <b>Name : </b>{memberData.name} 
       </Typography>
       <Typography variant="body2">
       <b>Email : </b>{memberData.email} 
      </Typography>
      <Typography variant="body2">
       <b>Phone Number : </b>{memberData.number} 
      </Typography>
      <Typography variant="body2">
       <b>Gym Code : </b>{memberData.charCode} 
      </Typography>
      <Typography variant="body2">
       <b>Subcription Type : </b>{memberData.subscriptionType} 
      </Typography>
      <Typography variant="body2">
       <b>Valid Upto : </b>{formatDate(memberData.subscriptionExpireDate)} 
      </Typography>
      <Typography variant="body2">
       <b>Insta ID : </b>{memberData.insta} 
      </Typography>

      <Typography variant="body2">
       <b>Address: </b>{memberData.street} {memberData.city} {memberData.pincode} 
      </Typography>
      
      </Box>
      <Divider />
      <MenuList
        disablePadding
        dense
        sx={{
          p: '8px',
          '& > *': {
            borderRadius: 1,
          },
        }}
      >
        <MenuItem onClick={handleSignOut}
style={{color:"red"}}>Sign out</MenuItem>
      </MenuList>
    </Popover>
  );
};

AccountPopover.propTypes = {
  anchorEl: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
};
