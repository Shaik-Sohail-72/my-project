import { useTheme } from '@mui/material/styles';
import Image from 'next/image';

export const Logo = () => {
  const theme = useTheme();
  const fillColor = theme.palette.primary.main;

  return (
    <Image
      src="/ff.png" // Assuming ff.png is in the public directory
      height={60}  // Set the height in pixels
      width={300}   // Set the width in pixels
      alt="Your Logo"
    />
  );
};
